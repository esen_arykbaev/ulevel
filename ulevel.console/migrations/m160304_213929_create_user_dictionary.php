<?php

use yii\db\Migration;

class m160304_213929_create_user_dictionary extends Migration
{
    public function up()
    {
        $tableOptions = 'ENGINE = InnoDB DEFAULT CHARACTER SET = utf8mb4';

        $this->createTable('user_dictionary', [
            'user_id' => 'INT UNSIGNED NOT NULL',
            'word_id' => 'INT UNSIGNED NOT NULL',
            'translate' => 'VARCHAR(255) NOT NULL',

            'INDEX `fk_user_dictionary_user_user_id_idx` (`user_id` ASC)',
            'INDEX `fk_user_dictionary_dictionary_word_id_idx` (`word_id` ASC)',
            'CONSTRAINT `fk_user_dictionary_user_user_id`
                FOREIGN KEY (`user_id`)
                REFERENCES `ulevel`.`user` (`user_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION',
            'CONSTRAINT `fk_user_dictionary_dictionary_word_id`
                FOREIGN KEY (`word_id`)
                REFERENCES `ulevel`.`dictionary` (`word_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('user_dictionary');
    }
}
