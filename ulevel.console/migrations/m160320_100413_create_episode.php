<?php

use yii\db\Migration;

class m160320_100413_create_episode extends Migration
{
    public function up()
    {
        $tableOptions = 'ENGINE = InnoDB DEFAULT CHARACTER SET = utf8mb4';

        $this->createTable('episode', [
            'movie_id' => 'INT UNSIGNED NOT NULL',
            'episode_id' => 'INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'season' => 'TINYINT(1) NOT NULL',
            'name_ru' => 'VARCHAR(255) NOT NULL',
            'name_en' => 'VARCHAR(255) NOT NULL',
            'description' => 'VARCHAR(5000) NOT NULL',
            'viewed' => 'INT UNSIGNED NOT NULL',
            'release_year' => 'YEAR NOT NULL',
            'duration' => 'VARCHAR(45) NOT NULL',
            'en_subtitle' => 'VARCHAR(255) NULL',
            'ru_subtitle' => 'VARCHAR(255) NULL',
            'youtube' => 'VARCHAR(255) NULL',
            'vimeo' => 'VARCHAR(255) NULL',
            'megogo' => 'VARCHAR(255) NULL',
            'local_video' => 'VARCHAR(255) NULL',
            'created_at' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',

            'INDEX `fk_movie_id_episode_movie_idx` (`movie_id` ASC)',
            'CONSTRAINT `fk_movie_id_episode_movie`
                FOREIGN KEY (`movie_id`)
                REFERENCES `ulevel`.`movie` (`movie_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('episode');
    }
}
