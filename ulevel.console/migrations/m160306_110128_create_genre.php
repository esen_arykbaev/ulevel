<?php

use yii\db\Migration;

class m160306_110128_create_genre extends Migration
{
    public function up()
    {
        $tableOptions = 'ENGINE = InnoDB DEFAULT CHARACTER SET = utf8mb4';

        $this->createTable('genre', [
            'genre_id' => 'INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'genre_name' => 'VARCHAR(255) NOT NULL',
            'level' => 'TINYINT(1) UNSIGNED NULL'
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('genre');
    }
}
