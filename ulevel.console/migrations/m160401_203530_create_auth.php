<?php

use yii\db\Migration;

class m160401_203530_create_auth extends Migration
{
    public function up()
    {
        $tableOptions = 'ENGINE = InnoDB DEFAULT CHARACTER SET = utf8mb4';

        $this->createTable('auth', [
            'auth_id' => 'INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'user_id' => 'INT UNSIGNED NOT NULL',
            'source' => 'VARCHAR(255) NOT NULL',
            'source_id' => 'VARCHAR(255) NOT NULL',

            'INDEX `fk_user_id_auth_user_user_idx` (`user_id` ASC)',
            'CONSTRAINT `fk_user_id_auth_user_user_idx`
                FOREIGN KEY (`user_id`)
                REFERENCES `user` (`user_id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE'
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('auth');
    }
}
