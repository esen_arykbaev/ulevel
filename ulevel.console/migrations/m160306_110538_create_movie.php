<?php

use yii\db\Migration;

class m160306_110538_create_movie extends Migration
{
    public function up()
    {
        $tableOptions = 'ENGINE = InnoDB DEFAULT CHARACTER SET = utf8mb4';

        $this->createTable('movie', [
            'movie_id' => 'INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'name_ru' => 'VARCHAR(255) NOT NULL',
            'name_en' => 'VARCHAR(255) NOT NULL',
            'description' => 'VARCHAR(5000) NOT NULL',
            'viewed' => 'INT UNSIGNED NOT NULL DEFAULT 0',
            'release_year' => 'YEAR NOT NULL',
            'duration' => 'VARCHAR(45) NOT NULL',
            'level' => 'TINYINT(1) NOT NULL',
            'image' => 'VARCHAR(45) NULL',
            'en_subtitle' => 'VARCHAR(255) NULL',
            'ru_subtitle' => 'VARCHAR(255) NULL',
            'youtube' => 'VARCHAR(255) NULL',
            'vimeo' => 'VARCHAR(255) NULL',
            'megogo' => 'VARCHAR(255) NULL',
            'local_video' => 'VARCHAR(255) NULL',
            'movie_type' => 'TINYINT(1) UNSIGNED NOT NULL',
            'created_at' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('movie');
    }
}
