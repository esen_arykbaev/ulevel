<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = 'ENGINE = InnoDB DEFAULT CHARACTER SET = utf8mb4';

        $this->createTable('{{%user}}', [
            'user_id' => 'INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'first_name' => 'VARCHAR(255) NOT NULL',
            'email' => 'VARCHAR(255) NOT NULL',
            'email_status' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0',
            'avatar' => 'VARCHAR(255) NULL',
            'about_me' => 'TEXT NULL',
            'password' => 'VARCHAR(255) NOT NULL',
            'auth_key' => 'VARCHAR(45) NOT NULL',
            'password_reset_token' => 'VARCHAR(255) NULL',
            'level' => 'TINYINT(1) NOT NULL',
            'status' => 'TINYINT(1) NOT NULL',
            'created_at' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP'
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
