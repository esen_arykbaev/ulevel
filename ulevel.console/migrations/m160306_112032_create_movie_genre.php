<?php

use yii\db\Migration;

class m160306_112032_create_movie_genre extends Migration
{
    public function up()
    {
        $tableOptions = 'ENGINE = InnoDB DEFAULT CHARACTER SET = utf8mb4';

        $this->createTable('movie_genre', [
            'movie_id' => 'INT UNSIGNED NOT NULL',
            'genre_id' => 'INT UNSIGNED NOT NULL',

            'INDEX `fk_movie_genre_movie_movie_id_idx` (`movie_id` ASC)',
            'INDEX `fk_movie_genre_genre_genre_id_idx` (`genre_id` ASC)',
            'CONSTRAINT `fk_movie_genre_movie_movie_id`
                FOREIGN KEY (`movie_id`)
                REFERENCES `ulevel`.`movie` (`movie_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION',
            'CONSTRAINT `fk_movie_genre_genre_genre_id`
                FOREIGN KEY (`genre_id`)
                REFERENCES `ulevel`.`genre` (`genre_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION'
        ], $tableOptions) ;
    }

    public function down()
    {
        $this->dropTable('movie_genre');
    }
}
