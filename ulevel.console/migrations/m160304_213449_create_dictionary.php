<?php

use yii\db\Migration;

class m160304_213449_create_dictionary extends Migration
{
    public function up()
    {
        $tableOptions = 'ENGINE = InnoDB DEFAULT CHARACTER SET = utf8mb4';

        $this->createTable('dictionary', [
            'word_id' => 'INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'word_en' => 'VARCHAR(255) NOT NULL',
            'word_ru' => 'VARCHAR(255) NOT NULL',

            'INDEX `word_en_idx` (word_en)',
            'INDEX `word_ru_idx` (word_ru)'
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('dictionary');
    }
}
