<?php
namespace frontend\models;

use common\models\Level;
use Yii;
use yii\base\Model;

use common\models\User;


/**
 * Signup form
 */
class SignupForm extends Model
{
    public $firstName;
    public $email;
    public $password;
    public $passwordRepeat;
    //public $captcha;
    public $reCaptcha;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['firstName', 'email', 'password', 'passwordRepeat'], 'filter', 'filter' => 'trim'
            ],
            [
                ['firstName', 'email', 'password', 'passwordRepeat'], 'required'
            ],
            [
                'firstName', 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process'
            ],
            [
                'email', 'email'
            ],
            [
                'email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'
            ],
            [
                ['password', 'passwordRepeat'], 'string', 'min' => 6, 'max' => 255, 'message' => 'Длина пароля должна быть больше 5 символов'
            ],
            [
                'password', 'compare', 'compareAttribute' => 'passwordRepeat', 'message' => 'Пароли не совпадают'
            ],
            //['captcha', 'captcha', 'message' => 'Неправильный код. Пожалуйста, введите заново'],
            [['reCaptcha'], \himiklab\yii2\recaptcha\ReCaptchaValidator::className(), 'secret' => '6LdtuRwTAAAAABFvNmWbI_jdZvIvmnfEpKZsr2Sb'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'firstName' => Yii::t('app', 'Имя'),
            'email' => 'E-mail',
            'password' => Yii::t('app', 'Пароль'),
            'passwordRepeat' => Yii::t('app', 'Повторите пароль'),
            //'captcha' => 'Введите текст с картинки'
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->first_name = $this->firstName;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->status = User::STATUS_ACTIVE;
        $user->level = Level::LEVEL_BEGINNER;
        
        return $user->save() ? $user : null;
    }
}
