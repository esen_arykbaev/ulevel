<?php

namespace frontend\models;


class MovieHelper
{
    /**
     * @param $season
     * @return null|string
     */
    public static function getEpisodeSourceName($season)
    {
        if (!empty($season->youtube)) {
            return 'youtube';
        } elseif (!empty($season->vimeo)) {
            return 'vimeo';
        } elseif (!empty($season->megogo)) {
            return 'megogo';
        } elseif (!empty($season->local_video)) {
            return 'local_video';
        } else {
            return null;
        }
    }

    /**
     * @param $season
     * @return null|string
     */
    public static function getEpisodeUrl($season)
    {
        if (!empty($season->youtube)) {
            return $season->youtube;
        } elseif (!empty($season->vimeo)) {
            return $season->vimeo;
        } elseif (!empty($season->megogo)) {
            return $season->megogo;
        } elseif (!empty($season->local_video)) {
            return $season->local_video;
        } else {
            return null;
        }
    }
}