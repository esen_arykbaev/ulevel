<?php

namespace frontend\models;

use common\models\Dictionary;
use common\models\UserDictionary;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;


class WordTest
{
    /**
     * Количество слов для теста
     */
    const WORDS_COUNT = 10;

    /**
     * @var array
     */
    public $words;

    /**
     * @var
     */
    public $tests;

    /**
     * @var
     */
    private $_userId;

    public function __construct()
    {
        $this->_userId = Yii::$app->user->identity->user_id;

        // Gets random user words
        $userDictionaries = Dictionary::find()
                            ->innerJoin(
                                UserDictionary::tableName(),
                                Dictionary::tableName() . '.word_id = ' . UserDictionary::tableName() . '.word_id'
                            )
                            ->where([ 'user_id' => $this->_userId ])
                            ->orderBy(new Expression('RAND()'))
                            ->limit(self::WORDS_COUNT)
                            ->all();

        $this->words = ArrayHelper::map($userDictionaries, 'word_en', 'word_ru');;
        $userDictionariesCount = count($this->words);

        if ($userDictionariesCount < 4) {
            return false;
        }

        $randomDictionaries = Dictionary::find()
                                        ->innerJoin(
                                            UserDictionary::tableName(),
                                            Dictionary::tableName() . '.word_id = ' . UserDictionary::tableName() . '.word_id'
                                        )
                                        ->where([ 'user_id' => $this->_userId ])
                                        ->orderBy(new Expression('RAND()'))
                                        ->limit($userDictionariesCount * 3)
                                        ->all();

        $randomDictionaries = ArrayHelper::getColumn($randomDictionaries, 'word_ru');
        $randomDictionariesCount = count($randomDictionaries);

        foreach ($this->words as $key => $val) {
            $tempArray = [];
            for ($i = $j = 0; $i < 3 && $j < $randomDictionariesCount; $j++) {
                // Если переводы совпадают
                if ($val == $randomDictionaries[$j]) {
                    continue;
                }
                $tempArray[] = $randomDictionaries[$j];
                $i++;
            }
            shuffle($randomDictionaries);
            $tempArray[] = $val;
            shuffle($tempArray);

            // Массив слов с вариантами переводов
            $this->tests[$key] = $tempArray;
        }

    }
}