<?php
namespace frontend\models;

use yii\base\Model;
use Yii;


class UserSettingsForm extends Model
{
    public $firstName;
    public $oldPassword;
    public $newPassword;
    public $newPasswordRepeat;
    public $aboutMe;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [
                ['firstName', 'oldPassword', 'newPassword', 'newPasswordRepeat', 'aboutMe'], 'filter', 'filter' => 'trim'
            ],
            [
                ['firstName', 'aboutMe'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process'
            ],
            [
                'firstName', 'required'
            ],
            [
                ['oldPassword', 'newPassword', 'newPasswordRepeat'], 'required', 'when' => function($model) {
                    return !empty($model->oldPassword) || !empty($model->newPassword) || !empty($model->newPasswordRepeat);
                }
            ],
            [
                ['oldPassword', 'newPassword', 'newPasswordRepeat'], 'string', 'min' => 6, 'max' => 255
            ],
            [
                'newPassword', 'compare', 'compareAttribute' => 'newPasswordRepeat', 'message' => 'Пароли не совпадают'
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'firstName' => Yii::t('app', 'Имя'),
            'oldPassword' => Yii::t('app', 'Старый Пароль'),
            'newPassword' => Yii::t('app', 'Новый Пароль'),
            'newPasswordRepeat' => Yii::t('app', 'Повторите Пароль'),
            'aboutMe' => Yii::t('app', 'О себе')
        ];
    }
}