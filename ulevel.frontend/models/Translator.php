<?php

namespace frontend\models;

use common\models\Dictionary;


/**
 * Class Translator
 * @package frontend\models
 */
class Translator
{
    /**
     *
     */
    const SUPPORTED_LANGUAGES = [
        'en', 'ru'
    ];

    /**
     * @var string
     */
    const GOOGLE_URL = 'https://www.googleapis.com/language/translate/v2';

    /**
     * Ключ для гугл переводчика
     * @var string
     */
    const GOOGLE_KEY = 'AIzaSyBPaE09txeCYisdccb6wt8JOiuYlSsnp3k';

    /**
     * @var string
     */
    public static $sourceLang = '';

    /**
     * @var string
     */
    public static $targetLang = '';


    /**
     * @var string
     */
    public static $word = '';

    /**
     * @var string
     */
    public static $translatedWord = '';

    /**
     * Возращает перевод слово
     * @return string
     */
    public static function translateWord()
    {

        if (self::$sourceLang == 'en') {
            $dictionary = Dictionary::findOne([
                'word_en' => self::$word
            ]);
        } else {
            $dictionary = Dictionary::findOne([
                'word_ru' => self::$word
            ]);
        }


        // Если нет в базе, то переводем
        if ($dictionary === null) {
            self::$translatedWord = self::getWordFromGoogleTranslator();
            $dictionary = self::saveWord();
        }

        self::$translatedWord = self::$sourceLang == 'en' ? $dictionary->word_ru : $dictionary->word_en;
    }

    /**
     * Возвращает переведенное слово гуглом
     * @return string
     */
    public static function getWordFromGoogleTranslator()
    {
        $url = self::GOOGLE_URL . '?'
                            . 'key=' . self::GOOGLE_KEY
                            . '&'
                            . 'source=' . self::$sourceLang
                            . '&'
                            . 'target=' . self::$targetLang
                            . '&'
                            . 'q=' . self::$word;

        $data = file_get_contents($url);

        if ($data === false) {
            return '';
        }

        $jsonObj = json_decode($data);
/*
 * вывод print_r($jsonData):
stdClass Object
(
  [data] => stdClass Object
  (
    [translations] => Array
      (
        [0] => stdClass Object
          (
            [translatedText] => слово
          )
      )
  )
)
*/
        return $jsonObj->data->translations[0]->translatedText;
    }

    /**
     * Сохраняет переведенное слово в БД
     */
    public static function saveWord()
    {
        $dictionary = new Dictionary();
        $dictionary->word_en = self::$sourceLang == 'en' ? self::$word : self::$translatedWord;
        $dictionary->word_ru = self::$sourceLang == 'ru' ? self::$word : self::$translatedWord;

        $dictionary->save();

        return $dictionary;
    }
}