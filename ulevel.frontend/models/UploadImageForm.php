<?php
namespace frontend\models;

use common\models\User;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use yii\imagine\Image as Imagine;
use Imagine\Image\Box;


class UploadImageForm extends Model
{
    public $image;

    public $imagePath;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [
                'image', 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg, gif'
            ]
        ];
    }

    public function __construct()
    {
        parent::__construct();

        $this->imagePath = Yii::getAlias('@webroot') . '/images/avatars';
    }

    /**
     *
     */
    public function upload()
    {
        if ($this->validate()) {

            $imageFolder = substr(md5(time()), mt_rand(0, 30), 2);

            $userId = Yii::$app->user->identity->user_id;
            // название файла: user_id-xxxxx
            $imageName = $userId . '_' .substr(md5(time()), mt_rand(0, 27), 5) . '.' . $this->image->extension;

            if (!file_exists($this->imagePath . DIRECTORY_SEPARATOR . $imageFolder)) {
                mkdir($this->imagePath . DIRECTORY_SEPARATOR . $imageFolder);
            }

            $imagePath = $this->imagePath . DIRECTORY_SEPARATOR . $imageFolder . DIRECTORY_SEPARATOR . $imageName;

            $this->image->saveAs($imagePath);

            $this->resizeImage($imagePath);

            return $imageFolder . '/' . $imageName;
        }

        return false;
    }


    public function resizeImage($_image)
    {
        $image = Imagine::getImagine()->open($_image);
        // Изменяем размер аватарки
        if ($this->isResizeImage($image->getSize())) {
            $newSizes = $this->getImageNewSizes($image->getSize());
            $resizedImage = $image->resize(new Box($newSizes['width'], $newSizes['height']));
            $resizedImage->save($_image);
        }
    }

    /**
     * @param $size
     * @return bool
     */
    public function isResizeImage($size)
    {
        return ($size->getWidth() > User::AVATAR_WIDTH) || ($size->getHeight() > User::AVATAR_HEIGHT);
    }

    /**
     * @param $size
     * @return array
     */
    public static function getImageNewSizes($size)
    {
        $w = $size->getWidth();
        $h = $size->getHeight();
        $newDimensions = [];

        if ($w > $h) {
            $newDimensions['width'] = User::AVATAR_WIDTH;
            $newDimensions['height'] = floor( ($h/$w) * User::AVATAR_WIDTH );
        } else {
            $newDimensions['width'] = floor( ($w/$h) * User::AVATAR_HEIGHT );
            $newDimensions['height'] = User::AVATAR_HEIGHT;
        }

        return $newDimensions;
    }
}