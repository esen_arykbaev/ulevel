<?php

namespace frontend\models;


use common\models\Genre;
use common\models\Level;
use common\models\Movie;
use common\models\MovieGenre;

class MovieSearch
{
    public static $level;

    public static $levelId;

    public static $isQuerySearch = false;

    /**
     * @param $requestParams
     * @return $this
     */
    public static function getQuery($requestParams)
    {
        $q          = isset($requestParams['q']) ? trim($requestParams['q']) : '';
        $level      = isset($requestParams['level']) ? $requestParams['level'] : '';
        $genreId    = isset($requestParams['genre']) ? $requestParams['genre'] : '';

        if ($level == Level::LEVELS_LINK[Level::LEVEL_INTERMEDIATE]) {
            self::$level = Level::LEVELS_LINK[Level::LEVEL_INTERMEDIATE];
            self::$levelId = Level::LEVEL_INTERMEDIATE;
        } elseif ($level == Level::LEVELS_LINK[Level::LEVEL_ADVANCED]) {
            self::$level = Level::LEVELS_LINK[Level::LEVEL_ADVANCED];
            self::$levelId = Level::LEVEL_ADVANCED;
        } else {
            self::$level = Level::LEVELS_LINK[Level::LEVEL_BEGINNER];
            self::$levelId = Level::LEVEL_BEGINNER;
        }

        $querySelect = [
            'movie.movie_id', 'name_ru', 'name_en', 'description', 'viewed', 'duration', 'image'
        ];

        $query = Movie::find()
                        ->select($querySelect);

        // Соединяем с таблицой жанров фильма
        if ($genreId != '') {
            $query = $query->innerJoin(MovieGenre::tableName(), 'movie.movie_id = movie_genre.movie_id');
        }

        if ($q == '') {
            $query = $query->where([
                'level' => self::$levelId
            ]);
        } else {
            $query = $query->where([
                    'like', 'name_ru', $q
                ])->orWhere([
                    'like', 'name_en', $q
                ]);
            self::$isQuerySearch = true;
        }

        if ($genreId != '') {
            $query = $query->andWhere([
                        'genre_id' => $genreId
                    ]);
        }

        return $query;
    }
}