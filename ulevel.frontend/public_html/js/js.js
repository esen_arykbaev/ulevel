jQuery(document).ready(function($) {

  $('[data-toggle="popover"]').popover();

  $(".scroll").click(function(event){
    event.preventDefault();
    $('html,body').animate({scrollTop:$(this.hash).offset().top}, 1400);
  });

  if ($('video').length) {
    setVideoPlayer();
  }

  $('.serial-item').on('click', function() {
    var source = $(this).data('source'),
        url = $(this).data('url'),
        enSub = $(this).data('en-sub'),
        ruSub = $(this).data('ru-sub');

      var o = {
        source: source,
        url: url,
        enSub: enSub,
        ruSub: ruSub
      };

    setEpisodeMovie(o);
  });

});

var TranslateIt = {

  wordTag: undefined,

  sourceLang: 'en',

  targetLang: 'ru',

  word: '',

  wordPosition: {},

  translate: function(word) {
    this.word = word;
    this.ajax();
  },

  ajax: function() {
    var ajaxData = {};

    ajaxData._csrf = getCsrf();
    ajaxData.source = this.sourceLang;
    ajaxData.target = this.targetLang;
    ajaxData.word = this.word;

    $.ajax({
      url: '/ajax/translate.html',
      method: 'post',
      data: ajaxData,
      success: function(result) {
        result = JSON.parse(result);
        TranslateIt.translatedWords[TranslateIt.word] = result.translatedWord;
        TranslateIt.show();
      },
      error: function(result) {
        console.log(result);
      }
    })
  },

  show: function() {
    var i = 'fa-plus-circle';
    $('#translated-word').remove();

    // Если пользователь уже добавил слово в свой словарь
    if (this.userDictionary.indexOf(this.word) !== -1) {
      i = 'fa-check-square';
    }

    this.wordTag.append('<div id="translated-word">' +
      '<div id="translated-word__title">'+ this.word +'</div>' +
      '<div id="translated-word__meaning">'+ TranslateIt.translatedWords[this.word] +'</div>' +
      '<i class="fa '+ i +'"></i>' +
      '</div>');

    var h = this.wordTag.height();
    $('#translated-word').css('bottom', h +'px');

  },

  userDictionary: [],

  addToDictionary: function() {
    var ajaxData = {
      word: TranslateIt.word,
      translate: TranslateIt.translatedWords[TranslateIt.word],
      action: 'add-word'
    };
    ajaxRequest(ajaxData);
    this.userDictionary.push(this.word);
    incDictionaryCount();
  },

  /**
   *
   */
  translatedWords: {}
};

var _csrf = null;

function getCsrf()
{
  if (!_csrf) {
    _csrf = $('meta[name="csrf-token"]').attr('content');
  }
  return _csrf;
}
/**
 *
 * @param data
 */
function ajaxRequest(data)
{
  data._csrf = getCsrf();

  $.ajax({
    url: '/ajax/'+ data.action +'.html',
    method: 'post',
    data: data,
    success: function(response) {
      var r = JSON.parse(response);

      if (data.callback) {
        var f = data.callback.split('.');
        window[f[0]][f[1]](r);
      } else {
        if (r.message.length > 0) {
          alert(r.message);
        }
      }
    }
  });
}

function setEpisodeMovie(obj)
{
  var $video = $('.video'),
      frame = '';

  if (obj.source == 'youtube') {
    frame = getEpisodeMovieYoutube(obj);
  } else if (obj.source == 'vimeo') {
    frame = getEpisodeMovieVimeo(obj);
  } else if (obj.source == 'megogo') {
    frame = getEpisodeMovieMegogo(obj);
  } else if (obj.source == 'local_video') {
    frame = getEpisodeMovieLocalVideo(obj);
  } else {
    return;
  }

  $video.html(frame);
  setVideoPlayer();
}

function getEpisodeMovieYoutube(obj)
{
  var t = '<link itemprop="embedUrl" href="'+ obj.url +'">' +
    '<video width="900" height="420" preload="none" controls="controls" itemprop="video" itemscope itemtype="http://schema.org/VideoObject">' +
    '<source type="video/youtube" src="'+ obj.url +'" />' +
    getHtmlSub('en', obj.enSub) +
    getHtmlSub('ru', obj.ruSub) +
    '</video>';

  return t;
}

function getEpisodeMovieVimeo(obj)
{
  var t = '<link itemprop="embedUrl" href="'+ obj.url +'">' +
    '<video width="900" height="420" preload="none" controls="controls" itemprop="video" itemscope itemtype="http://schema.org/VideoObject">' +
    '<source type="video/vimeo" src="'+ obj.url +'" />' +
    getHtmlSub('en', obj.enSub) +
    getHtmlSub('ru', obj.ruSub) +
    '</video>';

  return t;
}

function getEpisodeMovieMegogo(obj)
{
  var t = '<link itemprop="embedUrl" href="'+ obj.url +'">' +
    '<div itemprop="embedHTML">' +
    '<iframe width="900" height="420" src="'+ obj.url +'" frameborder="0" allowfullscreen></iframe>' +
    '</div>';

  return t;
}

function getEpisodeMovieLocalVideo(obj)
{
  var t = '<video width="900" height="420" preload="none" controls="controls" itemprop="video" itemscope itemtype="http://schema.org/VideoObject">' +
    '<source type="video/mp4" src="/videos/'+ obj.url +'" />' +
    getHtmlSub('en', obj.enSub) +
    getHtmlSub('ru', obj.ruSub) +
    '</video>';

  return t;
}

function getHtmlSub(subType, sub)
{
  if (sub) {
    return '<track srclang="'+ subType +'" label="'+ subType.toUpperCase() +'" kind="subtitles" src="/subs/'+ subType +'/'+ sub +'" itemprop="subtitle"/>';
  }
  return '';
}

function setVideoPlayer()
{
  $('video').mediaelementplayer({
    defaultVideoWidth: 900,
    defaultVideoHeight: 420,
    skipBackInterval: 5,
    jumpForwardInterval: 5,
    startLanguage:'en',
    startVolume: 0.3,
    features: ['playpause','skipback','speed','jumpforward','progress','current','duration','tracks','contextmenu','volume','fullscreen','universalgoogleanalytics'],
    speeds: ['0.5','0.75','1.5','2.0'],
    defaultSpeed: '1.0',
    enableAutosize: true,
    success: function(media) {

      var $mejsOverlayPlay = $('.mejs-overlay-play'),
        $mejsCaptionsText = $('.mejs-captions-text'),
        $mejsCaptionsPosition = $('.mejs-captions-position');

      // Добавляем в словарь пользователя
      $mejsCaptionsPosition.on('click', '.fa-plus-circle', function() {
        TranslateIt.addToDictionary();
        $mejsCaptionsPosition.find('.fa-plus-circle').removeClass('fa-plus-circle').addClass('fa-check-square');
      });

      /*
       * Обработчики событий
       */
      $mejsCaptionsText.hover(
        function() {
          media.pause();
          $mejsOverlayPlay.addClass('hide');
        }, function() {
          media.play();
          $mejsOverlayPlay.removeClass('hide');
        }
      );

      $mejsCaptionsText.on('mouseenter', '.word', function() {
        var $word = $(this),
          t = $word.text();
        TranslateIt.wordPosition = $word.position();
        TranslateIt.wordTag = $word;

        if (t in TranslateIt.translatedWords) {
          TranslateIt.word = t;
          TranslateIt.show();
        } else {
          TranslateIt.translate(t);
        }
      });

      $mejsCaptionsText.on('mouseleave', '.word', function() {
        $(this).find('#translated-word').remove();
      });

      /**
       * Меняет значения свойст объекта TranslateIt
       */
      $('input[name="mep_0_captions"]').change(function() {
        if (this.value === 'en') {
          TranslateIt.sourceLang = 'en';
          TranslateIt.targetLang = 'ru';
        } else if (this.value === 'ru') {
          TranslateIt.sourceLang = 'ru';
          TranslateIt.targetLang = 'en';
        }
      });
    }
  });
}

jQuery(document).ready(function()
{
  $('#begin-test').on('click', function() {
    var $testStart = $('#test-start');
    if (Test.tests) {
      Test.init();
      $testStart.hide();
      $('#test-container').show();
    } else {
      $testStart.html('<p class="alert alert-info"> У вас недостаточно слов, чтобы пройти тест :( </p>');
    }

  });

  $('#test-translations').on('change', '.test-radio', function() {

    Test.checkAnswer($('.test-radio:checked').val());
    Test.renderNextQuestion();
  });
});

var Test = {

  words: undefined,

  tests: undefined,

  translate: undefined,

  testArray: [],

  currectTestIndex: 0,

  passedTestsCount: 0,

  allTestsCount: undefined,

  correctAnswers: 0,

  wrongAnswers: 0,

  init: function() {

    // Если есть ошибки
    $('#result-chart').hide();
    $('#test-container-inner').show();

    this.currectTestIndex = 0;
    this.passedTestsCount = 0;
    this.correctAnswers = 0;
    this.wrongAnswers = 0;

    for (var i in this.tests) {
      this.testArray.push(i);
    }
    this.allTestsCount = Object.keys(this.tests).length;

    this.renderTestAllCount();
    this.renderTestLeftCount(this.allTestsCount);

    this.renderWord();
    this.renderQuestions();

    $('.test-again').hide();
  },

  renderTestAllCount: function() {
    $('#tests-all').text(this.allTestsCount);
  },

  renderTestLeftCount: function(n) {
    $('#tests-left').text(n);
  },

  renderWord: function() {
    $('#test-word').text(this.testArray[this.currectTestIndex]);
  },

  renderQuestions: function() {
    var tests = this.tests[this.testArray[this.currectTestIndex]],
        b = '';

    for (var i = 0; i < tests.length; i++) {
      b += '<div>' +
            '<input id="test_id_'+ i +'" type="radio" value="'+ tests[i] +'" class="test-radio">' +
            '<label for="test_id_'+ i +'" class="btn test-btn">'+ tests[i] +'</label>' +
          '</div>';
    }

    $('#test-translations').html(b);
  },

  renderNextQuestion: function() {

    this.passedTestsCount++;
    this.currectTestIndex++;

    if (this.passedTestsCount >= this.allTestsCount) {
      this.showResult(true);
      return;
    }

    this.renderTestLeftCount(this.allTestsCount - this.passedTestsCount);
    this.renderWord();
    this.renderQuestions();
  },

  checkAnswer: function(answer) {
    if (this.words[this.testArray[this.currectTestIndex]] == answer) {
      this.correctAnswers++;
      delete this.words[this.testArray[this.currectTestIndex]];
      delete this.tests[this.testArray[this.currectTestIndex]];
    } else {
      this.wrongAnswers++;
    }
  },

  showResult: function(passedAllTests) {
    $('#test-container-inner').hide();
    $('#result-chart').show();

    // Apply the theme
    Highcharts.setOptions({colors: ["#f45b5b", "#8085e9"]});

    $('#MyBar').highcharts({
      plotOptions: {
        series: {
          shadow: true
        },
        candlestick: {
          lineColor: '#404048'
        },
        map: {
          shadow: false
        }
      },
      chart: {
        type: 'column'
      },
      title: {
        text: 'Ваш результат:'
      },
      xAxis: {
        categories: ['Uword'],
        labels: {
          style: {
            color: '#6e6e70'
          }
        }
      },
      yAxis: {
        title: {
          text: 'значения'
        },
        labels: {
          style: {
            color: '#6e6e70'
          }
        }
      },
      credits: {
        enabled: false
      },
      series: [{
        name: 'Ошибок',
        data: [Test.wrongAnswers]
      }, {
        name: 'Правильных',
        data: [Test.correctAnswers]
      }]
    });

    if (passedAllTests && !this.wrongAnswers) {
      $('.test-again').hide();
    } else if (!passedAllTests) {
      $('.test-again').show().find('.btn').html('<i class="fa fa-repeat"></i>&nbsp;&nbsp;Продолжить');
    }  else if (this.wrongAnswers) {
      $('.test-again').show().find('.btn').html('<i class="fa fa-repeat"></i>&nbsp;&nbsp;Еще раз');
    }
    this.testArray = [];
  }
};

function incDictionaryCount()
{
  var $b = $('#dictionary-count'),
      c = parseInt($b.text());

  if (!isNaN(c)) {
    $b.text(++c);
  }
}