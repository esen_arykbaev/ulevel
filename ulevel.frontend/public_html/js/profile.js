jQuery(document).ready(function()
{
  $('.btn-word-edit').on('click', function() {
    var wordId = $(this).data('word-id'),
      $translate = $('#translate-' + wordId),
      word = $translate.data('word');

    $translate.html('<textarea class="form-control">'+ word +'</textarea>');
    $('.edit-btns-'+ wordId).toggleClass('hide');
  });

  $('.btn-word-remove').on('click', function() {
    UserDictionary.delete($(this).data('word-id'));
    $(this).closest('tr').remove();
  });

  $('.btn-word-save').on('click', function() {
    var wordId = $(this).data('word-id'),
      $translate = $('#translate-' + wordId),
      translate = $translate.find('textarea').val().trim();

    if (!translate) {
      return;
    }
    UserDictionary.save(wordId, translate);
    $translate.html(translate).data('word', translate);
    $('.edit-btns-'+ wordId).toggleClass('hide');
  });

  $('.btn-word-close').on('click', function() {
    var wordId = $(this).data('word-id'),
      $translate = $('#translate-' + wordId),
      word = $translate.data('word');

    $translate.html(word);
    $('.edit-btns-'+ wordId).toggleClass('hide');
  });

  //
  $('.say-word').on('click', function(e) {
    e.preventDefault();
    var word = $(this).data('word');
    var url = "https://ssl.gstatic.com/dictionary/static/sounds/de/0/" + word + ".mp3";
    $('#speech').attr('src', url).get(0).play();
  });

  $('#confirm-email').on('click', function() {
    var d = {
      action: 'resend-email-confirm'
    };

    ajaxRequest(d);
  });

});

var UserDictionary = {

  delete: function(wordId) {

    var ajaxData = {
      'action': 'remove-word',
      'wordId': wordId
    };

    ajaxRequest(ajaxData);
  },

  save: function(wordId, translate) {
    var ajaxData = {
      'action': 'save-word',
      'wordId': wordId,
      'translate': translate
    };

    ajaxRequest(ajaxData);
  }
};

var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
var lineChartData = {
  labels : ["January","February","March","April","May","June","July"],
  datasets : [
    {
      label: "Добавленных слов",
      fillColor : "rgba(220,220,220,0.2)",
      strokeColor : "rgba(220,220,220,1)",
      pointColor : "rgba(220,220,220,1)",
      pointStrokeColor : "#fff",
      pointHighlightFill : "#fff",
      pointHighlightStroke : "rgba(220,220,220,1)",
      data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
    },
    {
      label: "Выученных слов",
      fillColor : "rgba(151,187,205,0.2)",
      strokeColor : "rgba(151,187,205,1)",
      pointColor : "rgba(151,187,205,1)",
      pointStrokeColor : "#fff",
      pointHighlightFill : "#fff",
      pointHighlightStroke : "rgba(151,187,205,1)",
      data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
    }
  ]

}

window.onload = function(){
  var ctx = document.getElementById("canvas").getContext("2d");
  window.myLine = new Chart(ctx).Line(lineChartData, {
    responsive: true
  });
}