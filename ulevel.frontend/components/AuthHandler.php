<?php

namespace frontend\components;

use Yii;
use yii\authclient\ClientInterface;
use yii\helpers\ArrayHelper;

use common\models\Auth;
use common\models\User;
use common\models\Level;

/**
 * Class AuthHandler
 * @package frontend\components
 */
class AuthHandler
{
    /**
    * @var ClientInterface
    */
    private $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function handle()
    {
        $attributes = $this->client->getUserAttributes();

        $email = ArrayHelper::getValue($attributes, 'email');
        if ($this->client->getName() == 'google') {
            $email = $this->getGoogleUserEmaail($attributes);
        }


        $id = ArrayHelper::getValue($attributes, 'id');
        $username = $this->getUserName($attributes);

        /** @var Auth $auth */
        $auth = Auth::find()->where([
            'source' => $this->client->getId(),
            'source_id' => $id,
        ])->one();

        if (Yii::$app->user->isGuest) {
            if ($auth) { // login
                /** @var User $user */
                $user = $auth->user;
                Yii::$app->user->login($user, 3600 * 24 * 30);
            } else { // signup

                $user = User::findByEmail($email);

                if ($email !== null && $user !== null) {

                    /*Yii::$app->getSession()->setFlash('error',
                        Yii::t('app', "User with the same email as in {$this->client->getTitle()} account already exists but isn't linked to it. Login using email first to link it.")
                    );*/

                    /*
                     * Vk,Fb - можно так сделать что если этот email есть то пусть входит существющем аккаунте
                     */
                    Yii::$app->user->login($user, 3600 * 24 * 30);

                } else {
                    $user = new User([
                        'first_name' => $username,
                        'email' => $email
                    ]);
                    $user->setPassword(Yii::$app->security->generateRandomString(6));
                    $user->generateAuthKey();
                    $user->generatePasswordResetToken();
                    $user->email_status = User::EMAIL_CONFIRMED;
                    $user->status = User::STATUS_ACTIVE;
                    $user->level = Level::LEVEL_BEGINNER;

                    $transaction = User::getDb()->beginTransaction();

                    if ($user->save()) {
                        $auth = new Auth([
                            'user_id' => $user->user_id,
                            'source' => $this->client->getId(),
                            'source_id' => (string)$id,
                        ]);
                        if ($auth->save()) {
                            $transaction->commit();
                            Yii::$app->user->login($user, 3600 * 24 * 30);
                        } else {
                            Yii::$app->getSession()->setFlash('error',
                                Yii::t('app', "Unable to save {$this->client->getTitle()} account: " . json_encode($auth->getErrors()))
                            );
                        }
                    } else {
                        Yii::$app->getSession()->setFlash('error',
                            Yii::t('app', 'Unable to save user: ' . json_encode($user->getErrors()))
                        );
                    }
                }
            }
        } else { // user already logged in
            if (!$auth) { // add auth provider
                $auth = new Auth([
                    'user_id' => Yii::$app->user->id,
                    'source' => $this->client->getId(),
                    'source_id' => (string)$attributes['id'],
                ]);
                if ($auth->save()) {
                    //$this->updateUserInfo($user);
                    Yii::$app->getSession()->setFlash('success',
                        Yii::t('app', 'Linked {client} account.', [
                            'client' => $this->client->getTitle()
                        ])
                    );
                } else {
                    Yii::$app->getSession()->setFlash('error',
                        Yii::t('app', 'Unable to link {client} account: {errors}', [
                            'client' => $this->client->getTitle(),
                            'errors' => json_encode($auth->getErrors()),
                        ])
                    );
                }
            } else { // there's existing auth
                Yii::$app->getSession()->setFlash('error',
                    Yii::t('app',
                        "Unable to link {$this->client->getTitle()} account. There is another user using it."
                    )
                );
            }
        }

    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function getUserName($attributes = [])
    {
        if ($this->client->getName() == 'vkontakte') {
            return ArrayHelper::getValue($attributes, 'first_name');
        } elseif ($this->client->getName() == 'facebook') {
            return ArrayHelper::getValue($attributes, 'name');
        } elseif ($this->client->getName() == 'google') {
            return ArrayHelper::getValue($attributes, 'displayName');
        }
    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function getGoogleUserEmaail($attributes = [])
    {
        return $attributes['emails'][0]['value'];
    }
}