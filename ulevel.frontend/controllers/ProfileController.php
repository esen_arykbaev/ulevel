<?php

namespace frontend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\UploadedFile;

use common\models\User;
use common\models\Auth;
use common\models\LoginForm;
use common\models\Dictionary;
use common\models\UserDictionary;

use frontend\models\UploadImageForm;
use frontend\models\UserSettingsForm;
use frontend\models\SignupForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\components\AuthHandler;
use frontend\models\WordTest;
use frontend\components\FrontendController;


/**
 * ProfileController implements the CRUD actions for User model.
 */
class ProfileController extends FrontendController
{
    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction'
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
        ];
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            /*'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],*/
        ];
    }

    public function onAuthSuccess($client)
    {
        (new AuthHandler($client))->handle();

        if (Yii::$app->getSession()->hasFlash('error')) {
            Yii::$app->session->setFlash('error' ,
                Yii::$app->session->getFlash('error')
                );
            return $this->redirect(['login']);
        }

        return $this->redirect('view.html');
    }

    /**
     * Displays a single User model.
     * @return mixed
     */
    public function actionView()
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect('login.html');
        }

        $model =  $this->findModel(Yii::$app->user->identity->user_id);

        $uploadImageForm = new UploadImageForm();

        $settingsForm = new UserSettingsForm();

        if (Yii::$app->request->isPost) {

            if ($settingsForm->load(Yii::$app->request->post()) && $settingsForm->validate()) {

                $model->first_name  = $settingsForm->firstName;
                $model->about_me    = $settingsForm->aboutMe;

                if (!empty($settingsForm->oldPassword)) {
                    if ($model->validatePassword($settingsForm->oldPassword)) {
                        // Если старый пароль совпадает, то меняем на новый
                        $model->setPassword($settingsForm->newPassword);
                    } else {
                        $settingsForm->addError('oldPassword', 'Текущий пароль указан неверно');
                    }
                }

                if ($model->save(false)) {
                    Yii::$app->session->setFlash('successMessage', 'Настройки успешно сохранены');

                    $settingsForm->oldPassword          = null;
                    $settingsForm->newPassword          = null;
                    $settingsForm->newPasswordRepeat    = null;
                }
            }

            if ($uploadImageForm->load(Yii::$app->request->post())) {

                $uploadImageForm->image = UploadedFile::getInstance($uploadImageForm, 'image');
                $uploadedImagePath = $uploadImageForm->upload();
                if ($uploadedImagePath !== false) {
                    $model->avatar = $uploadedImagePath;
                    $model->save(false);
                }
            }

        } else {
            $settingsForm->firstName = $model->first_name;
            $settingsForm->aboutMe = $model->about_me;
        }

        return $this->render('view', [
            'model' => $model,
            'settingsForm' => $settingsForm,
            'uploadImageForm' => $uploadImageForm
        ]);
    }

    public function actionDictionary()
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect('login.html');
        }

        $words = (new Query())
            ->select(['user_dictionary.word_id', 'word_en', 'translate'])
            ->from(UserDictionary::tableName())
            ->innerJoin(Dictionary::tableName(), 'user_dictionary.word_id = dictionary.word_id')
            ->where([
                'user_id' => Yii::$app->user->identity->user_id
            ])
            ->all();

        return $this->render('dictionary', [
            'user' => $this->getUser(),
            'words' => $words
        ]);
    }

    /**
     * @return string
     */
    public function actionTests()
    {
        $wordTest = new WordTest();

        return $this->render('tests', [
            'user' => $this->getUser(),
            'words' => $wordTest->words,
            'tests' => $wordTest->tests
        ]);
    }

    /**
     * @return string
     */
    public function actionReferrals()
    {
        return $this->render('referral', [
            'user' => $this->getUser()
        ]);
    }
    /**
     * @return string
     */
    public function actionProgress()
    {
        return $this->render('progress', [
            'user' => $this->getUser()
        ]);
    }

    /**
     * @return string
     */
    public function actionSubscription()
    {
        return $this->render('subscription', [
            'user' => $this->getUser()
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect('view.html');
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect('view.html');
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionSignup()
    {
        $model = new SignupForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {

                // Отправляем письмо на подтверждения почты
                Yii::$app->mailer->compose('confirm-email', [
                                        'user' => $user,
                                        'password' => $model->password
                                    ])
                                    ->setFrom([ Yii::$app->params['noreplyEmail'] => Yii::$app->name ])
                                    ->setTo($user->email)
                                    ->setSubject('Пожалуйста, подтвердите свою регистрацию')
                                    ->send();

                if (Yii::$app->getUser()->login($user)) {

                    Yii::$app->session->setFlash('successMessage',
                        '<p>Мы отправили Вам письмо со ссылкой для подтверждения.<br>Пожалуйста, проверьте почту.</p>');

                    return $this->redirect(['view']);
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionRemoveAvatar()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $user = $this->findModel(Yii::$app->user->identity->user_id);

        $user->removeAvatar();

        return $this->redirect(['view']);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('successMessage', 'Check your email for further instructions.');
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('successMessage', 'New password was saved.');

            if (Yii::$app->user->getIsGuest()) {
                Yii::$app->user->login($model->getUser());
            }

            return $this->redirect('view.html');
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     *
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionConfirmEmail($id)
    {
        $user = User::findOne([
            'user_id' => User::getUserIdFromToken($id)
        ]);

        // todo: Исправить
        if ($user === null || ($user->getEmailToken() !== $id)) {
            throw new NotFoundHttpException('Страницы не существует.');
        }

        $user->email_status = User::EMAIL_CONFIRMED;
        $user->save(false);

        //
        if (Yii::$app->user->isGuest) {
            Yii::$app->user->login($user);
        } elseif (Yii::$app->user->identity->email != $user->email) {
            Yii::$app->user->logout();
            Yii::$app->user->login($user);
        }

        $message = '<strong>Регистрация успешно подтверждена</strong><br><br>' .
            'Ваша регистрация на ' . Yii::$app->name . ' успешно подтверждена. Спасибо за использование нашего сайта!';

        Yii::$app->session->setFlash('successMessage', $message);
        $this->redirect(['/profile/view']);
    }

    /**
     * @return User
     * @throws NotFoundHttpException
     */
    public function getUser()
    {
        if (Yii::$app->user->getIsGuest()) {
            return $this->redirect('login.html');
        }

        return $this->findModel(Yii::$app->user->identity->user_id);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
