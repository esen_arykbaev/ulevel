<?php

namespace frontend\controllers;

use common\models\Episode;
use common\models\Genre;
use common\models\MovieGenre;
use frontend\models\WordTest;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\Pagination;

use common\models\UserDictionary;
use common\models\Movie;

use frontend\models\MovieSearch;
use yii\web\User;
use frontend\components\FrontendController;


/**
 * MovieController implements the CRUD actions for Movie model.
 */
class MovieController extends FrontendController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Movie models.
     * @return mixed
     */
    public function actionIndex()
    {
        $request    = Yii::$app->request;
        $query      = MovieSearch::getQuery($request->queryParams);

        $countQuery = clone $query;

        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 20]);
        $pages->defaultPageSize = 20;

        $movies = $query->orderBy('created_at DESC')
                    ->offset($pages->offset)
                    ->limit($pages->limit)
                    ->all();

        if (MovieSearch::$isQuerySearch) {
            $foundCount = $query->count();
        } else {
            $foundCount = 0;
        }

        return $this->render('index', [
            'movies' => $movies,
            'level' => MovieSearch::$level,
            'levelId' => MovieSearch::$levelId,
            'isQuerySearch' => MovieSearch::$isQuerySearch,
            'foundCount' => $foundCount,
            'pages' => $pages
        ]);
    }

    /**
     * Displays a single Movie model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $userDictionaryCount = 0;

        $model = $this->findModel($id);

        $genres = [];
        $allSeasons = [];

        if (!Yii::$app->user->isGuest) {
            $userDictionaryCount = UserDictionary::getUserDictionaryCount(Yii::$app->user->identity->user_id);
        }

        if ($model->movie_type == Movie::MOVIE_TYPE_FILM) {
            $view = 'view';
        } else {

            $seasons = Episode::find()
                        ->select(['season'])
                        ->groupBy(['season'])
                        ->where([
                            'movie_id' => $model->movie_id
                        ])
                        ->all();

            $sortedSeasons = [];

            foreach ($seasons as $season) {
                $sortedSeasons[] = $season->season;
            }

            sort($sortedSeasons, SORT_NUMERIC);

            $allSeasons = [];

            foreach ($sortedSeasons as $key) {
                $allSeasons[$key] = Episode::findAll([
                    'season' => $key,
                    'movie_id' => $model->movie_id
                ]);
            }

            $_genres = Genre::find()
                        ->select(['genre_name'])
                        ->innerJoin(MovieGenre::tableName(), 'genre.genre_id = movie_genre.genre_id')
                        ->where([
                            'movie_id' => $model->movie_id
                        ])
                        ->all();

            foreach ($_genres as $genre) {
                $genres[] = $genre->genre_name;
            }

            $view = 'view_serial';

        }

        // Увеличиваем счетчик на единицу
        $model->viewed = (int)$model->viewed + 1;
        $model->save(false);

        $wordTest = new WordTest();

        return $this->render($view, [
            'model' => $model,
            'genres' => $genres,
            'allSeasons' => $allSeasons,
            'userDictionaryCount' => $userDictionaryCount,
            'words' => $wordTest->words,
            'tests' => $wordTest->tests
        ]);
    }

    /**
     * Creates a new Movie model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Movie();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->movie_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Movie model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->movie_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the Movie model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Movie the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Movie::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
