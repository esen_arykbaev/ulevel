<?php
namespace frontend\controllers;

use Yii;
use frontend\models\ContactForm;
use yii\web\Controller;

use frontend\components\FrontendController;


/**
 * Site controller
 */
class SiteController extends FrontendController
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = 'main-index';

        $contactForm = new ContactForm();

        if (Yii::$app->request->isPost) {

            if ($contactForm->load(Yii::$app->request->post()) && $contactForm->validate()) {
                if ($contactForm->sendEmail(Yii::$app->params['adminEmail'])) {
                    Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
                } else {
                    Yii::$app->session->setFlash('error', 'There was an error sending email.');
                }
                return $this->refresh('#contact');
            }

        }

        return $this->render('index', [
            'contactForm' => $contactForm
        ]);
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionLynchie()
    {
        return $this->render('lynchie');
    }

    public function actionHappy()
    {
        return $this->render('happy');
    }

    public function actionBusiness()
    {
        return $this->render('business');
    }

    public function actionTravel()
    {
        return $this->render('travel');
    }

    public function actionMedicine()
    {
        return $this->render('medicine');
    }

    public function actionTechnology()
    {
        return $this->render('technology');
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh('#contact');
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @return string
     */
    public function actionLevel()
    {
        return $this->render('level');
    }

    /**
     * @return string
     */
    public function actionLang()
    {
        $lang = Yii::$app->request->get('lang');

        if (in_array($lang, Yii::$app->params['languages'])) {

            Yii::$app->language = $lang;
            Yii::$app->session->set('lang', $lang);

        }

        // NULL может быть когда пользователь ввел адрес экшена в браузер
        if (Yii::$app->request->referrer !== null) {
            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->goHome();
    }
}
