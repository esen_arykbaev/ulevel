<?php

namespace frontend\controllers;

use common\models\Dictionary;
use common\models\UserDictionary;
use frontend\models\Translator;
use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\helpers\Json;
use yii\base\Action;

use common\models\User;


/**
 * Class AjaxController
 * @package app\controllers
 */
class AjaxController extends Controller
{
    const DEFAULT_ERROR_MESSAGE = 'Произошла ошибка. Пожалуйста, обновите страницу и попробуйте еще раз';

    /**
     * @var array
     */
    public static $responseDatas = [
        'error' => 0,
        'message' => ''
    ];

    /**
     * @var bool
     */
    public static $error = false;

    /**
     * Выполняется перед вызовом метода контроллера
     * @param Action $action
     * @return bool|Response
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        if (!Yii::$app->request->isAjax) {
            return $this->goHome();
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        return true;
    }

    /**
     * @param int $error
     */
    public static function setError($error = 0)
    {
        self::$responseDatas['error'] = $error;
    }

    /**
     * @param string $message
     */
    public static function setMessage($message = '')
    {
        self::$responseDatas['message'] = $message;
    }

    /**
     * @param $var
     * @param $value
     */
    public static function addResponseVar($var, $value)
    {
        self::$responseDatas[$var] = $value;
    }

    /**
     * @return string
     */
    public static function getResponse()
    {
        return Json::encode(self::$responseDatas);
    }

    /**
     * @return string
     */
    public function actionTranslate()
    {
        $request = Yii::$app->request;

        $word = $request->post('word', null);
        $source = $request->post('source', null);
        $target = $request->post('target', null);

        if ($word === null || !in_array($source, Translator::SUPPORTED_LANGUAGES)
            || !in_array($target, Translator::SUPPORTED_LANGUAGES)) {

            self::setError(1);

            return self::getResponse();
        }

        Translator::$word = mb_strtolower( trim($word) );
        Translator::$sourceLang = $source;
        Translator::$targetLang = $target;

        Translator::translateWord();

        self::addResponseVar('word', Translator::$word);
        self::addResponseVar('translatedWord', Translator::$translatedWord);

        return self::getResponse();
    }

    public function actionAddWord()
    {
        if (Yii::$app->user->isGuest) {
            self::setError(1);
            return self::getResponse();
        }

        $request = Yii::$app->request;

        $word = $request->post('word', null);
        $translate = $request->post('translate', null);

        if ($word === null || $translate === null) {

            self::setError(1);

            return self::getResponse();
        }

        $dictionary = Dictionary::findOne([
            'word_en' => $word
        ]);

        if ($dictionary === null) {
            self::setError(2);

            return self::getResponse();
        }

        $exe = UserDictionary::findOne([
            'word_id' => $dictionary->word_id
        ]);

        if ($exe !== null) {
            return self::getResponse();
        }

        $userDictionary = new UserDictionary();
        $userDictionary->user_id = Yii::$app->user->identity->user_id;
        $userDictionary->word_id = $dictionary->word_id;
        $userDictionary->translate = $translate;

        if (!$userDictionary->save()) {
            self::setError(3);

            return self::getResponse();
        }

        return self::getResponse();
    }

    /**
     * @return string
     */
    public function actionRemoveWord()
    {
        if (Yii::$app->user->isGuest) {
            self::setError(1);
            return self::getResponse();
        }

        $request = Yii::$app->request;

        $wordId = $request->post('wordId', 0);

        UserDictionary::deleteAll([
            'user_id' => Yii::$app->user->identity->user_id,
            'word_id' => $wordId
        ]);

        return self::getResponse();

    }

    /**
     * @return string
     */
    public function actionSaveWord()
    {
        if (Yii::$app->user->isGuest) {
            self::setError(1);
            return self::getResponse();
        }

        $request = Yii::$app->request;

        $wordId = $request->post('wordId', 0);
        $translate = $request->post('translate', '');

        $params = [
            ':translate' => $translate,
            ':user_id' => Yii::$app->user->identity->user_id,
            ':word_id' => $wordId
        ];

        Yii::$app->db->createCommand("UPDATE user_dictionary SET translate=:translate WHERE user_id=:user_id AND word_id=:word_id")
            ->bindValues($params)
            ->execute();

        return self::getResponse();

    }

    /**
     * Отправляет код подтверждения на почту
     * @return array|Response
     */
    public function actionResendEmailConfirm()
    {
        if (Yii::$app->user->isGuest) {
            self::setError(1);
            return self::getResponse();
        }

        $user = User::findOne([
            'user_id' => Yii::$app->user->identity->user_id
        ]);

        if ($user !== null) {
            // Отправляем письмо на подтверждения почты
            Yii::$app->mailer->compose('confirm-email-resend', [ 'user' => $user ])
                                ->setFrom([ Yii::$app->params['noreplyEmail'] => Yii::$app->name ])
                                ->setTo($user->email)
                                ->setSubject('Запрос подтверждения регистрации')
                                ->send();

            self::setError(0);
            self::setMessage('Код подтверждение успешно отправлено на Вашу электронную почту.');
        } else {
            self::setError(1);
            self::setMessage('Произошла ошибка. Пользователь не найден');
        }

        return self::getResponse();
    }
}