<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->registerJsFile('@web/assets/js/goodshare.min.js', ['depends' => 'frontend\assets\AppAsset']);

$this->title = 'Английский для путешествий онлайн - Ulevel.co';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Сегодня многие выбирают английский для путешествий онлайн, чтобы быстро и без напряжения научиться беглому
                        разговорному языку, который позволит чувствовать себя уверенно среди иностранцев.'
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'английский для путешествий, английский для путешествий, английский для путешествий - ulevel.co'
]);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about container">
    <div class="row" style="margin-top:20px">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ul class="breadcrumb">
                <li><a href="/site/level.html"><i class="fa fa-home"></i></a></li>
                <li class="active">Английский для путешествий</li>
            </ul>
        </div>
        <div class="col-xs-12 col-md-5 sign">
            <img src="/images/travel.png" alt="Надежда Счастливая" class="img-responsive img-thumbnail"/>
        </div>
        <div class="col-xs-12 col-md-7 sign">
            <h1><i class="fa fa-plane" aria-hidden="true"></i> Английский для путешествий</h1>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title">Добро пожаловать на канал Английский для путешествий!</h1>
                </div>
                <div class="panel-body">
                    <p>
                        Сегодня многие выбирают английский для путешествий онлайн, чтобы быстро и без напряжения научиться беглому
                        разговорному языку, который позволит чувствовать себя уверенно среди иностранцев.
                        <br/>
                        Сейчас, как никогда, существует множество возможностей для поездок в самые экзотические страны
                        с древней и богатой культурой.
                        <br/>
                        Посмотреть все их красоты и богатства без языка попросту нереально.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <style>
        .thumbnail .caption{
            font-size: 95%;
        }
        #episodes .cover-bg{
            bottom: 79px;
        }
        h1{margin-top: 0px;}
    </style>
    <div class="row" id="episodes">
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/203" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/98/203_4f9a5.jpg" alt='Розовая пантера - Аэропорт Охрана' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">2:05</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Розовая пантера - Аэропорт Охрана</h4>
                        <h5 itemprop="alternativeHeadline">he Pink Panther  - Airport Security </h5>
                       
                    </div>
                </a>
                <meta itemprop="name" content='Розовая пантера - Аэропорт Охрана'>
                <meta itemprop="thumbnailUrl " content="/images/movies/98/203_4f9a5.jpg">
                <link itemprop="image" href="/images/movies/98/203_4f9a5.jpg">
                <meta itemprop="duration" content="2:05">
                <meta itemprop="interactionCount" content="UserViews:14" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/204" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/82/204_82b96.jpg" alt='Розовая пантера - "Я хотел быкупить гамбургер"' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">2:39</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Розовая пантера - "Я хотел быкупить гамбургер"</h4>
                        <h5 itemprop="alternativeHeadline">Pink Panther - " I Would like to buy a Hamburger"</h5>

                    </div>
                </a>
                <meta itemprop="name" content='Розовая пантера - "Я хотел быкупить гамбургер"'>
                <meta itemprop="thumbnailUrl " content="/images/movies/82/204_82b96.jpg">
                <link itemprop="image" href="/images/movies/82/204_82b96.jpg">
                <meta itemprop="duration" content="2:39">
                <meta itemprop="interactionCount" content="UserViews:15" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/205" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/f2/205_32493.jpg" alt='Друзья - В аэропорту' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">1:42</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Друзья - В аэропорту</h4>
                        <h5 itemprop="alternativeHeadline">Friends - At the airport</h5>

                    </div>
                </a>
                <meta itemprop="name" content='Друзья - В аэропорту'>
                <meta itemprop="thumbnailUrl " content="/images/movies/f2/205_32493.jpg">
                <link itemprop="image" href="/images/movies/f2/205_32493.jpg">
                <meta itemprop="duration" content="1:42">
                <meta itemprop="interactionCount" content="UserViews:24" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/206" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/1e/206_d2a1e.jpg" alt='Как взять такси в Нью-Йорке' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">3:39</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Как взять такси в Нью-Йорке</h4>
                        <h5 itemprop="alternativeHeadline">How to Take a Cab in New York City</h5>

                    </div>
                </a>
                <meta itemprop="name" content='Как взять такси в Нью-Йорке'>
                <meta itemprop="thumbnailUrl " content="/images/movies/1e/206_d2a1e.jpg">
                <link itemprop="image" href="/images/movies/1e/206_d2a1e.jpg">
                <meta itemprop="duration" content="3:39">
                <meta itemprop="interactionCount" content="UserViews:10" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/207" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/e6/207_fe90f.jpg" alt='Как пройти через проверку безопасности аэропорта' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">2:49</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Как пройти через проверку безопасности аэропорта</h4>
                        <h5 itemprop="alternativeHeadline">How to Get through Airport Security</h5>

                    </div>
                </a>
                <meta itemprop="name" content='Как пройти через проверку безопасности аэропорта'>
                <meta itemprop="thumbnailUrl " content="/images/movies/e6/207_fe90f.jpg">
                <link itemprop="image" href="/images/movies/e6/207_fe90f.jpg">
                <meta itemprop="duration" content="2:49">
                <meta itemprop="interactionCount" content="UserViews:9" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/208" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/b7/208_d0a95.jpg" alt='Как справиться с авиаперелетом' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">1:38</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Как справиться с авиаперелетом</h4>
                        <h5 itemprop="alternativeHeadline">How to Cope with Air Travel</h5>

                    </div>
                </a>
                <meta itemprop="name" content='Как справиться с авиаперелетом'>
                <meta itemprop="thumbnailUrl " content="/images/movies/b7/208_d0a95.jpg">
                <link itemprop="image" href="/images/movies/b7/208_d0a95.jpg">
                <meta itemprop="duration" content="1:38">
                <meta itemprop="interactionCount" content="UserViews:7" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/209" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/f0/209_47cf3.jpg" alt='Как обращаться с водителем такси в чужой стране' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">2:32</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Как обращаться с водителем такси в чужой стране</h4>
                        <h5 itemprop="alternativeHeadline">How to Handle a Taxi Driver in a Foreign Country</h5>

                    </div>
                </a>
                <meta itemprop="name" content='Как обращаться с водителем такси в чужой стране'>
                <meta itemprop="thumbnailUrl " content="/images/movies/f0/209_47cf3.jpg">
                <link itemprop="image" href="/images/movies/f0/209_47cf3.jpg">
                <meta itemprop="duration" content="2:32">
                <meta itemprop="interactionCount" content="UserViews:17" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/210" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/c2/210_67887.jpg" alt='Как общаться с местными жителями в отпуск' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">1:24</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Как общаться с местными жителями в отпуск</h4>
                        <h5 itemprop="alternativeHeadline">How to Meet Locals on Vacation</h5>

                    </div>
                </a>
                <meta itemprop="name" content='Как общаться с местными жителями в отпуск'>
                <meta itemprop="thumbnailUrl " content="/images/movies/c2/210_67887.jpg">
                <link itemprop="image" href="/images/movies/c2/210_67887.jpg">
                <meta itemprop="duration" content="1:24">
                <meta itemprop="interactionCount" content="UserViews:12" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/211" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/a7/211_1a78d.jpg" alt='Как получить максимум от отдыха в городе' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">1:36</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Как получить максимум от отдыха в городе</h4>
                        <h5 itemprop="alternativeHeadline">How to Get the Most from a City Vacation</h5>

                    </div>
                </a>
                <meta itemprop="name" content='Как получить максимум от отдыха в городе'>
                <meta itemprop="thumbnailUrl " content="/images/movies/a7/211_1a78d.jpg">
                <link itemprop="image" href="/images/movies/a7/211_1a78d.jpg">
                <meta itemprop="duration" content="1:36">
                <meta itemprop="interactionCount" content="UserViews:20" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/212" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/b4/212_fae3c.jpg" alt='Как путешествовать недорого' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">1:51</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Как путешествовать недорого</h4>
                        <h5 itemprop="alternativeHeadline">How to Travel Cheaply</h5>

                    </div>
                </a>
                <meta itemprop="name" content='Как путешествовать недорого'>
                <meta itemprop="thumbnailUrl " content="/images/movies/b4/212_fae3c.jpg">
                <link itemprop="image" href="/images/movies/b4/212_fae3c.jpg">
                <meta itemprop="duration" content="1:51">
                <meta itemprop="interactionCount" content="UserViews:16" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
    </div>
</div>
