<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'О нас - Ulevel.co - английский язык онлайн бесплатно';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Как узнать о сайте ulevel.co - английский язык онлайн бесплатно'
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'узнать о сайте, о сайте ulevel.co, о нас - ulevel.co'
]);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about container">
    <div class="col-md-7 sign">
        <h1><i class="fa fa-rocket" aria-hidden="true"></i> О нас</h1>

        <h3>Добро пожаловать на Ulevel!</h3>

        <p>Ulevel - это место, где любой желающий имеет прекрасную возможность выучить Английский язык, повысить уровень и расширить знания языка занимаясь и развлекаясь одновременно.<br />
            <br/>
            Смотрите любимые фильмы, клипы, сериалы, отрывки из фильмов и учите разговорные фразы добавляя их в собственную библиотеку слов и тренируя в наших упражнениях. <br />Кроме того, вы можете использовать свои знания на практике и развивать их общаясь со своими друзьями по всему миру!
            <br/>
            <br/>
            Наслаждайтесь обучением вместе с Ulevel!</p>
    </div>
</div>
