<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->registerJsFile('@web/assets/js/goodshare.min.js', ['depends' => 'frontend\assets\AppAsset']);

$this->title = 'Английский от Доктора Хауса онлайн - Ulevel.co';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Просмотр сериала о гениальном докторе вряд ли пригодится будущим врачам,
                        желающим продолжить свою карьеру где-нибудь за пределами родины. '
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'английский от Доктора Хауса, английский House M D, House M D - ulevel.co'
]);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about container">
    <div class="row" style="margin-top:20px">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ul class="breadcrumb">
                <li><a href="/site/level.html"><i class="fa fa-home"></i></a></li>
                <li class="active">Английский от Доктора Хауса</li>
            </ul>
        </div>
        <div class="col-xs-12 col-md-5 sign">
            <img src="/images/house.png" alt="Доктора Хауса" class="img-responsive img-thumbnail"/>
        </div>
        <div class="col-xs-12 col-md-7 sign">
            <h1><i class="fa fa-h-square" aria-hidden="true"></i> Английский от Доктора Хауса</h1>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title">Добро пожаловать на канал Английский от Доктора Хауса!</h1>
                </div>
                <div class="panel-body">
                    <p>
                        Просмотр сериала о гениальном докторе может пригодится будущим врачам,
                        желающим продолжить свою карьеру где-нибудь за пределами родины. <br/>
                        Однако будет однозначно полезен тем, кто хочет пополнить свою копилку вариантами
                        искрометных шуточек в стиле знаменитого мизантропа. Кроме того, от сезона к сезону
                        темы сериала выходят далеко за рамки чистой медицины, так что набор полезных словечек
                        на тему «Я и страдания моей души» вы получите в полном объеме.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <style>
        .thumbnail .caption{
            font-size: 95%;
        }
        #episodes .cover-bg{
            bottom: 79px;
        }
        h1{margin-top: 0px;}
    </style>
    <div class="row" id="episodes">
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/215" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/c8/215_3663d.jpg" alt='Химиотерапия По словам доктора Грегори Хауса' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">1:34</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Химиотерапия По словам доктора Грегори Хауса</h4>
                        <h5 itemprop="alternativeHeadline">Chemotherapy According to Dr. Gregory House</h5>

                    </div>
                </a>
                <meta itemprop="name" content='Химиотерапия По словам доктора Грегори Хауса'>
                <meta itemprop="thumbnailUrl " content="/images/movies/c8/215_3663d.jpg">
                <link itemprop="image" href="/images/movies/c8/215_3663d.jpg">
                <meta itemprop="duration" content="1:34">
                <meta itemprop="interactionCount" content="UserViews:24" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/214" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/62/214_201ca.jpg" alt='Доктор Хаус против Anti-vaxxer' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">1:45</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Доктор Хаус против Anti-vaxxer</h4>
                        <h5 itemprop="alternativeHeadline">House vs. Anti-vaxxer</h5>

                    </div>
                </a>
                <meta itemprop="name" content='Доктор Хаус против Anti-vaxxer'>
                <meta itemprop="thumbnailUrl " content="/images/movies/62/214_201ca.jpg">
                <link itemprop="image" href="/images/movies/62/214_201ca.jpg">
                <meta itemprop="duration" content="1:45">
                <meta itemprop="interactionCount" content="UserViews:13" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/213" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/0e/213_8f260.jpg" alt='Доктор Хаус - Люди не меняются' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">9:20</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Доктор Хаус - Люди не меняются</h4>
                        <h5 itemprop="alternativeHeadline">House MD - People Don't Change</h5>

                    </div>
                </a>
                <meta itemprop="name" content='Доктор Хаус - Люди не меняются'>
                <meta itemprop="thumbnailUrl " content="/images/movies/0e/213_8f260.jpg">
                <link itemprop="image" href="/images/movies/0e/213_8f260.jpg">
                <meta itemprop="duration" content="9:20">
                <meta itemprop="interactionCount" content="UserViews:22" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/156" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/1c/156_d91d4.jpg" alt='Мозг с Дэвидом Иглман Кто контролирует ' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">11:34</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Мозг с Дэвидом Иглман '' Кто контролирует ''</h4>
                        <h5 itemprop="alternativeHeadline">The Brain With David Eagleman ''Who Is in Control''</h5>
                        
                    </div>
                </a>
                <meta itemprop="name" content='Мозг с Дэвидом Иглман '' Кто контролирует '''>
                <meta itemprop="thumbnailUrl " content="/images/movies/1c/156_d91d4.jpg">
                <link itemprop="image" href="/images/movies/1c/156_d91d4.jpg">
                <meta itemprop="duration" content="11:34">
                <meta itemprop="interactionCount" content="UserViews:38" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
    </div>
</div>
