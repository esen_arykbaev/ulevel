<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->registerJsFile('@web/assets/js/goodshare.min.js', ['depends' => 'frontend\assets\AppAsset']);

$this->registerCssFile('@web/assets/build/mediaelementplayer.min.css', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('@web/assets/build/mediaelement-and-player.min.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('@web/assets/build/mep-feature-jumpforward.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('@web/assets/build/mep-feature-universalgoogleanalytics.js', ['depends' => 'frontend\assets\AppAsset']);

$this->registerCssFile('@web/css/application.css', ['depends' => 'frontend\assets\AppAsset']);

$this->title = 'HappyHope - Киношкола английского языка Надежды Счастливой - Ulevel.co';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Обучаем разговорному английскому языку без учебников - только по фильмам и сериалам онлайн! Киношкола английского языка Надежды Счастливой онлайн'
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'Киношкола английского языка, Надежда Счастливая,  Надежда Счастливая - ulevel.co'
]);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about container">
    <div class="row" style="margin-top:20px">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ul class="breadcrumb">
                <li><a href="/site/level.html"><i class="fa fa-home"></i></a></li>
                <li class="active">HappyHope</li>
            </ul>
        </div>
        <div class="col-xs-12 col-md-5 sign">
            <img src="/images/happy.jpg" alt="Надежда Счастливая" class="img-responsive img-thumbnail"/>
        </div>
        <div class="col-xs-12 col-md-7 sign">
            <h1><i class="fa fa-graduation-cap" aria-hidden="true"></i> HappyHope</h1>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title">Добро пожаловать на HappyHope!</h1>
                </div>
                <div class="panel-body">
                    <p>
                        Киношкола английского языка Надежды Счастливой <br />
                        Обучаем разговорному английскому языку без учебников - только по фильмам и сериалам!
                    </p>
                </div>
            </div>
        </div>
    </div>
    <style>
        .thumbnail .caption{
            font-size: 95%;
        }
        #episodes .cover-bg{
            bottom: 79px;
        }
    </style>
    <div class="row" id="episodes">
        <div class="video">
            <video id="player" width="750" height="420" preload="none" controls="controls">
                <source type="video/mp4" src="/videos/echo.mp4" />
            </video>
        </div>
        <div class="lightbox">
            <a href="#toggle" class="toggle">&blacktriangledown;&nbsp; Подсказка</a>
            <div class="text"><i>Включите микрофон и повторите слово</i></div>
            <label>
                <b>Повторите слово HOME</b>
            </label>
            <label>
                <input type="text" name="c1" placeholder="Здесь слово HOME">
            </label>
            <div><a href="#resume" class="resume">Дальше</a></div>
        </div>

        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/188" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/17/188_6901f.jpg" alt='Blackberry Nights ( часть I) - Разбор фильма для кинокурса "Разбериха"' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">28:02</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Blackberry Nights ( часть I) - Разбор фильма для кинокурса "Разбериха"</h4>
                        <h5 itemprop="alternativeHeadline">Blackberry Nights ( часть I) - Разбор фильма для кинокурса "Разбериха"</h5>
                        <!--<h6><i class="fa fa-eye"></i> 10</h6>-->
                    </div>
                </a>
                <meta itemprop="name" content='Blackberry Nights ( часть I) - Разбор фильма для кинокурса "Разбериха"'>
                <meta itemprop="thumbnailUrl " content="/images/movies/17/188_6901f.jpg">
                <link itemprop="image" href="/images/movies/17/188_6901f.jpg">
                <meta itemprop="duration" content="28:02">
                <meta itemprop="interactionCount" content="UserViews:10" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/187" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/dd/187_94ddc.jpg" alt='My Blueberry Nights - Разбор фильма - кинокурс Разбериха' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">46:46</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">My Blueberry Nights - Разбор фильма - кинокурс Разбериха</h4>
                        <h5 itemprop="alternativeHeadline">My Blueberry Nights - Разбор фильма - кинокурс Разбериха</h5>
                        <!--<h6><i class="fa fa-eye"></i> 10</h6>-->
                    </div>
                </a>
                <meta itemprop="name" content='My Blueberry Nights - Разбор фильма - кинокурс Разбериха'>
                <meta itemprop="thumbnailUrl " content="/images/movies/dd/187_94ddc.jpg">
                <link itemprop="image" href="/images/movies/dd/187_94ddc.jpg">
                <meta itemprop="duration" content="39:05">
                <meta itemprop="interactionCount" content="UserViews:10" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/189" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/ab/189_8ab00.jpg" alt='Modern Family (Часть I) для Simple System' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">59:00</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Modern Family (Часть I) для Simple System</h4>
                        <h5 itemprop="alternativeHeadline">Modern Family (Часть I) для Simple System</h5>
                        <!--<h6><i class="fa fa-eye"></i> 10</h6>-->
                    </div>
                </a>
                <meta itemprop="name" content='Modern Family (Часть I)  для Simple System'>
                <meta itemprop="thumbnailUrl " content="/images/movies/ab/189_8ab00.jpg">
                <link itemprop="image" href="/images/movies/ab/189_8ab00.jpg">
                <meta itemprop="duration" content="59:00">
                <meta itemprop="interactionCount" content="UserViews:10" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/190" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/b1/190_cb47b.jpg" alt='My Blueberry Nights - Разбор фильма - кинокурс Разбериха' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">1:12:17</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Laugh your way to a better marriage (Часть I) Пофразовый разбор семинара</h4>
                        <h5 itemprop="alternativeHeadline">Laugh your way to a better marriage (Часть I) Пофразовый разбор семинара</h5>
                        <!--<h6><i class="fa fa-eye"></i> 10</h6>-->
                    </div>
                </a>
                <meta itemprop="name" content='Laugh your way to a better marriage (Часть I) Пофразовый разбор семинара'>
                <meta itemprop="thumbnailUrl " content="/images/movies/dd/187_94ddc.jpg">
                <link itemprop="image" href="/images/movies/b1/190_cb47b.jpg">
                <meta itemprop="duration" content="1:12:17">
                <meta itemprop="interactionCount" content="UserViews:10" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/186" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/23/186_84896.jpg" alt='Peppa Pig - Видеоразбор (c граммультиками)' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">1:23:57</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Peppa Pig - Видеоразбор (c граммультиками)</h4>
                        <h5 itemprop="alternativeHeadline">Peppa Pig - Видеоразбор (c граммультиками)</h5>
                        <!--<h6><i class="fa fa-eye"></i> 10</h6>-->
                    </div>
                </a>
                <meta itemprop="name" content='Peppa Pig - Видеоразбор (c граммультиками)'>
                <meta itemprop="thumbnailUrl " content="/images/movies/23/186_84896.jpg">
                <link itemprop="image" href="/images/movies/23/186_84896.jpg">
                <meta itemprop="duration" content="39:05">
                <meta itemprop="interactionCount" content="UserViews:10" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/185" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/33/185_3c9e9.jpg" alt='Методика "Мерседес". Надежда Счастливая. ' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">1:23:57</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Методика "Мерседес". Надежда Счастливая. </h4>
                        <h5 itemprop="alternativeHeadline">Методика "Мерседес". Надежда Счастливая. </h5>
                        <!--<h6><i class="fa fa-eye"></i> 10</h6>-->
                    </div>
                </a>
                <meta itemprop="name" content='Методика "Мерседес". Надежда Счастливая. '>
                <meta itemprop="thumbnailUrl " content="/images/movies/33/185_3c9e9.jpg">
                <link itemprop="image" href="/images/movies/33/185_3c9e9.jpg">
                <meta itemprop="duration" content="04:45">
                <meta itemprop="interactionCount" content="UserViews:10" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>

        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/184" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/8d/184_deef4.jpg" alt='Английские времена за 30 минут в ТОРТЕ?!' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">31:16</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Английские времена за 30 минут в ТОРТЕ?! </h4>
                        <h5 itemprop="alternativeHeadline">Английские времена за 30 минут в ТОРТЕ?!</h5>
                        <!--<h6><i class="fa fa-eye"></i> 10</h6>-->
                    </div>
                </a>
                <meta itemprop="name" content='Английские времена за 30 минут в ТОРТЕ?!'>
                <meta itemprop="thumbnailUrl " content="/images/movies/8d/184_deef4.jpg">
                <link itemprop="image" href="/images/movies/8d/184_deef4.jpg">
                <meta itemprop="duration" content="04:45">
                <meta itemprop="interactionCount" content="UserViews:10" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>

        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/183" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/ee/183_e1774.jpg" alt='9 формул для общения в трех временах! КАК СТРОИТЬ ПРЕДЛОЖЕНИЯ И ВОПРОСЫ?' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">51:45</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">9 формул для общения в трех временах! КАК СТРОИТЬ ПРЕДЛОЖЕНИЯ И ВОПРОСЫ?</h4>
                        <h5 itemprop="alternativeHeadline">9 формул для общения в трех временах! КАК СТРОИТЬ ПРЕДЛОЖЕНИЯ И ВОПРОСЫ?</h5>
                        <!--<h6><i class="fa fa-eye"></i> 10</h6>-->
                    </div>
                </a>
                <meta itemprop="name" content='9 формул для общения в трех временах! КАК СТРОИТЬ ПРЕДЛОЖЕНИЯ И ВОПРОСЫ?'>
                <meta itemprop="thumbnailUrl " content="/images/movies/ee/183_e1774.jpg">
                <link itemprop="image" href="/images/movies/ee/183_e1774.jpg">
                <meta itemprop="duration" content="04:45">
                <meta itemprop="interactionCount" content="UserViews:10" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>

        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/182" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/b5/182_032cc.jpg" alt='DID или HAVE DONE? Как правильно употреблять английские времена ' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">1:19:40</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">DID или HAVE DONE? Как правильно употреблять английские времена </h4>
                        <h5 itemprop="alternativeHeadline">DID или HAVE DONE? Как правильно употреблять английские времена </h5>
                        <!--<h6><i class="fa fa-eye"></i> 10</h6>-->
                    </div>
                </a>
                <meta itemprop="name" content='DID или HAVE DONE? Как правильно употреблять английские времена'>
                <meta itemprop="thumbnailUrl " content="/images/movies/b5/182_032cc.jpg">
                <link itemprop="image" href="/images/movies/b5/182_032cc.jpg">
                <meta itemprop="duration" content="04:45">
                <meta itemprop="interactionCount" content="UserViews:10" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>

    </div>
</div>
