<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Обратная связь - ulevel.co английский язык онлайн бесплатно';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Обратная связь на сайте ulevel.co - английский язык онлайн бесплатно'
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'обратная связь сайта, обратная связь ulevel.co, контакты - ulevel.co'
]);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact sign address container">
    <h1><i class="fa fa-envelope-o"></i> <?= Yii::t('app', 'Обратная связь') ?></h1>

    <p>
        <?= Yii::t('app', 'Если у вас есть идея, пожелания, деловое предложение или другие вопросы, пожалуйста, напишите нам. Спасибо.') ?>
    </p>

    <div class="row">
        <div class="col-lg-5">
            <?php if (Yii::$app->session->hasFlash('success')): ?>
                <div id="#contact" class="alert alert-success"><?= Yii::$app->session->getFlash('success') ?></div>
            <?php elseif (Yii::$app->session->hasFlash('error')): ?>
                <div id="#contact" class="alert alert-danger"><?= Yii::$app->session->getFlash('error') ?></div>
            <?php endif; ?>
            <?php $form = ActiveForm::begin([
                'enableClientScript' => false,
                'options' => [
                    'class' => 'contact-form'
                ]
            ]); ?>

                <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>

                <?= $form->field($model, 'reCaptcha')->widget(
                    \himiklab\yii2\recaptcha\ReCaptcha::className(),
                    ['siteKey' => '6LdtuRwTAAAAAAN26oc1tX5UtAH0-mi_UXg2cP8a']
                ) ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Отправить'), ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
