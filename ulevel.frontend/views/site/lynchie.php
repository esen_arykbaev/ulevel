<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->registerJsFile('@web/assets/js/goodshare.min.js', ['depends' => 'frontend\assets\AppAsset']);

$this->title = 'Lynchie English - это весёлый и оригинальный способ выучить английский язык - Ulevel.co';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Создатель проекта Alex Lynch не только мгновенно заинтересует материалом и развлечёт по полной программе, да ещё и научит такому, о чём вы даже понятия не имели! Смотрите и учитесь в удовольствие! '
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'Lynchie English, о Lynchie English ulevel.co, Lynchie English - ulevel.co'
]);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about container">
    <div class="row" style="margin-top:20px">
        <div class="col-xs-12 col-md-5 sign">
            <img src="/images/lynchie.png" alt="Lynchie English" class="img-responsive img-thumbnail"/>

        </div>
        <div class="col-xs-12 col-md-7 sign">
            <h1><i class="fa fa-rocket" aria-hidden="true"></i> Lynchie English</h1>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title">Добро пожаловать на Lynchie English!</h1>
                </div>
                <div class="panel-body">
                    <p>Lynchie English - это весёлый и оригинальный способ выучить английский язык. <br />
                        Создатель проекта Alex Lynch не только мгновенно заинтересует материалом и развлечёт по полной программе,
                        да ещё и научит такому, о чём вы даже понятия не имели!
                        <br />
                        Смотрите и учитесь в удовольствие! </p>
                </div>
            </div>
        </div>
    </div>
    <style>
        .thumbnail .caption{
            font-size: 95%;
        }
        #episodes .cover-bg{
            bottom: 79px;
        }
    </style>
    <div class="row" id="episodes">
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/179" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/46/179_3cfe6.jpg" alt='Lynchie English #10: WHERE DO I GO?!' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">04:45</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Lynchie English #10: WHERE DO I GO?!</h4>
                        <h5 itemprop="alternativeHeadline">Lynchie English #10: WHERE DO I GO?!</h5>
                        <!--<h6><i class="fa fa-eye"></i> 10</h6>-->
                    </div>
                </a>
                <meta itemprop="name" content='Lynchie English #10: WHERE DO I GO?!'>
                <meta itemprop="thumbnailUrl " content="/images/movies/46/179_3cfe6.jpg">
                <link itemprop="image" href="/images/movies/46/179_3cfe6.jpg">
                <meta itemprop="duration" content="04:45">
                <meta itemprop="interactionCount" content="UserViews:10" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/178" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/1d/178_0e2c5.jpg" alt='Lynchie English #9: Ложные Друзья Переводчика (часть 2)' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">09:44</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Lynchie English #9: Ложные Друзья Переводчика (часть 2)</h4>
                        <h5 itemprop="alternativeHeadline">Lynchie English #9: Ложные Друзья Переводчика (часть 2)</h5>
                        <!--<h6><i class="fa fa-eye"></i> 10</h6>-->
                    </div>
                </a>
                <meta itemprop="name" content='Lynchie English #9: Ложные Друзья Переводчика (часть 2)'>
                <meta itemprop="thumbnailUrl " content="/images/movies/1d/178_0e2c5.jpg">
                <link itemprop="image" href="/images/movies/1d/178_0e2c5.jpg">
                <meta itemprop="duration" content="09:44">
                <meta itemprop="interactionCount" content="UserViews:10" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/177" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/94/177_13a9a.jpg" alt='Lynchie English #8: ТОП-13 ОШИБОК В АНГЛИЙСКОМ' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">04:37</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Lynchie English #8: ТОП-13 ОШИБОК В АНГЛИЙСКОМ</h4>
                        <h5 itemprop="alternativeHeadline">Lynchie English #8: ТОП-13 ОШИБОК В АНГЛИЙСКОМ</h5>
                        <!--<h6><i class="fa fa-eye"></i> 10</h6>-->
                    </div>
                </a>
                <meta itemprop="name" content='Lynchie English #8: ТОП-13 ОШИБОК В АНГЛИЙСКОМ'>
                <meta itemprop="thumbnailUrl " content="/images/movies/94/177_13a9a.jpg">
                <link itemprop="image" href="/images/movies/94/177_13a9a.jpg">
                <meta itemprop="duration" content="04:37">
                <meta itemprop="interactionCount" content="UserViews:14" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/176" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/fe/176_3620f.jpg" alt='Lynchie English #7: Ложные Друзья Переводчика (часть 1)' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">10:31</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Lynchie English #7: Ложные Друзья Переводчика (часть 1)</h4>
                        <h5 itemprop="alternativeHeadline">Lynchie English #7: Ложные Друзья Переводчика (часть 1)</h5>
                        <!--<h6><i class="fa fa-eye"></i> 10</h6>-->
                    </div>
                </a>
                <meta itemprop="name" content='Lynchie English #7: Ложные Друзья Переводчика (часть 1)'>
                <meta itemprop="thumbnailUrl " content="/images/movies/fe/176_3620f.jpg">
                <link itemprop="image" href="/images/movies/fe/176_3620f.jpg">
                <meta itemprop="duration" content="10:31">
                <meta itemprop="interactionCount" content="UserViews:13" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/175" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/60/175_9f453.jpg" alt='Lynchie English #6: Суффикс -ISH, Национальный вопрос и многое другое!' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">05:02</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Lynchie English #6: Суффикс -ISH, Национальный вопрос и многое другое!</h4>
                        <h5 itemprop="alternativeHeadline">Lynchie English #6: Суффикс -ISH, Национальный вопрос и многое другое!</h5>
                        <!--<h6><i class="fa fa-eye"></i> 10</h6>-->
                    </div>
                </a>
                <meta itemprop="name" content='Lynchie English #6: Суффикс -ISH, Национальный вопрос и многое другое!'>
                <meta itemprop="thumbnailUrl " content="/images/movies/60/175_9f453.jpg">
                <link itemprop="image" href="/images/movies/60/175_9f453.jpg">
                <meta itemprop="duration" content="05:02">
                <meta itemprop="interactionCount" content="UserViews:18" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/174" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/16/174_02616.jpg" alt='Lynchie English #5: I'm (being) smart?' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">03:28</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Lynchie English #5: I'm (being) smart?</h4>
                        <h5 itemprop="alternativeHeadline">Lynchie English #5: I'm (being) smart?</h5>
                        <!--<h6><i class="fa fa-eye"></i> 10</h6>-->
                    </div>
                </a>
                <meta itemprop="name" content='Lynchie English #5: I'm (being) smart?'>
                <meta itemprop="thumbnailUrl " content="/images/movies/16/174_02616.jpg">
                <link itemprop="image" href="/images/movies/16/174_02616.jpg">
                <meta itemprop="duration" content="03:28">
                <meta itemprop="interactionCount" content="UserViews:8" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/173" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/59/173_9f059.jpg" alt='Lynchie English #4: Восклицания и междометия' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">04:50</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Lynchie English #4: Восклицания и междометия</h4>
                        <h5 itemprop="alternativeHeadline">Lynchie English #4: Восклицания и междометия</h5>
                        <!--<h6><i class="fa fa-eye"></i> 10</h6>-->
                    </div>
                </a>
                <meta itemprop="name" content='Lynchie English #4: Восклицания и междометия'>
                <meta itemprop="thumbnailUrl " content="/images/movies/59/173_9f059.jpg">
                <link itemprop="image" href="/images/movies/59/173_9f059.jpg">
                <meta itemprop="duration" content="04:50">
                <meta itemprop="interactionCount" content="UserViews:17" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/172" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/d3/172_56d10.jpg" alt='Lynchie English #3: (THE) SHIT?' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">03:29</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Lynchie English #3: (THE) SHIT?</h4>
                        <h5 itemprop="alternativeHeadline">Lynchie English #3: (THE) SHIT?</h5>
                        <!--<h6><i class="fa fa-eye"></i> 10</h6>-->
                    </div>
                </a>
                <meta itemprop="name" content='Lynchie English #3: (THE) SHIT?'>
                <meta itemprop="thumbnailUrl " content="/images/movies/d3/172_56d10.jpg">
                <link itemprop="image" href="/images/movies/d3/172_56d10.jpg">
                <meta itemprop="duration" content="03:29">
                <meta itemprop="interactionCount" content="UserViews:13" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/171" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/c5/171_3347b.jpg" alt='Lynchie English #2: Useful Adjectives' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">01:09</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Lynchie English #2: Useful Adjectives</h4>
                        <h5 itemprop="alternativeHeadline">Lynchie English #2: Useful Adjectives</h5>
                        <!--<h6><i class="fa fa-eye"></i> 10</h6>-->
                    </div>
                </a>
                <meta itemprop="name" content='Lynchie English #2: Useful Adjectives'>
                <meta itemprop="thumbnailUrl " content="/images/movies/c5/171_3347b.jpg">
                <link itemprop="image" href="/images/movies/c5/171_3347b.jpg">
                <meta itemprop="duration" content="01:09">
                <meta itemprop="interactionCount" content="UserViews:30" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/170" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/bd/170_c9aa3.jpg" alt='Lynchie English #1: FULL - FOOL, FOOT - FOOD, PULL - POOL, BOOK - BOOT?' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">03:53</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Lynchie English #1: FULL - FOOL, FOOT - FOOD, PULL - POOL, BOOK - BOOT?</h4>
                        <h5 itemprop="alternativeHeadline">Lynchie English #1: FULL - FOOL, FOOT - FOOD, PULL - POOL, BOOK - BOOT? </h5>
                        <!--<h6><i class="fa fa-eye"></i> 10</h6>-->
                    </div>
                </a>
                <meta itemprop="name" content='Lynchie English #1: FULL - FOOL, FOOT - FOOD, PULL - POOL, BOOK - BOOT?'>
                <meta itemprop="thumbnailUrl " content="/images/movies/bd/170_c9aa3.jpg">
                <link itemprop="image" href="/images/movies/bd/170_c9aa3.jpg">
                <meta itemprop="duration" content="03:53">
                <meta itemprop="interactionCount" content="UserViews:24" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
    </div>
</div>
