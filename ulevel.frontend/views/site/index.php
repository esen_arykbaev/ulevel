<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $contactForm frontend\models\ContactForm */

$this->registerCssFile('@web/css/landing-style.min.css', ['depends' => 'frontend\assets\AppAsset']);

$this->title = 'Учить разговорный английский онлайн по фильмам и сериалам с субтитрами и с онлайн переводчиком бесплатно';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Изучать разговорный английский язык онлайн бесплатно по фильмам и сериалам с субтитрами и с онлайн переводчиком'
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'разговорный английский онлайн, английский язык, английский язык бесплатно, переводчик онлайн'
]);
?>
<section id="top" class="bg">
    <div class="header-info text-center">
        <div class="container">
            <h1><label><?= Yii::t('app', 'Улучшай') ?></label> <?= Yii::t('app', 'свой разговорный') ?> <label><?= Yii::t('app', 'английский') ?></label></h1>
            <span class="liner"></span>
            <p><?= Yii::t('app', 'Смотря фильмы и сериалы') ?></p>
            <?= Html::a(Yii::t('app', 'Перейти к обучению'), ['site/level'], ['class' => 'big-btn']) ?>
            <span class="text"><?= Yii::t('app', 'Узнать подробнее') ?></span>
            <a class="down-arrow down-arrow-to scroll" href="#process"><i class="fa fa-angle-down"></i></a>
        </div>
    </div>
</section>
<section id="process" class="process">
    <div class="container">
        <div class="text-center">
            <h3><?= Yii::t('app', 'Как это') ?> <span><?= Yii::t('app', 'работает?') ?></span></h3>
            <p><?= Yii::t('app', 'Всего') ?> <span><?= Yii::t('app', '4 шага') ?></span> <?= Yii::t('app', 'до успеха') ?></p>
        </div>
        <div class="process-grid">
            <img src="/images/process.jpg" alt="process" title="process" class="img-responsive"/>
        </div>
    </div>
</section>
<section id="about" class="about">
    <div class="container">
        <div class="about-head text-center">
            <h3><?= Yii::t('app', 'Ваши') ?> <span><?= Yii::t('app', 'помощники') ?></span></h3>
            <p><span><?= Yii::t('app', 'просто') ?></span> <?= Yii::t('app', 'и') ?> <span><?= Yii::t('app', 'удобно') ?></span></p>
        </div>
        <div class="about-grids">
            <div class="col-xs-6 col-sm-3 col-md-3">
                <div class="about-grid text-center">
                    <span class="about-icon1"> </span>
                    <h3><?= Yii::t('app', 'Переводчик') ?><br /><label> </label></h3>
                    <p><?= Yii::t('app', 'Переводите слова') ?></p><p><?= Yii::t('app', 'не напрягаясь') ?></p>
                </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3">
                <div class="about-grid text-center">
                    <span class="about-icon2"> </span>
                    <h3><?= Yii::t('app', 'Уровни') ?><br /><label> </label></h3>
                    <p><?= Yii::t('app', 'Подбор') ?></p> <p><?= Yii::t('app', 'под Ваш уровень') ?></p>
                </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3">
                <div class="about-grid text-center">
                    <span class="about-icon3"> </span>
                    <h3><?= Yii::t('app', 'Словарь') ?><br /><label> </label></h3>
                    <p><?= Yii::t('app', 'Повышай словарь') ?>,</p> <p><?= Yii::t('app', 'добавляя слова') ?></p>
                </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3">
                <div class="about-grid text-center">
                    <span class="about-icon4"> </span>
                    <h3><?= Yii::t('app', 'Тесты') ?><br /><label> </label></h3>
                    <p><?= Yii::t('app', 'Веселые тесты') ?></p> <p><?= Yii::t('app', 'для закрепления') ?></p>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</section>
<section id="team" class="team">
    <div class="container">
        <div class="team-head text-center">
            <h4><?= Yii::t('app', 'Отзывы') ?></h4>
            <p><?= Yii::t('app', 'Что') ?> <span><?= Yii::t('app', 'думают') ?></span> <?= Yii::t('app', 'о нас') ?></p>
        </div>
        <div class="team-members text-center">
            <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="testimonials">
                    <img src="/images/t3.png" alt="Эльнура" title="Эльнура" class="img-responsive"/>
                </div>
                <blockquote class="team-member" style="padding:5px 0 0 5px">
                    <h5><b><em><?= Yii::t('app', 'Эльнура') ?> </em></b>21 <?= Yii::t('app', 'год') ?></h5>
                    <p><q><?= Yii::t('app', 'Встроенный онлайн переводчик помогает мне переводить новые слова, которые встречаются во время просмотра.') ?></q></p>
                </blockquote>
            </div>
            <div class="hidden-xs col-sm-4 col-md-4">
                <div class="testimonials">
                    <img src="/images/t2.png" alt="Айбек" title="Айбек" class="img-responsive"/>
                </div>
                <blockquote class="team-member" style="padding:5px 0 0 5px">
                    <h5><b><em><?= Yii::t('app', 'Айбек') ?> </em></b>21 <?= Yii::t('app', 'год') ?></h5>
                    <p><q><?= Yii::t('app', 'С помощью сервиса увеличил свой словарный запас, а также слуховое восприятие.') ?></q></p>
                </blockquote>
            </div>
            <div class="hidden-xs col-sm-4 col-md-4">
                <div class="testimonials">
                    <img src="/images/t1.png" alt="Айгерим" title="Айгерим" class="img-responsive"/>
                </div>
                <blockquote class="team-member" style="padding:5px 0 0 5px">
                    <h5><b><em><?= Yii::t('app', 'Айгерим') ?> </em></b>19 <?= Yii::t('app', 'лет') ?></h5>
                    <p><q><?= Yii::t('app', 'Очень крутой помощник, благодаря вам подготовила себя к экзаменам, очень удобно.') ?></q></p>
                </blockquote>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</section>
<section id="contact" class="address">
    <div class="container">
        <div class="address-head text-center">
            <h3><?= Yii::t('app', 'Обратная') ?> <span><?= Yii::t('app', 'связь') ?></span></h3>
            <p><?= Yii::t('app', 'Будьте') ?> <span><?= Yii::t('app', 'в теме') ?></span> <?= Yii::t('app', 'событий') ?></p>
        </div>
        <div class="hidden-xs col-sm-4 col-md-6">
            <h4><span><i class="fa fa-share-alt"></i></span> <span><?= Yii::t('app', 'Мы') ?></span> <?= Yii::t('app', 'в') ?> <span><?= Yii::t('app', 'соц') ?></span><?= Yii::t('app', 'иальных') ?> <span><?= Yii::t('app', 'сетях') ?></span></h4>
            <script type="text/javascript" src="//vk.com/js/api/openapi.js?121"></script>
            <!-- VK Widget -->
            <div id="vk_groups"></div>
            <script type="text/javascript">
                VK.Widgets.Group("vk_groups", {mode: 0, width: 'auto', height: '400', color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 87263842);
            </script>
        </div>
        <div class="col-xs-12 col-sm-8 col-md-6">
            <h4 class="h4"><span><i class="fa fa-edit"></i> <?= Yii::t('app', 'Напишите') ?></span> <?= Yii::t('app', 'нам') ?></h4>

            <?php if (Yii::$app->session->hasFlash('success')): ?>
                <div id="#contact" class="alert alert-success"><?= Yii::$app->session->getFlash('success') ?></div>
            <?php elseif (Yii::$app->session->hasFlash('error')): ?>
                <div id="#contact" class="alert alert-danger"><?= Yii::$app->session->getFlash('error') ?></div>
            <?php endif; ?>

            <?php $form = ActiveForm::begin([
                'enableClientScript' => false,
                'options' => [
                    'class' => 'contact-form'
                ]
            ]); ?>

            <?= $form->field($contactForm, 'name')->textInput(['required' => true]) ?>

            <?= $form->field($contactForm, 'email')->input('email', ['required' => true]) ?>

            <?= $form->field($contactForm, 'body')->textArea(['required' => true, 'rows' => 5, 'cols' => 30]) ?>

            <?= $form->field($contactForm, 'reCaptcha')->widget(
                \himiklab\yii2\recaptcha\ReCaptcha::className(),
                ['siteKey' => '6LdtuRwTAAAAAAN26oc1tX5UtAH0-mi_UXg2cP8a']
            ) ?>

            <div class="form-group text-center">
                <?= Html::submitButton(Yii::t('app', 'Отправить'), ['class' => 'btn', 'name' => 'contact-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</section>
<footer>
    <div class="footer text-center">
        <div class="footer-logo">
            <img src="/images/footer-logo.png" alt="logo" />
        </div>
        <div class="footer-menu">
            <div>
                <a href="#" class="scroll" title="Главная"><?= Yii::t('app', 'Главная') ?></a>
                <br/>
                <a href="/site/level.html" title="Уровни начинающий">Level</a>
            </div>
            <div>
                <a href="#about" class="scroll" title="Ваши помощники"><?= Yii::t('app', 'Ваши помощники') ?></a>
                <br/>
                <a href="#team" class="scroll" title="Отзывы"><?= Yii::t('app', 'Отзывы') ?></a>
            </div>
            <div>
                <a href="#contact" class="scroll" title="Обратная связь"><?= Yii::t('app', 'Обратная связь') ?></a>
                <br/>
                <a href="#process" class="scroll" title="Как это работает?"><?= Yii::t('app', 'Как это работает?') ?></a>
            </div>
        </div>
        <?php if (Yii::$app->user->isGuest): ?>
            <div class="footer-sign-in">
                <?= Html::a(Yii::t('app', 'Войти'), ['profile/login'], ['class' => 'btn btn-primary', 'title' => 'Войти']) ?>
                <?= Html::a(Yii::t('app', 'Регистрация'), ['profile/signup'], ['class' => 'btn btn-primary', 'title' => 'Регистрация']) ?>
            </div>
        <?php endif; ?>
        <div class="clearfix"></div>
    </div>
</footer>
