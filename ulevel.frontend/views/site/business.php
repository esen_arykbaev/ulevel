<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->registerJsFile('@web/assets/js/goodshare.min.js', ['depends' => 'frontend\assets\AppAsset']);

$this->title = 'Бизнес английский или деловой английский онлайн - Ulevel.co';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Business English или Деловой английский поможет улучшить ваш Английский язык. Для вас подготовлен делового английского для изучающих английский со среднего уровня (intermediate) и выше, и для тех, кто хочет использовать английский для работы. Т.к. для этого курса вы должны уже понимать английский на достаточном уровне, дальше видео будет только на английском.'
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'business English, бизнес английский,  деловой английский - ulevel.co'
]);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about container">
    <div class="row" style="margin-top:20px">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ul class="breadcrumb">
                <li><a href="/site/level.html"><i class="fa fa-home"></i></a></li>
                <li class="active">Бизнес английский</li>
            </ul>
        </div>
        <div class="col-xs-12 col-md-5 sign">
            <img src="/images/business.png" alt="Бизнес английский" class="img-responsive img-thumbnail"/>
        </div>
        <div class="col-xs-12 col-md-7 sign">
            <h1><i class="fa fa-cubes" aria-hidden="true"></i> Бизнес английский</h1>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title">Добро пожаловать на канал Бизнес английский!</h1>
                </div>
                <div class="panel-body">
                    <p>
                        Business English или Деловой английский поможет улучшить ваш Английский язык.<br/>
                        Для вас подготовлен делового английского для изучающих английский со среднего уровня (intermediate) и выше, и для тех, кто хочет использовать английский для работы. Т.к. для этого курса вы должны уже понимать английский на достаточном уровне, дальше видео будет только на английском.
                        <br/>
                        То, что нужно для серьезных людей и деловых бесед.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <style>
        .thumbnail .caption{
            font-size: 95%;
        }
        #episodes .cover-bg{
            bottom: 79px;
        }
        h1{margin-top: 0px;}
    </style>
    <div class="row" id="episodes">
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/193" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/21/193_21f42.jpg" alt='Бизнес английский - Эпизод 1: Рад встретиться с вами' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">10:02</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Бизнес английский - Эпизод 1: Рад встретиться с вами</h4>
                        <h5 itemprop="alternativeHeadline">The Business of English - Episode 1: Pleased to meet you</h5>
                        
                    </div>
                </a>
                <meta itemprop="name" content='Бизнес английский - Эпизод 1: Рад встретиться с вами'>
                <meta itemprop="thumbnailUrl " content="/images/movies/21/193_21f42.jpg">
                <link itemprop="image" href="/images/movies/21/193_21f42.jpg">
                <meta itemprop="duration" content="10:02">
                <meta itemprop="interactionCount" content="UserViews:10" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/194" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/3d/194_dd17f.jpg" alt='Бизнес английский - Эпизод 2: Почему бы вам не присоединиться к нам?' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">10:05</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Бизнес английский - Эпизод 2: Почему бы вам не присоединиться к нам?</h4>
                        <h5 itemprop="alternativeHeadline">The Business of English - Episode 2: Why don't you join us?</h5>

                    </div>
                </a>
                <meta itemprop="name" content='Бизнес английский - Эпизод 2: Почему бы вам не присоединиться к нам?'>
                <meta itemprop="thumbnailUrl " content="/images/movies/3d/194_dd17f.jpg">
                <link itemprop="image" href="/images/movies/3d/194_dd17f.jpg">
                <meta itemprop="duration" content="10:05">
                <meta itemprop="interactionCount" content="UserViews:8" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/195" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/ff/195_ff27e.jpg" alt='Бизнес английский - Эпизод 3: Ознакомление' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">10:11</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Бизнес английский - Эпизод 3: Ознакомление</h4>
                        <h5 itemprop="alternativeHeadline">The Business of English - Episode 3: Getting Acquainted</h5>

                    </div>
                </a>
                <meta itemprop="name" content='Бизнес английский - Эпизод 3: Ознакомление'>
                <meta itemprop="thumbnailUrl " content="/images/movies/ff/195_ff27e.jpg">
                <link itemprop="image" href="/images/movies/ff/195_ff27e.jpg">
                <meta itemprop="duration" content="10:11">
                <meta itemprop="interactionCount" content="UserViews:4" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/196" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/86/196_02597.jpg" alt='Бизнес английский - Эпизод 4: Любые другие вопросы?' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">10:11</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Бизнес английский - Эпизод 4: Любые другие вопросы?</h4>
                        <h5 itemprop="alternativeHeadline">The Business of English - Episode 4: Any other business?</h5>

                    </div>
                </a>
                <meta itemprop="name" content='Бизнес английский - Эпизод 4: Любые другие вопросы?'>
                <meta itemprop="thumbnailUrl " content="/images/movies/86/196_02597.jpg">
                <link itemprop="image" href="/images/movies/86/196_02597.jpg">
                <meta itemprop="duration" content="10:11">
                <meta itemprop="interactionCount" content="UserViews:5" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/197" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/1f/197_82193.jpg" alt='Бизнес английский - Эпизод 5: Выслушайте! Выслушайте!' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">10:18</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Бизнес английский - Эпизод 5: Выслушайте! Выслушайте!</h4>
                        <h5 itemprop="alternativeHeadline">The Business of English - Episode 5: Hear! Hear!</h5>

                    </div>
                </a>
                <meta itemprop="name" content='Бизнес английский - Эпизод 5: Выслушайте! Выслушайте!'>
                <meta itemprop="thumbnailUrl " content="/images/movies/1f/197_82193.jpg">
                <link itemprop="image" href="/images/movies/1f/197_82193.jpg">
                <meta itemprop="duration" content="10:18">
                <meta itemprop="interactionCount" content="UserViews:9" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/198" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/61/198_83c32.jpg" alt='Бизнес английский - Эпизод 6: Какие варианты?' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">10:20</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Бизнес английский - Эпизод 6: Какие варианты?</h4>
                        <h5 itemprop="alternativeHeadline">The Business of English - Episode 6: What are the options?</h5>

                    </div>
                </a>
                <meta itemprop="name" content='Бизнес английский - Эпизод 6: Какие варианты?'>
                <meta itemprop="thumbnailUrl " content="/images/movies/61/198_83c32.jpg">
                <link itemprop="image" href="/images/movies/61/198_83c32.jpg">
                <meta itemprop="duration" content="10:20">
                <meta itemprop="interactionCount" content="UserViews:4" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/199" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/b0/199_74ae6.jpg" alt='Бизнес английский - Эпизод 7: Отчет о прогрессе' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">10:01</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Бизнес английский - Эпизод 7: Отчет о прогрессе</h4>
                        <h5 itemprop="alternativeHeadline">The Business of English - Episode 7: A report on progress</h5>

                    </div>
                </a>
                <meta itemprop="name" content='Бизнес английский - Эпизод 7: Отчет о прогрессе'>
                <meta itemprop="thumbnailUrl " content="/images/movies/b0/199_74ae6.jpg">
                <link itemprop="image" href="/images/movies/b0/199_74ae6.jpg">
                <meta itemprop="duration" content="10:01">
                <meta itemprop="interactionCount" content="UserViews:6" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/200" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/97/200_2a149.jpg" alt='Бизнес английский - Эпизод 8: Графики и тенденции' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">10:12</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Бизнес английский - Эпизод 8: Графики и тенденции</h4>
                        <h5 itemprop="alternativeHeadline">The Business of English - Episode 8: Graphs and trends</h5>

                    </div>
                </a>
                <meta itemprop="name" content='Бизнес английский - Эпизод 8: Графики и тенденции'>
                <meta itemprop="thumbnailUrl " content="/images/movies/97/200_2a149.jpg">
                <link itemprop="image" href="/images/movies/97/200_2a149.jpg">
                <meta itemprop="duration" content="10:12">
                <meta itemprop="interactionCount" content="UserViews:5" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/201" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/45/201_c450f.jpg" alt='Бизнес английский - Эпизод 9: Опрос клиентов' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">10:17</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Бизнес английский - Эпизод 9: Опрос клиентов</h4>
                        <h5 itemprop="alternativeHeadline">The Business of English - Episode 9: A customer survey </h5>

                    </div>
                </a>
                <meta itemprop="name" content='Бизнес английский - Эпизод 9: Опрос клиентов'>
                <meta itemprop="thumbnailUrl " content="/images/movies/45/201_c450f.jpg">
                <link itemprop="image" href="/images/movies/45/201_c450f.jpg">
                <meta itemprop="duration" content="10:17">
                <meta itemprop="interactionCount" content="UserViews:5" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/202" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/76/202_87684.jpg" alt='Бизнес английский -  Эпизод 10:Подводя итоги' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">10:17</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Бизнес английский -  Эпизод 10:Подводя итоги</h4>
                        <h5 itemprop="alternativeHeadline">The Business of English - Episode 10: Wrapping it up</h5>

                    </div>
                </a>
                <meta itemprop="name" content='Бизнес английский -  Эпизод 10:Подводя итоги'>
                <meta itemprop="thumbnailUrl " content="/images/movies/76/202_87684.jpg">
                <link itemprop="image" href="/images/movies/76/202_87684.jpg">
                <meta itemprop="duration" content="10:17">
                <meta itemprop="interactionCount" content="UserViews:7" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
    </div>
</div>
