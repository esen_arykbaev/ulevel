<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->registerJsFile('@web/assets/js/goodshare.min.js', ['depends' => 'frontend\assets\AppAsset']);

$this->title = 'IT английский онлайн - Ulevel.co';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Эта программа поможет слушателям даже с начинающим уровнем владения языком получить
                        необходимый уровень языковой компетенции для работы в международной IT-компании,
                        уверенного общения с зарубежными коллегами на профессиональные темы,
                        чтения оригинальной литературы.'
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'IT английский, английский IT , английский IT - ulevel.co'
]);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about container">
    <div class="row" style="margin-top:20px">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ul class="breadcrumb">
                <li><a href="/site/level.html"><i class="fa fa-home"></i></a></li>
                <li class="active">IT английский</li>
            </ul>
        </div>
        <div class="col-xs-12 col-md-5 sign">
            <img src="/images/it.png" alt="IT английский" class="img-responsive img-thumbnail"/>
        </div>
        <div class="col-xs-12 col-md-7 sign">
            <h1><i class="fa fa-hashtag" aria-hidden="true"></i> IT английский</h1>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title">Добро пожаловать на канал IT английский!</h1>
                </div>
                <div class="panel-body">
                    <p>
                        Эта программа поможет слушателям даже с начинающим уровнем владения языком получить
                        необходимый уровень языковой компетенции для работы в международной IT-компании,
                        уверенного общения с зарубежными коллегами на профессиональные темы,
                        чтения оригинальной литературы.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <style>
        .thumbnail .caption{
            font-size: 95%;
        }
        #episodes .cover-bg{
            bottom: 79px;
        }
        h1{margin-top: 0px;}
    </style>
    <div class="row" id="episodes">
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/152" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/bf/152_83749.jpg" alt='TED - Сноуден о том, как мы Вернуть Интернет ' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">35:17</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">TED - Сноуден о том, как мы Вернуть Интернет </h4>
                        <h5 itemprop="alternativeHeadline">TED - Edward Snowden on How We Take Back the Internet</h5>
                      
                    </div>
                </a>
                <meta itemprop="name" content='TED - Сноуден о том, как мы Вернуть Интернет '>
                <meta itemprop="thumbnailUrl " content="/images/movies/bf/152_83749.jpg">
                <link itemprop="image" href="/images/movies/bf/152_83749.jpg">
                <meta itemprop="duration" content="35:17">
                <meta itemprop="interactionCount" content="UserViews:42" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/151" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/14/151_2d003.jpg" alt='TED - Бионика, прыжки и танцы | Хью Герр' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">19:00</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">TED - Бионика, прыжки и танцы | Хью Герр</h4>
                        <h5 itemprop="alternativeHeadline">TED - Bionics, Climb and Dance | Hugh Herr </h5>

                    </div>
                </a>
                <meta itemprop="name" content='TED - Бионика, прыжки и танцы | Хью Герр'>
                <meta itemprop="thumbnailUrl " content="/images/movies/14/151_2d003.jpg">
                <link itemprop="image" href="/images/movies/14/151_2d003.jpg">
                <meta itemprop="duration" content="19:00">
                <meta itemprop="interactionCount" content="UserViews:21" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/138" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/84/138_a680e.jpg" alt='Современные технологии полиции' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">27:46</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Современные технологии полиции</h4>
                        <h5 itemprop="alternativeHeadline">Technologies police departments </h5>

                    </div>
                </a>
                <meta itemprop="name" content='Современные технологии полиции'>
                <meta itemprop="thumbnailUrl " content="/images/movies/84/138_a680e.jpg">
                <link itemprop="image" href="/images/movies/84/138_a680e.jpg">
                <meta itemprop="duration" content="27:46">
                <meta itemprop="interactionCount" content="UserViews:28" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/131" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/49/131_49431.jpg" alt='Технологии нашего времени и будущего в сфере образовании' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">1:38</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Технологии нашего времени и будущего в сфере образовании</h4>
                        <h5 itemprop="alternativeHeadline">Technology Trends Driving the Future of Higher Education</h5>

                    </div>
                </a>
                <meta itemprop="name" content='Технологии нашего времени и будущего в сфере образовании'>
                <meta itemprop="thumbnailUrl " content="/images/movies/49/131_49431.jpg">
                <link itemprop="image" href="/images/movies/49/131_49431.jpg">
                <meta itemprop="duration" content="1:38">
                <meta itemprop="interactionCount" content="UserViews:39" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/130" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/02/130_d3905.jpg" alt='Когда родители пользуются гаджетами' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">8:14</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Когда родители пользуются гаджетами</h4>
                        <h5 itemprop="alternativeHeadline">When Parents Use Technology</h5>

                    </div>
                </a>
                <meta itemprop="name" content='Когда родители пользуются гаджетами'>
                <meta itemprop="thumbnailUrl " content="/images/movies/02/130_d3905.jpg">
                <link itemprop="image" href="/images/movies/02/130_d3905.jpg">
                <meta itemprop="duration" content="8:14">
                <meta itemprop="interactionCount" content="UserViews:34" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/51" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/9a/51_88379.jpg" alt='Пятьдесят Оттенков серо-бежевого' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">6:58</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Пятьдесят Оттенков серо-бежевого</h4>
                        <h5 itemprop="alternativeHeadline">Fifty Shades of Greige</h5>

                    </div>
                </a>
                <meta itemprop="name" content='Пятьдесят Оттенков серо-бежевого'>
                <meta itemprop="thumbnailUrl " content="/images/movies/9a/51_88379.jpg">
                <link itemprop="image" href="/images/movies/9a/51_88379.jpg">
                <meta itemprop="duration" content="6:58">
                <meta itemprop="interactionCount" content="UserViews:43" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/40" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/79/40_e00ff.png" alt='Discovery: Подводные лодки - Стальные акулы' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs">46:20</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Discovery: Подводные лодки - Стальные акулы</h4>
                        <h5 itemprop="alternativeHeadline">Discovery Channel: Submarines - Sharks Of Steel</h5>

                    </div>
                </a>
                <meta itemprop="name" content='Discovery: Подводные лодки - Стальные акулы'>
                <meta itemprop="thumbnailUrl " content="/images/movies/79/40_e00ff.png">
                <link itemprop="image" href="/images/movies/79/40_e00ff.png">
                <meta itemprop="duration" content="46:20">
                <meta itemprop="interactionCount" content="UserViews:37" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="">
                <meta itemprop="datePublished" content="">-->
            </div>
        </div>
    </div>
</div>
