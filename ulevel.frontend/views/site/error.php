<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Ошибка '. $this->title . ' . на сайте ulevel.co - английский язык онлайн бесплатно'
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $this->title. ' сайта, ошибка ulevel.co, ошибка на сайте ulevel.co'
]);
?>
<div class="site-error container">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <h4>
        <?= Yii::t('app', 'ОЙ, ПЫТАЕТЕСЬ ЧТО-ТО НАЙТИ?') ?>
    </h4>
    <p>
        <?= Yii::t('app', 'Страница, которую вы искали, не найдена. Вернитесь на') ?> <a href="/" title="главную страница"><?= Yii::t('app', 'главную') ?></a>.
    </p>

</div>
