<?php

use yii\helpers\Html;

$this->title = 'Разговорный английский язык онлайн по уровням с нуля для начинающих, средних и профессионалов';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Изучение разговорного английского языка по урокам, темам, видео по уровням с нуля для начинающих,средних и профессионалов и также фильмы, мультфильмы, сериалы в оригинале'
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'английский по уровням, уровни для начинающих,английский для начинающих, уровни intermediate'
]);
?>
<main class="site-level bg">
    <div class="header-info text-center">
        <div class="container">
            <h1><?= Yii::t('app', 'Выберите ваш')?> <label><?= Yii::t('app', 'уровень')?></label> <?= Yii::t('app', 'английского')?></h1>
            <?= Html::a(Yii::t('app', 'Начинающий'), ['movie/index', 'level' => 'beginner'], ['class' => 'big-btn btn-1']) ?>
            <?= Html::a(Yii::t('app', 'Средний'), ['movie/index', 'level' => 'intermediate'], ['class' => 'big-btn btn-2']) ?>
            <?= Html::a(Yii::t('app', 'Продвинутый'), ['movie/index', 'level' => 'advanced'], ['class' => 'big-btn btn-3']) ?>
        </div>
    </div>
</main>
