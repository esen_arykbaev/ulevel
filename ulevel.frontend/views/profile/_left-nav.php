<?php

use yii\helpers\Html;

use common\models\UserDictionary;

/* @var $user common\models\User */
/* @var $userDictionaryCount int */

$actionId = Yii::$app->controller->action->id;
$userDictionaryCount = UserDictionary::getUserDictionaryCount(Yii::$app->user->identity->user_id);
$this->registerJsFile('@web/js/profile.js', ['depends' => 'frontend\assets\AppAsset']);
?>
<div class="col-xs-12 col-sm-3 col-md-3">
    <?php if (!empty($user->avatar)): ?>
        <img src="/images/avatars/<?= $user->avatar ?>" alt="<?= $user->first_name ?>" class="img-responsive img-circle fit-cover" />
    <?php else: ?>
        <img src="/images/avatars/avatar_default.png" class="img-responsive img-circle" alt="avatar"/>
    <?php endif; ?>
    <hr />
    <h3 class="text-center"><?= $user->first_name ?></h3>
    <hr />
    <ul class="nav nav-pills nav-vertical" role="tablist">
        <li role="presentation"<?= $actionId == 'dictionary'? ' class="active"' : '' ?>>
            <?= Html::a('<i class="fa fa-book"></i>&nbsp; '. Yii::t('app', 'Мой словарь').'&nbsp;<span class="badge">'. $userDictionaryCount .'</span>', ['/profile/dictionary']) ?>
        </li>
        <li role="presentation"<?= $actionId == 'tests'? ' class="active"' : '' ?>>
            <?= Html::a('<i class="fa fa-graduation-cap" aria-hidden="true"></i> '. Yii::t('app', 'Мои тесты'), ['/profile/tests']) ?>
        </li>
        <li role="presentation"<?= $actionId == 'video'? ' class="active"' : '' ?> >
            <?= Html::a('<i class="fa fa-heart-o" aria-hidden="true"></i>&nbsp; '. Yii::t('app', 'Мои видео').'&nbsp;<span class="badge">0</span>', ['/profile/video']) ?>
        </li>
        <li role="presentation"<?= $actionId == 'referrals'? ' class="active"' : '' ?> style="display: none;">
            <?= Html::a('<i class="fa fa-user-plus"></i>&nbsp; '. Yii::t('app', 'Мои рефераллы').'&nbsp;<span class="badge">0</span>', ['/profile/referrals']) ?>
        </li>
        <li role="presentation"<?= $actionId == 'progress'? ' class="active"' : '' ?> style="display: none;">
            <?= Html::a('<i class="fa fa-line-chart"></i>&nbsp;'. Yii::t('app', 'Мой прогресс'), ['profile/progress']) ?>
        </li>
        <li role="presentation"<?= $actionId == 'subscription'? ' class="active"' : '' ?> style="display: none;">
            <?= Html::a('<i class="fa fa-usd"></i>&nbsp; '. Yii::t('app', 'Мой абонемент'), ['/profile/subscription']) ?>
        </li>
        <li role="presentation"<?= $actionId == 'view'? ' class="active"' : '' ?>>
            <?= Html::a('<i class="fa fa-user"></i>&nbsp; '. Yii::t('app', 'Мой профиль'), ['/profile/view']) ?>
        </li>
        <li role="presentation">
            <?= Html::a('<i class="fa fa-sign-out"></i>&nbsp;'. Yii::t('app', 'Выход'), ['profile/logout']) ?>
        </li>
    </ul>
</div>
