<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', 'Восстановления пароля');
$this->params['breadcrumbs'][] = $this->title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Восстановления пароля на сайте ulevel.co - английский язык онлайн'
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'восстановления пароля, восстановления пароля на сайте, восстановления пароля ulevel.co'
]);
?>
<div class="container">
    <div class="row sign">
        <div class="col-xs-10 col-sm-6 col-md-4">
            <?php if (Yii::$app->session->hasFlash('successMessage')): ?>
                <div class="alert alert-success"><?= Yii::$app->session->getFlash('successMessage') ?></div>
            <?php else: ?>
                <h1 class="text-center"><?= Html::encode($this->title) ?></h1>

                <p class="text-center"><small><?=Yii::t('app', 'Пожалуйста, введите электронную почту. Ссылка для сброса пароля будет отправлена на указанный Ваш Email.')?></small></p>

                <?php if (Yii::$app->session->hasFlash('error')): ?>
                    <div class="alert alert-danger"><?= Yii::$app->session->getFlash('error') ?></div>
                <?php endif; ?>

                <?php $form = ActiveForm::begin([
                    'id' => 'request-password-reset-form',
                    'options' => [
                        'class' => 'contact-form'
                    ]
                ]); ?>

                <?= $form->field($model, 'email')->input('email', ['autofocus' => true, 'required' => true]) ?>

                <div class="form-group text-center">
                    <?= Html::submitButton(Yii::t('app', 'Отправить'), ['class' => 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            <?php endif; ?>
        </div>
    </div>
</div>
