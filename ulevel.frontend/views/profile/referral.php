<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */
$this->title = 'Мои Рефераллы для приглашения друзей на английском языке онлайн на сайте Ulevel.co';
$this->params['breadcrumbs'][] = $this->title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Мои рефераллы для приглашения друзей на английском языке онлайн на сайте ulevel.co'
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'мои рефераллы, рефераллы на сайте, рефераллы ulevel.co'
]);
?>
<div class="container">
    <div class="profile clearfix">
        <?= $this->render('@app/views/profile/_left-nav.php', ['user' => $user]) ?>
        <div class="col-xs-12 col-sm-9 col-md-9">
            <h3>Мой рефераллы</h3>
            <div class="section">
                <div class="body">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Рефераллы</div>
                        <table class="table">
                            <tr>
                                <th>#</th>
                                <th>Имя</th>
                                <th>E-mail</th>
                                <th>Действия</th>
                            </tr>
                            <!--<tr>
                                <th scope="row">1</th>
                                <td>User</td>
                                <td>user@mail.ru</td>
                                <td>
                                    <a class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>-->
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
