<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
/* @var $words[] array */
$this->title = 'Мой словарь для изучения слов на английском языке онлайн на сайте Ulevel.co';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Мой словарь для изучения слов на английском языке онлайн на сайте ulevel.co'
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'мой словарь, словарь на сайте, словарь ulevel.co'
]);
?>
<div class="container">
    <div class="profile clearfix">
        <?= $this->render('@app/views/profile/_left-nav.php', ['user' => $user]) ?>
        <div class="col-xs-12 col-sm-9 col-md-9">
            <h3><?=Yii::t('app', 'Мой словарь')?></h3>
            <audio src="" id="speech" hidden></audio>
            <div class="section">
                <div class="body">
                    <div class="panel panel-primary">
                        <div class="panel-heading"><?=Yii::t('app', 'Библиотека')?></div>
                        <table class="table">
                            <tr>
                                <th>#</th>
                                <th style="width: 55px;"><i class="fa fa-music"></i></th>
                                <th><?=Yii::t('app', 'Слово')?></th>
                                <th><?=Yii::t('app', 'Перевод')?></th>
                                <th><?=Yii::t('app', 'Действия')?></th>
                            </tr>
                            <?php $number = 0; foreach ($words as $word): $number ++;?>
                                <tr>
                                    <th scope="row"><?=$number?></th>
                                    <td><a href="#" class="btn btn-warning say-word" data-word="<?= $word['word_en'] ?>"><i class="fa fa-volume-up"></i></a></td>
                                    <td><?= $word['word_en'] ?></td>
                                    <td id="translate-<?= $word['word_id'] ?>" data-word="<?= $word['translate'] ?>"><?= $word['translate'] ?></td>
                                    <td>
                                        <div class="form-group">
                                            <a class="btn btn-primary btn-word-edit" data-word-id="<?= $word['word_id'] ?>"><i class="fa fa-edit"></i></a>
                                            <a class="btn btn-danger btn-word-remove" data-word-id="<?= $word['word_id'] ?>"><i class="fa fa-trash-o"></i></a>
                                        </div>
                                        <div class="edit-btns-<?= $word['word_id'] ?> hide">
                                            <a class="btn btn-success btn-word-save" data-word-id="<?= $word['word_id'] ?>"><i class="fa fa-check"></i></a>
                                            <a class="btn btn-info btn-word-close" data-word-id="<?= $word['word_id'] ?>"><i class="fa fa-close"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
