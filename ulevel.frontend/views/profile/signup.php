<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\authclient\widgets\AuthChoice;

$this->title = 'Регистрация на сайте Ulevel.co';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Как зарегистрироваться на сайт ulevel.co'
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'как зарегистрироваться, регистрация на сайте, регистрация ulevel.co'
]);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="row registration">
        <div class="col-xs-12 col-sm-8 col-md-8 login">
            <h1 class="text-center"><i class="fa fa-pencil"></i> <?=Yii::t('app', 'Регистрация')?></h1>

            <?php if (Yii::$app->session->hasFlash('error')): ?>
                <div class="alert alert-danger"><?= Yii::$app->session->getFlash('error') ?></div>
            <?php endif; ?>

            <?php $form = ActiveForm::begin([
                'id' => 'form-signup',
                'enableClientScript' => false,
                'options' => [
                    'class' => 'contact-form',
                    'autocomplete' => 'off'
                ]
            ]); ?>

            <?= $form->field($model, 'firstName')->textInput(['required' => true]) ?>

            <?= $form->field($model, 'email')->input('email', ['required' => true]) ?>

            <?= $form->field($model, 'password')->passwordInput(['required' => true]) ?>

            <?= $form->field($model, 'passwordRepeat')->passwordInput(['required' => true]) ?>

            <?= $form->field($model, 'reCaptcha')->widget(
                \himiklab\yii2\recaptcha\ReCaptcha::className(),
                ['siteKey' => '6LdtuRwTAAAAAAN26oc1tX5UtAH0-mi_UXg2cP8a']
            ) ?>

            <div class="form-group text-center">
                <?= Html::submitButton(Yii::t('app', 'Регистрация'), ['class' => 'btn', 'name' => 'signup-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>

            <div class="line text-center form-group">
                <span class="line-first"></span>
                <span class="line-second"><?=Yii::t('app', 'ИЛИ')?></span>
                <span class="line-third"></span>
            </div>

            <?php $authAuthChoice = AuthChoice::begin([
                'baseAuthUrl' => ['profile/auth'],
                'popupMode' => true,
                'options' => [
                    'class' => 'social-login text-center'
                ]
            ]); ?>

            <?php foreach ($authAuthChoice->getClients() as $client): ?>
                <?php $authAuthChoice->clientLink(
                    $client,
                    '<i class="fa fa-' . $client->getName() . '"></i> '.Yii::t('app', 'Войти с помощью').' ' . $client->getTitle(),
                    ['class' => 'btn btn-social btn-' . $client->getName()]) ?>
            <?php endforeach; ?>

            <?php AuthChoice::end(); ?>

        </div>
    </div>
</div>
