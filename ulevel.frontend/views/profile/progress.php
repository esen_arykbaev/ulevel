<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */
$this->title = 'Мои прогресс на сайте Ulevel.co - английском языке онлайн';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Мои прогресс на сайте Ulevel.co - английском языке онлайн'
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'мои прогресс, прогресс на сайте, прогресс ulevel.co'
]);
$this->registerJsFile('@web/js/flot/jquery.flot.min.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('@web/js/flot/jquery.flot.test.js', ['depends' => 'frontend\assets\AppAsset']);
?>
<div class="container">
    <div class="profile clearfix">
        <?= $this->render('@app/views/profile/_left-nav.php', ['user' => $user]) ?>
        <div class="col-xs-12 col-sm-9 col-md-9">
            <h3>Мой прогресс</h3>
            <div class="section">
                <div class="body">
                    <!--<canvas id="canvas" height="450" width="600"></canvas>-->
                    <!-- Line chart -->
                    <div class="box box-primary">
                        <div class="box-body">
                            <div id="line-chart" style="height: 300px;"></div>
                        </div><!-- /.box-body-->
                    </div><!-- /.box -->
                </div>
            </div>
        </div>
    </div>
</div>
