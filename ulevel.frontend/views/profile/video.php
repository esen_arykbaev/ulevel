<?php

use yii\web\View;

/* @var $this yii\web\View */
/* @var $user common\models\User */
/* @var $words[] array */

$this->title = 'Мои избранные видео для изучения слов на английском языке онлайн на сайте Ulevel.co';

$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Мои избранные видео для изучения слов на английском языке онлайн на сайте ulevel.co'
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'мои избранные видео, видео на сайте, видео ulevel.co'
]);
?>
<div class="container">
    <div class="profile clearfix">
        <?= $this->render('@app/views/profile/_left-nav.php', ['user' => $user]) ?>
        <div class="col-xs-12 col-sm-9 col-md-9">
            <h3><?=Yii::t('app', 'Мои видео')?></h3>
            <div class="section">
                <div class="body clearfix">
                    <!-- start video widget-->
                    <div class="row" id="episodes">
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 clearfix">
                            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                                <a itemprop="url" href="/movie/view/240" class="img-link">
                                    <img itemprop="thumbnail" src="/images/movies/f0/240_48109.jpg" alt='Nooow I get it! - "Тупой и еще Тупее" (часть 1)' class="img-responsive fit-cover">
                                    <div class="cover-bg"></div>
                                    <span class="views hidden-xs" style="display: none;">7:23</span>
                                    <div class="caption text-left">
                                        <h4 itemprop="name">Nooow I get it! - "Тупой и еще Тупее" (часть 1)</h4>
                                        <h5 itemprop="alternativeHeadline">Nooow I get it! - "Тупой и еще Тупее" (часть 1)</h5>
                                        <h6><i class="fa fa-eye"></i> 23</h6>
                                    </div>
                                </a>
                                <meta itemprop="name" content='Nooow I get it! - "Тупой и еще Тупее" (часть 1)'>
                                <meta itemprop="thumbnailUrl " content="/images/movies/f0/240_48109.jpg">
                                <link itemprop="image" href="/images/movies/f0/240_48109.jpg">
                                <meta itemprop="duration" content="7:23">
                                <meta itemprop="interactionCount" content="UserViews:23" />
                                <meta itemprop="isFamilyFriendly" content="true">
                                <meta itemprop="width" content="265">
                                <meta itemprop="height" content="150">
                                <meta itemprop="license" content="СС">
                                <meta itemprop="status" content="published">
                                <!--<meta itemprop="uploadDate" content="">
                                <meta itemprop="datePublished" content="">-->
                            </div>
                        </div>
                    </div>
                    <!-- end video widget  -->
                </div>
            </div>
        </div>
    </div>
</div>
