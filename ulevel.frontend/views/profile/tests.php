<?php

use yii\web\View;

/* @var $this yii\web\View */
/* @var $user common\models\User */
/* @var $words[] array */

$this->title = 'Мои тесты для изучения слов на английском языке онлайн на сайте Ulevel.co';

$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Мои тесты для изучения слов на английском языке онлайн на сайте ulevel.co'
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'мои тесты, тесты на сайте, тесты ulevel.co'
]);

$this->registerJsFile('@web/js/highcharts.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('@web/js/exporting.js', ['depends' => 'frontend\assets\AppAsset']);

$this->registerJs(
    'Test.words = ' . json_encode($words) . ';' . 'Test.tests = ' . json_encode($tests) . ';',
    View::POS_END
);
?>
<div class="container">
    <div class="profile clearfix">
        <?= $this->render('@app/views/profile/_left-nav.php', ['user' => $user]) ?>
        <div class="col-xs-12 col-sm-9 col-md-9">
            <h3><?=Yii::t('app', 'Мои тесты')?></h3>
            <div class="section">
                <div class="body clearfix">
                    <!-- start test widget-->
                    <div id="test-one" style="margin-top: 0;">
                        <div id="test-start">
                            <p class="alert alert-info"><?=Yii::t('app', 'Пройдите тест, чтобы запомнить свои слова из словаря')?></p>
                            <button id="begin-test" class="btn btn-success"><?=Yii::t('app', 'Давайте начнем')?></button>
                        </div>
                        <aside id="test-container" class="text-center" style="width: 100%; display: none;">
                            <div id="test-container-inner">
                                <p>
                                    <small><?=Yii::t('app', 'Осталось выучить слов')?>: <span><span id="tests-left"></span>/<span id="tests-all"></span></span></small>
                                    <button type="button"
                                            data-toggle="popover"
                                            title="<?=Yii::t('app', 'Подсказка')?>"
                                            data-content="<?=Yii::t('app', 'Переводите слова для изучения')?>"
                                            data-trigger="hover"
                                            class="btn btn-default btn-sm">
                                        <i class="fa fa-info"></i>
                                    </button>
                                </p>
                                <style>
                                    #test-translations>div{
                                        display: inline-block;
                                        width: 20%;
                                        margin-right: 10px;
                                        text-align: center;
                                    }
                                    #test-translations>div:last-of-type {
                                        margin-right: 0;
                                    }
                                </style>
                                <form name="test" action="#" method="post" class="form">
                                    <div class="test-word text-center" style="margin: 15px auto;">
                                        <label id="test-word"></label>
                                    </div>
                                    <div>
                                        <div id="test-translations" class="clearfix"></div>
                                        <div class="text-center">
                                            <input type="button" name="test" value="<?=Yii::t('app', 'Завершить')?>" class="btn test-btn-success" onclick="Test.showResult();">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                            <!-- Result Chart js -->
                            <div id="result-chart" style="width: 100%; display: none;">
                                <div id="MyBar" style="min-width: 110px; height: 350px;"></div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="test-again" style="display: none;">
                                <button type="button" class="btn test-btn-primary" onclick="Test.init(); ga('send', 'event', 'shares', 'tw');">
                                    <i class="fa fa-repeat"></i>&nbsp;&nbsp;<?=Yii::t('app', 'Еще раз')?>
                                </button>
                            </div>
                        </aside>
                    </div>
                    <!--    end test widget        -->
                </div>
            </div>
        </div>
    </div>
</div>
