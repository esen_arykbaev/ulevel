<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $user common\models\User */

$this->title = 'Мой абонемент';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Мой абонемент на сайт ulevel.co'
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'мой абонемент, абонемент на сайте, абонемент ulevel.co'
]);
?>
<div class="container">
    <div class="profile clearfix">
        <?= $this->render('@app/views/profile/_left-nav.php', ['user' => $user]) ?>
        <div class="col-xs-12 col-sm-9 col-md-9">
            <h3>Абонемент</h3>
            <div class="section">
                <div class="body">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Мой абонемент</div>
                        <table class="table">
                            <tr>
                                <th>Абонемент</th>
                                <th>Истекает</th>
                                <th>Действия</th>
                            </tr>
                            <tr>
                                <td>Free</td>
                                <td>Ограниченный</td>
                                <td>
                                    <a class="btn btn-success">Обновить <i class="fa fa-refresh"></i></a>
                                    <a class="btn btn-danger">Отменить <i class="fa fa-close"></i></a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
