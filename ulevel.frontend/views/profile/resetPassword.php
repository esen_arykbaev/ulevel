<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', 'Сброс пароля');
$this->params['breadcrumbs'][] = $this->title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Сброс пароля на сайте ulevel.co - английский язык онлайн'
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'сброс пароля, сброс пароля на сайте, сброс пароля ulevel.co'
]);
?>
<div class="container">
    <div class="row sign">
        <div class="col-xs-10 col-sm-6 col-md-4">
            <h1 class="text-center"><?= Html::encode($this->title) ?></h1>

            <p class="text-center"><small><?=Yii::t('app', 'Пожалуйста, введите новый пароль')?>:</small></p>

            <?php $form = ActiveForm::begin([
                'id' => 'reset-password-form',
                'options' => [
                    'class' => 'contact-form'
                ]
            ]); ?>

            <?= $form->field($model, 'password')->passwordInput(['autofocus' => true]) ?>

            <div class="form-group text-center">
                <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

    </div>
</div>
