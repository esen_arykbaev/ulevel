<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\ActiveForm;

use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $settingsForm frontend\models\UserSettingsForm */
/* @var $uploadImageForm frontend\models\UploadImageForm */

$this->title = 'Мой профиль на английском языке онлайн на сайте Ulevel.co';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Мой профиль на английском языке онлайн на сайте ulevel.co'
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'мой профиль, профиль на сайте, профиль ulevel.co'
]);
?>
<div class="container">
    <div class="profile clearfix">
        <?= $this->render('@app/views/profile/_left-nav.php', ['user' => $model]) ?>
        <div class="col-xs-12 col-sm-9 col-md-9">
            <?php if (Yii::$app->session->hasFlash('successMessage')): ?>
                <div class="alert alert-success">
                    <?= Yii::$app->session->getFlash('successMessage') ?>
                </div>
            <?php endif; ?>
            <h3><?=Yii::t('app', 'Профиль')?></h3>
            <div class="section">
                <div class="body">

                    <?php if ($model->email_status == User::EMAIL_NOT_CONFIRMED && !Yii::$app->session->hasFlash('successMessage')): ?>
                        <div class="alert alert-info" role="alert">
                            Пожалуйста, подтвердите адрес электронной почты.<br>
                            <a href="javascript: void(0);" id="confirm-email" class="alert-link">Отправить код подтверждения на почту</a>.
                        </div>
                    <?php endif; ?>

                    <?php $uploadForm = ActiveForm::begin([
                        'enableClientScript' => false,
                        'options' => [
                            'autocomplete' => 'off',
                            'enctype' => 'multipart/form-data'
                        ]
                    ]) ?>

                    <div class="fields_group text-center">
                        <div class="field">
                            <div class="image_uploader">
                                <div class="type_avatar">
                                    <?php if (!empty($model->avatar)): ?>
                                        <img src="/images/avatars/<?= $model->avatar ?>" alt="<?= $model->first_name ?>" class="img-responsive img-circle fit-cover" />
                                    <?php else: ?>
                                        <img src="/images/avatars/avatar_default.png" class="img-responsive img-circle fit-cover" alt="avatar" />
                                    <?php endif; ?>
                                </div>
                                <br />
                                <div class="description">
                                    <b><?=Yii::t('app', 'Ваша фотография.')?></b> <?=Yii::t('app', 'Формат')?>: jpg, gif, png.<br /> <?=Yii::t('app', 'Максимальный размер файла')?>: 100kb. <?=Yii::t('app', 'Рекомендованный размер')?>: 150х150px.
                                </div>
                                <?= $uploadForm->field($uploadImageForm, 'image')->fileInput(['accept' => 'image/*'])->label(false) ?>
                                <div class="text-center">
                                    <?= Html::submitInput(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-primary']) ?>
                                    <?= Html::a('<i class="fa fa-trash-o" aria-hidden="true"></i> '. Yii::t('app', 'Удалить'), ['profile/remove-avatar'], ['class' => 'btn btn-danger', 'title' => 'Удалить изображение']) ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php ActiveForm::end(); ?>

                    <?php $form = ActiveForm::begin([
                        'enableClientScript' => false,
                        'options' => [
                            'autocomplete' => 'off'
                        ]
                    ]); ?>

                    <?= $form->field($settingsForm, 'firstName')->textInput(['required' => true]) ?>

                    <?= $form->field($settingsForm, 'aboutMe')->textarea() ?>

                    <?= $form->field($settingsForm, 'oldPassword')->passwordInput() ?>

                    <?= $form->field($settingsForm, 'newPassword')->passwordInput() ?>

                    <?= $form->field($settingsForm, 'newPasswordRepeat')->passwordInput() ?>

                    <?= Html::submitInput(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-primary']) ?>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>