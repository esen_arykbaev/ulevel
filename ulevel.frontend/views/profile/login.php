<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\authclient\widgets\AuthChoice;

$this->title = 'Войти на сайт Ulevel.co - англйский онлайн';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Как войти на сайт ulevel.co - англйский онлайн'
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'как войти, войти на сайт, сайт ulevel.co'
]);
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="container">
    <div class="row sign">
        <div class="col-xs-12 col-sm-8 col-md-8 login clearfix">
            <h1 class="text-center"><i class="fa fa-sign-in"></i> <?=Yii::t('app', 'Войти')?></h1>

            <?php if (Yii::$app->session->hasFlash('error')): ?>
            <div class="alert alert-danger"><?= Yii::$app->session->getFlash('error') ?></div>
            <?php endif; ?>

            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'enableClientScript' => false,
                'options' => [
                    'class' => 'contact-form'
                ]
            ]); ?>

            <?= $form->field($model, 'email')->input('email', ['required' => true]) ?>

            <?= $form->field($model, 'password')->passwordInput(['required' => true]) ?>

            <div class="form-group">
                <?= $form->field($model, 'rememberMe')->checkbox() ?>

                <?= Html::a(Yii::t('app','Забыли пароль?'), ['profile/request-password-reset'], ['class' => 'restore-link']) ?>
            </div>

            <div class="text-center form-group">
                <?= Html::submitButton(Yii::t('app','Войти'), ['class' => 'btn']) ?>
            </div>

            <?php ActiveForm::end(); ?>

            <div class="line text-center form-group">
                <span class="line-first"></span>
                <span class="line-second"><?=Yii::t('app', 'ИЛИ')?></span>
                <span class="line-third"></span>
            </div>

            <?php $authAuthChoice = AuthChoice::begin([
                'baseAuthUrl' => ['profile/auth'],
                'popupMode' => true,
                'options' => [
                    'class' => 'social-login text-center'
                ]
            ]); ?>

            <?php foreach ($authAuthChoice->getClients() as $client): ?>
                <?php $authAuthChoice->clientLink(
                    $client,
                    '<i class="fa fa-' . $client->getName() . '"></i> '.Yii::t('app', 'Войти с помощью').' ' . $client->getTitle(),
                    ['class' => 'btn btn-social btn-' . $client->getName()]) ?>
            <?php endforeach; ?>

            <?php AuthChoice::end(); ?>
            
        </div>
    </div>
</div>