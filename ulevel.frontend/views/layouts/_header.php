<?php

use yii\helpers\Html;

$q = isset($_GET['q']) ? Html::encode($_GET['q']) : '';
$lang = Yii::$app->language;

?>
<header>
    <nav class="navbar navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#w0-collapse">
                    <span class="sr-only"><?= Yii::t('app', 'Меню') ?></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/site/level.html" title="Уровни">
                    <img src="/images/logo.png" alt="logo" class="img-responsive"/>
                </a>
            </div>
            <div  itemscope itemtype="http://schema.org/WebSite" id="searchfor" class="navbar-form navbar-left">
                <link itemprop="url" href="https://ulevel.co/" />
                <form itemprop="potentialAction" itemscope itemtype="http://schema.org/SearchAction" action="/movie/index.html" method="get" id="searchform">
                    <div class="form-group">
                        <meta itemprop="target" content="http://ulevel.co/movie/index.html?q=<?= $q ?>"/>
                        <input itemprop="query-input" type="text" placeholder="<?= Yii::t('app', 'Поиск') ?>" value="<?= $q ?>" name="q" id="search" class="form-control" required />
                    </div>
                    <button type="submit" id="searchsubmit" class="btn"><i class="fa fa-search"></i></button>
                </form>
            </div>
            <div id="w0-collapse" class="collapse navbar-collapse">
                <ul id="w1" class="navbar-nav navbar-right nav">
                    <li><a href="/site/level.html" title="Главная"><i class="fa fa-home"></i><?= Yii::t('app', 'Главная') ?></a></li>
                    <li class="dropdown">
                        <a href="#" title="Уровни" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-sliders fa-rotate-270"></i><?= Yii::t('app', 'Уровни') ?><b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <?= Html::a('<i class="fa fa-star-o" aria-hidden="true"></i> '. Yii::t('app', 'Начинающий'), ['movie/index', 'level' => 'beginner']) ?>
                            </li>
                            <li>
                                <?= Html::a('<i class="fa fa-star-half-o" aria-hidden="true"></i> '. Yii::t('app', 'Средний'), ['movie/index', 'level' => 'intermediate']) ?>
                            </li>
                            <li>
                                <?= Html::a('<i class="fa fa-star" aria-hidden="true"></i> '. Yii::t('app', 'Продвинутый'), ['movie/index', 'level' => 'advanced']) ?>
                            </li>
                        </ul>
                    </li>
                    <?php if (Yii::$app->user->isGuest): ?>
                        <li>
                            <?= Html::a('<i class="fa fa-sign-in"></i>&nbsp; '. Yii::t('app', 'Войти'), ['profile/login']) ?>
                        </li>
                        <li>
                            <?= Html::a('<i class="fa fa-pencil"></i>&nbsp; ' . Yii::t('app', 'Регистрация'), ['profile/signup']) ?>
                        </li>
                    <?php else: ?>
                        <li class="avatar hidden-xs">
                            <?php if (Yii::$app->user->identity->avatar != ''): ?>
                                <img src="/images/avatars/<?= Yii::$app->user->identity->avatar ?>" alt="avatar - <?= Yii::$app->user->identity->first_name ?>" class="img-responsive img-circle fit-cover" />
                            <?php else: ?>
                                <img src="/images/avatars/avatar_default.png" class="img-responsive img-circle fit-cover" alt="avatar" />
                            <?php endif ?>
                        </li>
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <?= Yii::$app->user->identity->first_name ?>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <?= Html::a('<i class="fa fa-book"></i>&nbsp;'. Yii::t('app', 'Мой словарь'), ['profile/dictionary']) ?>
                                </li>
                                <li>
                                    <?= Html::a('<i class="fa fa-graduation-cap" aria-hidden="true"></i>' . Yii::t('app', 'Мои тесты'), ['profile/tests']) ?>
                                </li>
                                <li>
                                    <?= Html::a('<i class="fa fa-heart-o" aria-hidden="true"></i>&nbsp; ' . Yii::t('app', 'Мои видео'), ['profile/video']) ?>
                                </li>
                                <li style="display: none;">
                                    <?= Html::a('<i class="fa fa-usd"></i>&nbsp; ' . Yii::t('app', 'Мой абонемент'), ['profile/subscription']) ?>
                                </li>
                                <li>
                                    <?= Html::a('<i class="fa fa-user"></i>&nbsp; ' . Yii::t('app', 'Мой профиль'), ['profile/view']) ?>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <?= Html::a('<i class="fa fa-sign-out"></i>&nbsp; ' . Yii::t('app', 'Выход'), ['profile/logout']) ?>
                                </li>
                            </ul>
                        </li>
                    <?php endif; ?>
                        <li class="dropdown dropdowns">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-top: 12px;">
                                <img src="/images/<?= $lang == 'ru' ? 'ru' : 'en' ?>.png" alt="<?= $lang == 'ru' ? 'Русский' : 'English' ?>" style="vertical-align: sub;"/>
                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="/site/lang.html?lang=<?= $lang == 'ru' ? 'en' : 'ru' ?>">
                                        <img src="/images/<?= $lang == 'ru' ? 'en' : 'ru' ?>.png" alt="<?= $lang == 'ru' ? 'English' : '' ?>" />
                                        <span><?= $lang == 'ru' ? 'English' : 'Русский' ?></span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
