<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;

AppAsset::register($this);

$this->registerJsFile('@web/js/main.js', ['depends' => 'frontend\assets\AppAsset']);

$qw = common\models\Dictionary::findAll([
    'word_en' => 'of'
]);

$lang = Yii::$app->language;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
<meta charset="UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<?= Html::csrfMetaTags() ?>
<title><?= Html::encode($this->title) ?></title>
<?php $this->head() ?>
<meta property="og:title" content="<?= Html::encode($this->title) ?>"/>
<meta property="og:locale" content="ru"/>
<meta property="og:type" content="website"/>
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico"/>
<link rel="apple-touch-icon" href="/favicon.png"/>
<link rel="alternate" hreflang="ru" href="https://ulevel.co/" />
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,100,500&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap">
    <header>
        <nav id="w" class="navbar navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#w0-collapse">
                        <span class="sr-only"><?= Yii::t('app', 'Меню') ?></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">
                        <img src="/images/logo.png" alt="logo" class="img-responsive"/>
                    </a>
                </div>
                <div id="w0-collapse" class="collapse navbar-collapse">
                    <ul id="w1" class="navbar-nav navbar-right nav">
                        <li><a href="#process" class="scroll icon-1"><?= Yii::t('app', 'Как это работает?') ?></a></li>
                        <li><a href="#about" class="scroll icon-2"><?= Yii::t('app', 'Ваши помощники') ?></a></li>
                        <li><a href="#team" class="scroll icon-3"><?= Yii::t('app', 'Отзывы') ?></a></li>
                        <li><a href="#contact" class="scroll icon-4"><?= Yii::t('app', 'Обратная связь') ?></a></li>
                    <?php if (Yii::$app->user->isGuest): ?>
                        <li><a href="/profile/signup.html" class="icon-5"><?= Yii::t('app', 'Регистрация') ?></a></li>
                    <?php endif; ?>
                        <style>
                            .dropdown .dropdown-menu li a{padding-top: 5px !important;}
                        </style>
                        <li class="dropdown dropdowns">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-top: 20px;">
                                <img src="/images/<?= $lang == 'ru' ? 'ru' : 'en' ?>.png" alt="<?= $lang == 'ru' ? 'Русский' : 'English' ?>" style="vertical-align: sub;"/>
                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="/site/lang.html?lang=<?= $lang == 'ru' ? 'en' : 'ru' ?>">
                                        <img src="/images/<?= $lang == 'ru' ? 'en' : 'ru' ?>.png" alt="<?= $lang == 'ru' ? 'English' : '' ?>" />
                                        <span><?= $lang == 'ru' ? 'English' : 'Русский' ?></span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <?= $content ?>
</div>
<?php $this->endBody() ?>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-74983895-1', 'auto');
    ga('send', 'pageview');
</script>
<!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter36031850 = new Ya.Metrika({ id:36031850, clickmap:true, trackLinks:true, accurateTrackBounce:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/36031850" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
</body>
</html>
<?php $this->endPage() ?>