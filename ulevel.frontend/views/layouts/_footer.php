<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Ulevel.co 2016. <?= Yii::t('app', 'Все права защищены') ?>.</p>
        <ul class="feedback">
            <li>
                <a title="Напишите нам" href="/site/contact.html"><?= Yii::t('app', 'Напишите нам') ?> <i class="fa fa-paper-plane-o"></i></a>
            </li>
        </ul>
        <ul class="navbar-nav navbar-right nav">
            <li class="facebook">
                <a target="_blank" href="https://www.facebook.com/ulevel.co/" title="facebook.com/ulevel.co/"><i class="fa fa-facebook"></i></a>
            </li>
            <li class="google">
                <a target="_blank" href="https://plus.google.com/+Ulevelco" title="+Ulevelco"><i class="fa fa-google-plus"></i></a>
            </li>
            <li class="vk">
                <a target="_blank" href="https://vk.com/ulevelco" title="ulevelco"><i class="fa fa-vk"></i></a>
            </li>
            <li class="instagram">
                <a target="_blank" href="https://www.instagram.com/ulevel_co/" title="ulevel_co"><i class="fa fa-instagram"></i></a>
            </li>
            <li class="twitter">
                <a target="_blank" href="https://twitter.com/ulevelco" title="twitter.com/ulevelco"><i class="fa fa-twitter"></i></a>
            </li>
            <li class="ok">
                <a target="_blank" href="http://ok.ru/ulevelco" title="ok.ru/ulevelco"><i class="fa fa-odnoklassniki"></i></a>
            </li>
        </ul>
    </div>
</footer>