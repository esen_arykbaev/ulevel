<?php

use yii\widgets\Breadcrumbs;
use yii\web\View;

use common\models\Level;

/* @var $this yii\web\View */
/* @var $model common\models\Movie */
/* @var $words array */
/* @var $tests array */

$this->registerCssFile('@web/assets/build/mediaelementplayer.min.css', ['depends' => 'frontend\assets\AppAsset']);

$this->registerJsFile('@web/assets/build/mediaelement-and-player.min.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('@web/assets/build/mep-feature-jumpforward.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('@web/assets/build/mep-feature-universalgoogleanalytics.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('@web/assets/js/goodshare.min.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('@web/js/video.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('@web/js/highcharts.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('@web/js/exporting.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerCssFile('@web/css/application.css', ['depends' => 'frontend\assets\AppAsset']);

$this->registerJs(
  'Test.words = ' . json_encode($words) . ';' . 'Test.tests = ' . json_encode($tests) . ';',
  View::POS_END
);

$this->title = 'Смотреть на английском '.$model->name_ru . ' с субтитрами';
$this->params['breadcrumbs'][] = [
    'label' => Level::LEVELS[$model->level],
    'url' => ['index', 'level' => Level::LEVELS_LINK[$model->level]]
    ];
$this->params['breadcrumbs'][] = $model->name_ru;
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Английский урок на тему '. $model->name_ru . ' с английскими и русскими субтитрами, большое количество фраз, диалогов, слов, которое поможет разговорному английскому языку онлайн бесплатно'
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'урок ' .$model->name_ru . ', английский урок '. $model->name_ru .  ' , английский IT, английский медицина, английский знакомство'
]);
?>
<meta property="og:description" content="Английский урок на тему <?= $model->name_ru ?> с английскими и русскими субтитрами, большое количество фраз, диалогов, слов, которое поможет разговорному английскому языку онлайн бесплатно"/>
<main itemscope itemtype="http://schema.org/VideoObject" class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
            <?=
            Breadcrumbs::widget([
                'homeLink' => [
                    'label' => '<i class="fa fa-home"></i>',
                    'url' => '/'
                ],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                'encodeLabels' => false
            ]);
            ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
            <?php if (Yii::$app->user->isGuest): ?>
                <a href="/profile/login.html" class="notification-trigger">
                    <span><?= Yii::t('app', 'Мой словарь') ?></span> &nbsp;<i class="fa fa-book fa-2x"></i>
                    <div class="notification-num"><?= $userDictionaryCount ?></div>
                </a>
            <?php else: ?>
            <a href="/profile/dictionary.html" class="notification-trigger">
                <span><?= Yii::t('app', 'Мой словарь') ?></span> &nbsp;<i class="fa fa-book fa-2x"></i>
                <div id="dictionary-count" class="notification-num"><?= $userDictionaryCount ?></div>
            </a>
            <?php endif; ?>
        </div>
    </div>
    <div class="row" id="films">
        <article>
            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                <h1 itemprop="name" class="h1"><?= $model->name_ru ?></h1>
                <h2 itemprop="alternativeHeadline" class="h2"><?= $model->name_en ?></h2>
                <div class="video">
                    <?php if (!empty($model->youtube)): ?>
                        <link itemprop="embedUrl" href="<?= $model->youtube ?>">
                        <video id="player" width="750" height="420" preload="none" controls="controls">
                            <source type="video/youtube" src="<?= $model->youtube ?>" />
                            <?php if (!empty($model->en_subtitle)): ?>
                                <track srclang="en" label="EN" kind="subtitles" src="/subs/en/<?= $model->en_subtitle ?>" onclick="ga('send', 'event', 'subtitle', 'en');"/>
                            <?php endif; ?>
                            <?php if (!empty($model->ru_subtitle)): ?>
                                <track srclang="ru" label="RU" kind="subtitles" src="/subs/ru/<?= $model->ru_subtitle ?>" onclick="ga('send', 'event', 'subtitle', 'ru');"/>
                            <?php endif; ?>
                        </video>
                    <?php elseif (!empty($model->vimeo)): ?>
                        <link itemprop="embedUrl" href="<?= $model->vimeo ?>">
                        <video id="player" width="750" height="420"  preload="none" controls="controls">
                            <source type="video/vimeo" src="<?= $model->vimeo ?>" />
                            <?php if (!empty($model->en_subtitle)): ?>
                                <track srclang="en" label="EN" kind="subtitles" src="/subs/en/<?= $model->en_subtitle ?>" onclick="ga('send', 'event', 'subtitle', 'en');"/>
                            <?php endif; ?>
                            <?php if (!empty($model->ru_subtitle)): ?>
                                <track srclang="ru" label="RU" kind="subtitles" src="/subs/ru/<?= $model->ru_subtitle ?>" onclick="ga('send', 'event', 'subtitle', 'ru');"/>
                            <?php endif; ?>
                        </video>
                    <?php elseif (!empty($model->megogo)): ?>
                        <link itemprop="embedUrl" href="<?= $model->megogo ?>">
                        <div itemprop="embedHTML"><iframe width="750" height="420" src="<?= $model->megogo ?>" frameborder="0" allowfullscreen></iframe></div>
                        <meta itemprop="allowEmbed" content="true">
                        <meta itemprop="playerType" content="Flash">
                        <meta itemprop="width" content="1920">
                        <meta itemprop="height" content="1080">
                        <meta itemprop="videoQuality" content="full HD">
                        <meta itemprop="bitrate" content="526">
                        <meta itemprop="contentSize" content="526000">
                        <meta itemprop="availablePlatform " content="Mobile">
                        <meta itemprop="image " content="/images/movies/<?=$model->image?>">
                        <meta itemprop="uploadDate" content="<?= $model->created_at ?>">
                        <meta itemprop="datePublished" content="<?= $model->created_at ?>">
                    <?php else: ?>
                        <video id="player" width="750" height="420" preload="none" controls="controls">
                            <source type="video/mp4" src="/videos/<?= $model->local_video ?>" />
                            <?php if (!empty($model->en_subtitle)): ?>
                                <track srclang="en" label="EN" kind="subtitles" src="/subs/en/<?= $model->en_subtitle ?>" onclick="ga('send', 'event', 'subtitle', 'en');"/>
                            <?php endif; ?>
                            <?php if (!empty($model->ru_subtitle)): ?>
                                <track srclang="ru" label="RU" kind="subtitles" src="/subs/ru/<?= $model->ru_subtitle ?>" onclick="ga('send', 'event', 'subtitle', 'ru');"/>
                            <?php endif; ?>
                        </video>
                    <?php endif; ?>
                </div>
                <?php if(empty($model->megogo)) { ?>
                <div class="lightbox">
                    <a href="#toggle" class="toggle">&blacktriangledown;&nbsp; Подсказка</a>
                    <div class="text"><i>Включите микрофон и повторите слово</i></div>
                        <label>
                            <b>Тут нами заданное слово HOME</b>
                        </label>
                        <label>
                            <input type="text" name="c1" placeholder="Здесь слово HOME">
                        </label>
                    <div><a href="#resume" class="resume">Дальше</a></div>
                </div>
                <?php } ?>
                <?php if(!empty($model->megogo)) { ?>
                    <p class="alert alert-info" style="margin-top: 20px;width: 98%;"><i class="fa fa-info-circle"></i> <?= Yii::t('app', 'Для просмотра фильма в оригинале(на английском) нажмите нижнем в правом углу плеера на знак настройки') ?> <i class="fa fa-cog" aria-hidden="true"></i> <?= Yii::t('app', 'и выберите аудиодорожку и субтитр') ?>.</p>
                <?php } ?>
                <div class="social" style="width:100%;">
                    <button class="goodshare fb"
                            data-type="fb"
                            data-title="<?= $this->title ?>"
                            data-text="<?= $model->description ?>"
                            data-image="https://ulevel.co/images/movies/<?=$model->image?>"
                            onclick="ga('send', 'event', 'shares', 'fb');">
                        <i class="fa fa-facebook"></i>&nbsp;&nbsp;
                        <span data-counter="fb"></span>
                    </button>
                    <button class="goodshare gp" data-type="gp" onclick="ga('send', 'event', 'shares', 'gp');">
                        <i class="fa fa-google-plus"></i>&nbsp;&nbsp;
                        <span data-counter="gp"></span>
                    </button>
                    <button class="goodshare vk" data-type="vk" onclick="ga('send', 'event', 'shares', 'vk');">
                        <i class="fa fa-vk"></i>&nbsp;&nbsp;
                        <span data-counter="vk"></span>
                    </button>
                    <button class="goodshare ok" data-type="ok" onclick="ga('send', 'event', 'shares', 'ok');">
                        <i class="fa fa-odnoklassniki"></i>&nbsp;&nbsp;
                        <span data-counter="ok"></span>
                    </button>
                    <button class="goodshare tw" data-type="tw" onclick="ga('send', 'event', 'shares', 'tw');">
                        <i class="fa fa-twitter"></i>
                    </button>
                    <a class="goodshare liked btn" href="#" title="Добавить в избранное">
                        <i class="fa fa-heart-o" aria-hidden="true"></i>
                    </a>
                </div>
                <section id="descriptions" style="width:100%;">
                    <hr />
                    <h4 class="h4"><?= Yii::t('app', 'Описание') ?> <i class="fa fa-file-o"></i></h4>
                    <p itemprop="description" class="caption">
                        <?= $model->description ?>
                    </p>
                </section>
                <!--<section id="comments" style="width:100%;">
                    <hr />
                    <h4 class="h4">Комментарий <i class="fa fa-comments-o"></i></h4>
                    <div class="media">
                        <p class="pull-left">
                            <img class="media-object img-circle" alt="" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI2NCIgaGVpZ2h0PSI2NCI+PHJlY3Qgd2lkdGg9IjY0IiBoZWlnaHQ9IjY0IiBmaWxsPSIjZWVlIi8+PHRleHQgdGV4dC1hbmNob3I9Im1pZGRsZSIgeD0iMzIiIHk9IjMyIiBzdHlsZT0iZmlsbDojYWFhO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjEycHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+NjR4NjQ8L3RleHQ+PC9zdmc+" />
                        </p>
                        <div class="media-body">
                            <h4 class="media-heading">Заголовок медиа</h4>
                            <span class="comments-date"><i class="fa fa-calendar"></i> 10.02.16 в 12:28</span>
                            <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>

                            <div class="media">
                                <p class="pull-left">
                                    <img class="media-object img-circle" alt="" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI2NCIgaGVpZ2h0PSI2NCI+PHJlY3Qgd2lkdGg9IjY0IiBoZWlnaHQ9IjY0IiBmaWxsPSIjZWVlIi8+PHRleHQgdGV4dC1hbmNob3I9Im1pZGRsZSIgeD0iMzIiIHk9IjMyIiBzdHlsZT0iZmlsbDojYWFhO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjEycHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+NjR4NjQ8L3RleHQ+PC9zdmc+" />
                                </p>
                                <div class="media-body">
                                    <h4 class="media-heading">Вложенный заголовок медиа</h4>
                                    <span class="comments-date"><i class="fa fa-calendar"></i> 10.02.16 в 12:28</span>
                                    <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>-->
                <section id="comments" class="hidden-xs" style="width:100%;">
                    <hr />
                    <h4 class="h4"><?= Yii::t('app', 'Комментарий') ?> <i class="fa fa-comments-o"></i></h4>
                    <div id="disqus_thread"></div>
                    <script>
                        /**
                         *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                         *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
                         */
                        /*
                         var disqus_config = function () {
                         this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
                         this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                         };
                         */
                        (function() {  // DON'T EDIT BELOW THIS LINE
                            var d = document, s = d.createElement('script');

                            s.src = '//ulevelco.disqus.com/embed.js';

                            s.setAttribute('data-timestamp', +new Date());
                            (d.head || d.body).appendChild(s);
                        })();
                    </script>
                    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
                    <!-- <h4 class="h4">Оставить комментарий <i class="fa fa-comment-o"></i></h4>
                     <form action="#" method="post" class="form">
                         <p>Здравствуйте, Name. <a href="#" title="Выйти">Выход &raquo;</a></p>
                         <textarea id="real-comment" class="form-control" name="real-comment" rows="5" cols="30"></textarea>
                         <p class="pull-right">
                             <input type="submit" name="submit" value="Отправить" id="submit" class="btn" />
                             <input type="hidden" name="comment_post_ID" value="" id="comment_post_ID" />
                             <input type="hidden" name="comment_parent"  value="" id="comment_parent" />
                         </p>
                     </form>-->
                </section>
            </div>
            <meta itemprop="name" content="<?= $model->name_ru ?>">
            <meta itemprop="alternativeHeadline" content="<?= $model->name_en ?>">
            <meta itemprop="thumbnailUrl " content="https://ulevel.co/images/movies/<?=$model->image?>">
            <meta itemprop="image " content="https://ulevel.co/images/movies/<?=$model->image?>">
            <meta itemprop="uploadDate" content="<?= $model->created_at ?>">
            <meta itemprop="datePublished" content="<?= $model->created_at ?>">
            <meta itemprop="duration" content="<?= $model->duration ?>">
            <meta itemprop="interactionCount" content="UserViews:<?= $model->viewed ?>" />
            <meta itemprop="license" content="СС">
            <meta itemprop="status" content="published">
            <meta itemprop="inLanguage" content="ru">
            <meta itemprop="isFamilyFriendly" content="true">
            <!-- banner widget-->
            <div class="col-lg-3 col-md-3 hidden-xs" style="margin-top: 80px;width: 26.6%;box-shadow: 0px 10px 20px 0px rgba(0, 0, 0, 0.2);padding: 5px;">
                <img src="/images/668810.png" alt="banner" class="img-responsive"/>
            </div>
            <!-- start test widget-->
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" id="test-one" style="box-shadow: 0 10px 20px 0 rgba(0,0,0,.2); margin-top: 25px">
                <div id="test-start">
                    <p class="alert alert-info"><?= Yii::t('app', 'Пройдите тест, чтобы запомнить свои слова из словаря') ?> <i class="fa fa-smile-o" aria-hidden="true"></i>
                    </p>
                    <button id="begin-test" class="btn btn-success" onclick="ga('send', 'event', 'test', 'uword');"><?= Yii::t('app', 'Давайте начнем') ?></button>
                </div>
                <aside id="test-container" style="width: 100%; display: none;">
                    <div id="test-container-inner">
                        <h3>Uword</h3>
                        <p>
                            <small><?= Yii::t('app', 'Осталось выучить слов') ?>: <span><span id="tests-left"></span>/<span id="tests-all"></span></span></small>
                            <button type="button"
                                    data-toggle="popover"
                                    title="<?= Yii::t('app', 'Подсказка') ?>"
                                    data-content="<?= Yii::t('app', 'Переводите слова для изучения') ?>"
                                    data-trigger="hover"
                                    class="btn btn-default btn-sm">
                                <i class="fa fa-info"></i>
                            </button>
                        </p>
                        <form name="test" action="#" method="post" class="form">
                            <div class="pull-left test-word text-center">
                                <label id="test-word"></label>
                            </div>
                            <div class="pull-right" style="width: 50%;">
                                <div id="test-translations"></div>
                                <div class="pull-right" style="width: 100%;">
                                    <input type="button" name="test" value="<?= Yii::t('app', 'Завершить') ?>" class="btn test-btn-success" onclick="Test.showResult();" style="width: 100%;">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                    <!-- Result Chart js -->
                    <div id="result-chart" style="width: 100%; display: none;">
                        <div id="MyBar" style="min-width: 110px; height: 350px;"></div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="test-again" style="display: none;">
                        <button type="button" class="btn test-btn-primary" onclick="Test.init(); ga('send', 'event', 'shares', 'tw');">
                            <i class="fa fa-repeat"></i>&nbsp;&nbsp;<?= Yii::t('app', 'Еще раз') ?>
                        </button>
                    </div>
                </aside>
                <hr />
                <!-- Recommendation widget
                <aside class="widget">
                    <h4 class="widget-title">Рекомендуем также просмотреть:</h4>
                    <ul>
                        <li class="clearfix">
                            <div class="recent-thumb col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <img src="images/films1.jpg" class="img-responsive" alt=""/>
                                <span class="views">02:21</span>
                            </div>
                            <div class="recent-title col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                <a href="" title="" >
                                    <p class="ru">Час Пик</p>
                                    <p class="en">Rush Hour</p>
                                </a>
                                <span class="post-date">2016 просмотров</span>
                            </div>
                        </li>
                        <li class="clearfix">
                            <div class="recent-thumb col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <img src="images/films2.png" class="img-responsive" alt=""/>
                                <span class="views">02:21</span>
                            </div>
                            <div class="recent-title col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                <a href="" title="">
                                    <p class="ru">Люди в черном 2</p>
                                    <p class="en">Men in black 2</p>
                                </a>
                                <span class="post-date">2016 просмотров</span>
                            </div>
                        </li>
                        <li class="clearfix">
                            <div class="recent-thumb col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <img src="images/films3.png" class="img-responsive" alt="" />
                                <span class="views">02:21</span>
                            </div>
                            <div class="recent-title col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                <a href="" title="">
                                    <p class="ru">Гарри Поттер 2</p>
                                    <p class="en">Harry Potter 2</p>
                                </a>
                                <span class="post-date">2016 просмотров</span>
                            </div>
                        </li>
                        <li class="clearfix">
                            <div class="recent-thumb col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <img src="images/films4.png" class="img-responsive" alt="" />
                                <span class="views">02:21</span>
                            </div>
                            <div class="recent-title col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                <a href="" title="">
                                    <p class="ru">Трансформер</p>
                                    <p class="en">Transformer</p>
                                </a>
                                <span class="post-date">2016 просмотров</span>
                            </div>
                        </li>
                        <li class="clearfix">
                            <div class="recent-thumb col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                <img src="images/films4.png" class="img-responsive" alt="" />
                                <span class="views">02:21</span>
                            </div>
                            <div class="recent-title col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                <a href="" title="">
                                    <p class="ru">Трансформер</p>
                                    <p class="en">Transformer</p>
                                </a>
                                <span class="post-date">2016 просмотров</span>
                            </div>
                        </li>
                    </ul>
                </aside>-->
            </div>
<!--    end test widget        -->
        </article>
    </div>
</main>