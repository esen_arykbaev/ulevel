<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;

use common\models\Genre;
use common\models\Level;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Фильмы,сериалы,отрывки из видео на английском с субтитрами c онлайн переводчиком бесплатно';

$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Разговорный английские уроки,видео,фильмы,сериалы,отрывки из видео для начинающих встречи, за обедом, истории, знакомство, пикап, IT, медицина. Фильмы,сериалы,отрывки из фильмов на английском с субтитрами c онлайн переводчиком'
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'уроки для начинающих, английский встречи, английский IT, английский медицина, английский знакомство'
]);

$this->params['breadcrumbs'][] = $this->title;

$genres = [];

if ($levelId == Level::LEVEL_BEGINNER) {
    $genres = Genre::getGenreBeginner();
} elseif ($levelId == Level::LEVEL_INTERMEDIATE) {
    $genres = Genre::getGenreIntermediate();
} else {
    $genres = Genre::getGenreAdvanced();
}

$genresCount = count($genres);
$dropDownGenres = [];

if ($genresCount > 7) {
    $tempGenres = array_slice($genres, 0, 7, true);
    $dropDownGenres = array_slice($genres, 7, null, true);
    $genres = $tempGenres;
}
?>
<main class="container">
    <?php if (!$isQuerySearch): ?>
    <menu id="category" class="navbar">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#categorys-menu">
            <span class="sr-only"><?= Yii::t('app', 'Меню') ?></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <ul id="categorys-menu" class="nav navbar-nav collapse navbar-collapse" itemscope itemtype="http://schema.org/VideoObject">
            <?php if (count($dropDownGenres)): ?>
                <li class="dropdown dropdowns">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span><?= Yii::t('app', 'Категории') ?></span><i class="fa fa-bars"></i></a>
                    <ul class="dropdown-menu">
                        <?php foreach ($dropDownGenres as $levelKey => $levelVal): ?>
                            <li>
                                <?= Html::a($levelVal, [
                                    'movie/index', 'level' => $level, 'genre' => $levelKey
                                ]) ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
            <?php endif; ?>
            <?php if ($levelId == Level::LEVEL_BEGINNER) { ?>
                <!--<li class="category-menu hidden-xs">
                    <a href="/movie/index.html?level=beginner" style="font-weight:500;font-size:115%;"><?= Yii::t('app', 'Все видео') ?></a>
                </li>-->
            <?php } elseif ($levelId == Level::LEVEL_INTERMEDIATE) { ?>
                <!--<li class="category-menu hidden-xs">
                    <a href="/movie/index.html?level=intermediate" style="font-weight:500;"><?= Yii::t('app', 'Все видео') ?></a>
                </li>-->
            <?php } else { ?>
                <!--<li class="category-menu hidden-xs">
                    <a href="/movie/index.html?level=advanced" style="font-weight:500;font-size:115%;"><?= Yii::t('app', 'Все видео') ?></a>
                </li>-->
            <?php } ?>
                <!--<li style="border-right: 1px solid rgb(219, 219, 219);">
                    <a href="/site/happy.html" title="happyhope english" style="padding-top:0;padding-bottom:0">
                        <img src="/images/673229.jpg" alt="happyhope english" class="img-responsive"/>
                    </a>
                </li>-->
                <li style="border-right: 1px solid rgb(219, 219, 219);">
                    <a href="/site/lynchie.html" title="lynchie english" style="padding-top:0;padding-bottom:0" onclick="ga('send', 'event', 'blogger', 'lynchie');">
                        <img src="/images/669685.png" alt="lynchie english" class="img-responsive"/>
                    </a>
                </li>
            <?php foreach ($genres as $levelKey => $levelVal): ?>
                <li class="category-menu">
                    <?= Html::a($levelVal, [
                        'movie/index', 'level' => $level, 'genre' => $levelKey
                    ]) ?>
                </li>
            <?php endforeach; ?>
        </ul>
    </menu>
    <?php else: ?>
    <div class="row search">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1><?= Yii::t('app', 'Поиск') ?>: <span><?= $_GET['q'] ?></span></h1>
            <p><?= Yii::t('app', 'Всего найдено') ?>: <span><?= $foundCount ?></span> <?= Yii::t('app', 'записей') ?></p>
        </div>
    </div>
    <?php endif; ?>
    <div class="row" id="episodes">
    <?php if ($levelId == Level::LEVEL_INTERMEDIATE) { ?>
        <h4 style="text-align: center;color: #A144E3;">ТОП - 4 для практики английского</h4>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/site/business.html" class="img-link">
                    <img itemprop="thumbnail" src="/images/business.png" alt='' class="img-responsive ">
                    <div class="cover-bg" style="bottom: 79px;"></div>
                    <span class="views hidden-xs">10:00</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Бизнес английский</h4>
                        <h5 itemprop="alternativeHeadline">The Business of English</h5>
                    </div>
                </a>
                <meta itemprop="name" content='Бизнес английский'>
                <meta itemprop="thumbnailUrl " content="/images/movies/76/202_87684.jpg">
                <link itemprop="image" href="/images/movies/76/202_87684.jpg">
                <meta itemprop="duration" content="11:00">
                <meta itemprop="interactionCount" content="UserViews:20" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/site/travel.html" class="img-link">
                    <img itemprop="thumbnail" src="/images/travel.png" alt='' class="img-responsive fit-cover">
                    <div class="cover-bg" style="bottom: 79px;"></div>
                    <span class="views hidden-xs">10:00</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Английский для путешествий</h4>
                        <h5 itemprop="alternativeHeadline">The Travel of English</h5>
                    </div>
                </a>
                <meta itemprop="name" content='Английский для путешествий'>
                <meta itemprop="thumbnailUrl " content="/images/movies/76/202_87684.jpg">
                <link itemprop="image" href="/images/travel.png">
                <meta itemprop="duration" content="11:00">
                <meta itemprop="interactionCount" content="UserViews:20" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/site/technology.html" class="img-link">
                    <img itemprop="thumbnail" src="/images/it.png" alt='' class="img-responsive ">
                    <div class="cover-bg" style="bottom: 79px;"></div>
                    <span class="views hidden-xs">10:00</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">IT английский</h4>
                        <h5 itemprop="alternativeHeadline">The IT of English</h5>
                    </div>
                </a>
                <meta itemprop="name" content='IT английский'>
                <meta itemprop="thumbnailUrl " content="/images/movies/76/202_87684.jpg">
                <link itemprop="image" href="/images/it.png">
                <meta itemprop="duration" content="11:00">
                <meta itemprop="interactionCount" content="UserViews:20" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/site/medicine.html" class="img-link">
                    <img itemprop="thumbnail" src="/images/house.png" alt='' class="img-responsive ">
                    <div class="cover-bg" style="bottom: 79px;"></div>
                    <span class="views hidden-xs">10:00</span>
                    <div class="caption text-left">
                        <h4 itemprop="name">Английский от Доктора Хауса</h4>
                        <h5 itemprop="alternativeHeadline">The English by HOUSE M.D</h5>
                    </div>
                </a>
                <meta itemprop="name" content='Английский от Доктора Хауса'>
                <meta itemprop="thumbnailUrl " content="/images/movies/76/202_87684.jpg">
                <link itemprop="image" href="/images/house.png">
                <meta itemprop="duration" content="11:00">
                <meta itemprop="interactionCount" content="UserViews:20" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
            </div>
        </div>
        <div class="col-xs-12">
            <hr/>
            <h4 style="text-align: center;color: #A144E3;">Новинки</h4>
        </div>
    <?php } ?>
    <?php foreach ($movies as $movie): ?>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
            <div class="thumbnail" itemscope itemtype="http://schema.org/VideoObject">
                <a itemprop="url" href="/movie/view/<?= $movie->movie_id ?>" class="img-link">
                    <img itemprop="thumbnail" src="/images/movies/<?= $movie->image ?>" alt='<?= $movie->name_ru ?>' class="img-responsive fit-cover">
                    <div class="cover-bg"></div>
                    <span class="views hidden-xs"><?= $movie->duration ?></span>
                    <div class="caption text-left">
                        <h4 itemprop="name"><?= $movie->name_ru ?></h4>
                        <h5 itemprop="alternativeHeadline"><?= $movie->name_en ?></h5>
                        <h6><i class="fa fa-eye"></i> <?= $movie->viewed ?></h6>
                    </div>
                </a>
                <meta itemprop="name" content='<?= $movie->name_ru ?>'>
                <meta itemprop="thumbnailUrl " content="/images/movies/<?=$movie->image?>">
                <link itemprop="image" href="/images/movies/<?= $movie->image ?>">
                <meta itemprop="duration" content="<?= $movie->duration ?>">
                <meta itemprop="interactionCount" content="UserViews:<?= $movie->viewed ?>" />
                <meta itemprop="isFamilyFriendly" content="true">
                <meta itemprop="width" content="265">
                <meta itemprop="height" content="150">
                <meta itemprop="license" content="СС">
                <meta itemprop="status" content="published">
                <!--<meta itemprop="uploadDate" content="<?= $movie->created_at ?>">
                <meta itemprop="datePublished" content="<?= $movie->created_at ?>">-->
            </div>
        </div>
    <?php endforeach; ?>
    </div>
    <div class="text-center"><?= LinkPager::widget([ 'pagination' => $pages ]) ?></div>
</main>
