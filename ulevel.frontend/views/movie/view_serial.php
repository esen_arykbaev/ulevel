<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Breadcrumbs;

use common\models\Level;
use common\models\Movie;

use frontend\models\MovieHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Movie */

$this->registerCssFile('@web/assets/build/mediaelementplayer.min.css', ['depends' => 'frontend\assets\AppAsset']);

$this->registerJsFile('@web/assets/build/mediaelement-and-player.min.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('@web/assets/build/mep-feature-jumpforward.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('@web/assets/build/mep-feature-universalgoogleanalytics.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('@web/assets/js/goodshare.min.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('@web/js/video.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJs(
    'Test.words = ' . json_encode($words) . ';' . 'Test.tests = ' . json_encode($tests) . ';',
    View::POS_END
);
$this->title = 'Смотреть на английском '.$model->name_ru. ' с субтитрами';
$this->params['breadcrumbs'][] = [
  'label' => Level::LEVELS[$model->level],
  'url' => ['index', 'level' => Level::LEVELS_LINK[$model->level]]
];
$this->params['breadcrumbs'][] = $model->name_ru;
$this->registerMetaTag([
  'name' => 'description',
  'content' => 'Разговорный английский язык урок на тему ' . $model->name_ru . ' с английскими и русскими субтитрами, большое количество фраз, диалогов, слов, которое поможет разговорному английскому'
]);
$this->registerMetaTag([
  'name' => 'keywords',
  'content' => 'урок ' .$model->name_ru . ', английский урок '. $model->name_ru .  ' , английский IT, английский медицина, английский знакомство'
]);

?>
<meta property="og:description" content="Английский урок на тему <?= $model->name_ru ?> с английскими и русскими субтитрами, большое количество фраз, диалогов, слов, которое поможет разговорному английскому языку онлайн бесплатно"/>
<main class="container" itemscope itemtype="http://schema.org/VideoObject">
  <meta itemprop="name" content="<?= $model->name_ru ?>">
  <meta itemprop="alternativeHeadline" content="<?= $model->name_en ?>">
  <meta itemprop="thumbnailUrl " content="https://ulevel.co/images/movies/<?=$model->image?>">
  <meta itemprop="image " content="https://ulevel.co/images/movies/<?=$model->image?>">
  <meta itemprop="uploadDate" content="<?= $model->created_at ?>">
  <meta itemprop="datePublished" content="<?= $model->created_at ?>">
  <meta itemprop="duration" content="<?= $model->duration ?>">
  <meta itemprop="interactionCount" content="UserViews:<?= $model->viewed ?>" />
  <meta itemprop="license" content="СС">
  <meta itemprop="status" content="published">
  <meta itemprop="inLanguage" content="ru">
  <meta itemprop="isFamilyFriendly" content="true">
  <div class="row" id="season">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <?=
      Breadcrumbs::widget([
          'homeLink' => [
              'label' => '<i class="fa fa-home"></i>',
              'url' => '/'
          ],
          'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
          'encodeLabels' => false
      ]);
      ?>
    </div>
    <div class="col-xs-12 col-sm-3 col-md-3">
      <img src="/images/movies/<?= $model->image ?>" alt='<?= $model->name_ru ?>' class="img-responsive thumbnail fit-cover" />
      <ul>
        <li>
          <span><i class="fa fa-calendar"></i> <?= Yii::t('app', 'Год') ?>: </span><?= $model->release_year ?>
        </li>
        <li>
          <span><i class="fa fa-clock-o"></i> <?= Yii::t('app', 'Время') ?>: </span><?= $model->duration ?>
        </li>
        <li>
          <span><i class="fa fa-film"></i> <?= Yii::t('app', 'Жанр') ?>: </span>
          <?php for ($i = 0, $len = count($genres); $i < $len; $i++): ?>
          <?= $genres[$i] ?><?= $i != ($len - 1) ? ', ' : '' ?>
          <?php endfor; ?>
        </li>
      </ul>
      <div class="panel panel-default">
        <div class="panel-heading">
          <h1 class="panel-title"><?= $model->name_en ?></h1>
        </div>
        <div class="panel-body">
          <?= $model->description ?>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-9 col-md-9">
      <div class="row">
        <div class="col-xs-6 social">
          <button class="goodshare fb" data-type="fb" onclick="ga('send', 'event', 'shares', 'fb');">
            <i class="fa fa-facebook"></i>&nbsp;&nbsp;
            <span data-counter="fb"></span>
          </button>
          <button class="goodshare gp" data-type="gp" onclick="ga('send', 'event', 'shares', 'gp');">
            <i class="fa fa-google-plus"></i>&nbsp;&nbsp;
            <span data-counter="gp"></span>
          </button>
          <button class="goodshare vk" data-type="vk" onclick="ga('send', 'event', 'shares', 'vk');">
            <i class="fa fa-vk"></i>&nbsp;&nbsp;
            <span data-counter="vk"></span>
          </button>
          <button class="goodshare ok" data-type="ok" onclick="ga('send', 'event', 'shares', 'ok');">
            <i class="fa fa-odnoklassniki"></i>&nbsp;&nbsp;
            <span data-counter="ok"></span>
          </button>
          <button class="goodshare tw" data-type="tw" onclick="ga('send', 'event', 'shares', 'tw');">
            <i class="fa fa-twitter"></i>
          </button>
        </div>
        <div class="col-xs-6 serial-dictionary">
          <?php if (Yii::$app->user->isGuest): ?>
            <a href="/profile/login.html" class="notification-trigger">
              <span><?= Yii::t('app', 'Мой словарь') ?></span> &nbsp;<i class="fa fa-book fa-2x"></i>
              <div class="notification-num"><?= $userDictionaryCount ?></div>
            </a>
          <?php else: ?>
            <a href="/profile/dictionary.html" class="notification-trigger">
              <span><?= Yii::t('app', 'Мой словарь') ?></span> &nbsp;<i class="fa fa-book fa-2x"></i>
              <div id="dictionary-count" class="notification-num"><?= $userDictionaryCount ?></div>
            </a>
          <?php endif; ?>
        </div>
      </div>
      <div class="row" id="films">
        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
          <div class="video"></div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="test-one" style="box-shadow: 0 10px 20px 0 rgba(0,0,0,.2); margin-top: 25px">
          <div id="test-start">
            <p class="alert alert-info"><?= Yii::t('app', 'Пройдите тест, чтобы запомнить свои слова из словаря') ?> <i class="fa fa-smile-o" aria-hidden="true"></i>
            </p>
            <button id="begin-test" class="btn btn-success" onclick="ga('send', 'event', 'test', 'uword');"><?= Yii::t('app', 'Давайте начнем') ?></button>
          </div>
          <aside id="test-container" style="width: 100%; display: none;">
            <div id="test-container-inner">
              <h3>Uword</h3>
              <p>
                <small><?= Yii::t('app', 'Осталось выучить слов') ?>: <span><span id="tests-left"></span>/<span id="tests-all"></span></span></small>
                <button type="button"
                        data-toggle="popover"
                        title="<?= Yii::t('app', 'Подсказка') ?>"
                        data-content="<?= Yii::t('app', 'Переводите слова для изучения') ?>"
                        data-trigger="hover"
                        class="btn btn-default btn-sm">
                  <i class="fa fa-info"></i>
                </button>
              </p>
              <form name="test" action="#" method="post" class="form">
                <div class="pull-left test-word text-center">
                  <label id="test-word"></label>
                </div>
                <div class="pull-right" style="width: 50%;">
                  <div id="test-translations"></div>
                  <div class="pull-right" style="width: 100%;">
                    <input type="button" name="test" value="<?= Yii::t('app', 'Завершить') ?>" class="btn test-btn-success" onclick="Test.showResult();" style="width: 100%;">
                  </div>
                </div>
                <div class="clearfix"></div>
              </form>
            </div>
            <!-- Result Chart js -->
            <div id="result-chart" style="width: 100%; display: none;">
              <div id="MyBar" style="min-width: 110px; height: 350px;"></div>
              <div class="clearfix"></div>
            </div>
            <div class="test-again" style="display: none;">
              <button type="button" class="btn test-btn-primary" onclick="Test.init(); ga('send', 'event', 'shares', 'tw');">
                <i class="fa fa-repeat"></i>&nbsp;&nbsp;<?= Yii::t('app', 'Еще раз') ?>
              </button>
            </div>
          </aside>
        </div>
      </div>
      <div class="row">
        <div class="tabs">
          <ul class="nav nav-tabs">
            <?php $i = 1; foreach ($allSeasons as $seasonNumber => $seasons): ?>
              <li role="presentation"<?= $i == 1 ? ' class="active"' : '' ?>><a data-toggle="tab" href="#tab-<?= $i++ ?>">Сезон <?= $seasonNumber ?> <span class="badge"><?= count($seasons) ?></span></a></li>
            <?php endforeach; ?>
          </ul>
          <div class="tab-content">
            <?php $i = 1; $spoiler = 1; foreach ($allSeasons as $seasonNumber => $seasons):?>
              <div class="tab-pane fade<?= $i == 1 ? ' in active' : '' ?>" id="tab-<?= $i++ ?>">
                <table class="table">
                  <?php $j = 1;  foreach ($seasons as $season): ?>
                    <tr>
                      <td><?= $j++ ?>.</td>
                      <td>
                        <a href="#1" class="serial-item" data-source="<?= MovieHelper::getEpisodeSourceName($season)  ?>" data-url="<?= MovieHelper::getEpisodeUrl($season) ?>"
                           data-en-sub="<?= $season->en_subtitle ?>" data-ru-sub="<?= $season->ru_subtitle ?>">
                          <?= $season->name_en ?>
                        </a>
                      </td>
                      <td>
                        <a href="#spoiler-<?= $spoiler ?>" data-toggle="collapse"	class="spoiler collapsed"><?= Yii::t('app', 'Описание') ?></a>
                        <div class="collapse" id="spoiler-<?= $spoiler++ ?>">
                          <div class="well">
                            <p><?= $season->description ?></p>
                          </div>
                        </div>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                </table>
              </div>
            <?php endforeach; ?>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
          <div class="col-sm-8 col-md-12 hidden-xs">
            <section id="comments" class="" style="width:100%;">
              <hr />
              <h4 class="h4"><?= Yii::t('app', 'Комментарий') ?> <i class="fa fa-comments-o"></i></h4>
              <div id="disqus_thread"></div>
              <script>
                (function() {  // DON'T EDIT BELOW THIS LINE
                  var d = document, s = d.createElement('script');

                  s.src = '//ulevelco.disqus.com/embed.js';

                  s.setAttribute('data-timestamp', +new Date());
                  (d.head || d.body).appendChild(s);
                })();
              </script>
              <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>

              <!--<form action="#" method="post" class="form">
                  <p>Здравствуйте, Name. <a href="#" title="Выйти">Выход &raquo;</a></p>
                  <textarea id="real-comment" class="form-control" name="real-comment" rows="5" cols="30"></textarea>
                  <p class="pull-right">
                      <input type="submit" name="submit" value="Отправить" id="submit" class="btn" />
                      <input type="hidden" name="comment_post_ID" value="" id="comment_post_ID" />
                      <input type="hidden" name="comment_parent"  value="" id="comment_parent" />
                  </p>
              </form>-->
            </section>
          </div>
      </div>
    </div>
  </div>
</main>
