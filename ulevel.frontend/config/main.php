<?php
$params = array_merge(
    require(__DIR__ . '/../../ulevel.common/config/params.php'),
    require(__DIR__ . '/../../ulevel.common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'name' => 'Ulevel',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'language' => 'ru',
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => [
                [
                    'pattern' => '',
                    'route' => 'site/index',
                    'suffix' => '',
                ],
                [
                    'pattern' => '<controller>/<action>/<id:\w+>',
                    'route' => '<controller>/<action>',
                    'suffix' => '',
                ],
                [
                    'pattern' => '<controller>/<action>',
                    'route' => '<controller>/<action>',
                    'suffix' => '.html',
                ],
                [
                    'pattern' => '<module>/<controller>/<action>/<id:\d+>',
                    'route' => '<module>/<controller>/<action>',
                    'suffix' => '',
                ],
                [
                    'pattern' => '<module>/<controller>/<action>',
                    'route' => '<module>/<controller>/<action>',
                    'suffix' => '.html',
                ],
            ],
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'google' => [
                    'class' => 'yii\authclient\clients\GoogleOAuth',
                    'clientId' => '130906238812-0v49v1tsh2n9m84j0gmjg5he9uiti5u5.apps.googleusercontent.com',
                    'clientSecret' => 'WsJBMPb5ZP0QRDLshHl-bSdG',
                    'returnUrl' => 'http://ulevel.co/profile/auth?authclient=google'
                ],
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId' => '166633817055693',
                    'clientSecret' => 'ece13efd99080958758c3246b825fb30',
                ],
                'vkontakte' => [
                    'class' => 'yii\authclient\clients\VKontakte',
                    'clientId' => '5391314',
                    'clientSecret' => 'N8T7ssogIdE7lwsdxvhS',
                    'scope' => 'email'
                ]
            ],
        ],
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
            'siteKey' => '6LdtuRwTAAAAAAN26oc1tX5UtAH0-mi_UXg2cP8a',
            'secret' => '6LdtuRwTAAAAABFvNmWbI_jdZvIvmnfEpKZsr2Sb',
        ],
    ],
    'params' => $params,
];
