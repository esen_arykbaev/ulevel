<?php

use yii\helpers\Url;

/** @var $user common\models\User */

?>
<p>Здравствуйте, <strong><?= $user->first_name ?></strong>!</p>
<p>
    Вы получили это письмо, потому что Ваш адрес электронной почты был указан при регистрации на
    <a href="<?= Url::home(true) ?>" target="_blank"><?= Yii::$app->name ?></a>.
</p>
<p>Чтобы подтвердить регистрацию, перейдите по этой ссылке.</p>
<a href="<?= Url::home(true) ?>profile/confirm-email/<?= $user->getEmailToken() ?>">
    <?= Url::home(true) ?>profile/confirm-email/<?= $user->getEmailToken() ?>
</a>
<p>Если ссылка не открывается, скопируйте её и вставьте в адресную строку браузера.</p>