<?php
return [
    'adminEmail' => 'office@ulevel.co',
    'supportEmail' => 'office@ulevel.co',
    'noreplyEmail' => 'no-reply@ulevel.co',
    'user.passwordResetTokenExpire' => 3600,
    'languages' => ['ru', 'en']
];
