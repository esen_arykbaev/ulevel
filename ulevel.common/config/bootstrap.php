<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/ulevel.frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/ulevel.backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/ulevel.console');
