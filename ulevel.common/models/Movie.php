<?php

namespace common\models;

use Yii;
use yii\imagine\Image as Imagine;
use Imagine\Image\Box;


/**
 * This is the model class for table "movie".
 *
 * @property string $movie_id
 * @property string $name_ru
 * @property string $name_en
 * @property string $movie_type
 * @property string $description
 * @property string $viewed
 * @property string $release_year
 * @property string $duration
 * @property string $level
 * @property string $image
 * @property string $en_subtitle
 * @property string $ru_subtitle
 * @property string $youtube
 * @property string $vimeo
 * @property string $megogo
 * @property string $local_video
 * @property string $created_at
 *
 * @property MovieGenre[] $movieGenres
 */
class Movie extends \yii\db\ActiveRecord
{
    /**
     * @var string
     */
    const IMAGE_PATH   = 'images/movies';

    /**
     * @var string
     */
   const SUB_EN_PATH    = 'subs/en';

    /**
     * @var string
     */
    const SUB_RU_PATH    = 'subs/ru';

    /**
     *
     */
    const MOVIE_TYPE_FILM = 1;

    /**
     *
     */
    const MOVIE_TYPE_SERIAL = 2;

    /**
     *
     */
    const MOVIE_TYPES = [
        self::MOVIE_TYPE_FILM => 'Фильм', self::MOVIE_TYPE_SERIAL => 'Сериал'
    ];

    /**
     * 
     */
    const IMAGE_WIDTH   = 260;

    /**
     * 
     */
    const IMAGE_HEIGHT  = 150;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'movie';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['name_ru', 'name_en', 'movie_type', 'description', 'release_year', 'duration', 'level'], 'required'
            ],
            [
                ['movie_type', 'release_year', 'viewed', 'level'], 'integer'
            ],
            [
                ['viewed', 'created_at'], 'safe'
            ],
            [
                ['name_ru', 'name_en', 'youtube', 'vimeo', 'megogo', 'local_video'], 'string', 'max' => 255
            ],
            [
                ['movie_type'],  function($attribute, $params) {
                            if (!array_key_exists($this->$attribute, self::MOVIE_TYPES)) {
                                $this->addError($attribute, 'Выберите тип фильма');
                            }
                        }, 'skipOnEmpty' => false, 'skipOnError' => false
            ],
            [
                ['description'], 'string', 'max' => 5000
            ],
            [
                ['duration'], 'string', 'max' => 45
            ],
            [
                ['image'], 'required', 'on' => 'create'
            ],
            [
                ['image'], 'file', 'extensions' => 'png, jpg, jpeg, gif'
            ],
            [
                ['en_subtitle', 'ru_subtitle'], 'file', 'skipOnEmpty' => true, 'checkExtensionByMimeType' => false, 'extensions' => 'srt, sub, sbv'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'movie_id' => 'Movie ID',
            'name_ru' => 'Рус. название',
            'name_en' => 'Англ. название',
            'movie_type' => 'Movie Type',
            'description' => 'Описание',
            'viewed' => 'Кол-во просмотров',
            'release_year' => 'Год выпуска',
            'duration' => 'Продолжительность',
            'level' => 'Уровень',
            'image' => 'Картинка',
            'en_subtitle' => 'Англ. субтитры',
            'ru_subtitle' => 'Рус. субтитры',
            'youtube' => 'Youtube',
            'vimeo' => 'Vimeo',
            'megogo' => 'Megogo',
            'local_video' => 'Local Video',
            'created_at' => 'Created At'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMovieGenres()
    {
        return $this->hasMany(MovieGenre::className(), ['movie_id' => 'movie_id']);
    }

    /**
     * Обрабатывает и загружает картинку фильма
     * @param $primaryKey
     * @return string
     */
    public function imageUpload($primaryKey)
    {
        // название картинки: movie_id_xxxxx.extension
        $imageName = $primaryKey . '_' . substr(md5(time()), mt_rand(0, 27), 5) . '.' . $this->image->extension;
        $imageFolderName = self::getAssetFolderName(self::IMAGE_PATH);

        $image = self::getAssetsPath(self::IMAGE_PATH) . DIRECTORY_SEPARATOR . $imageFolderName . DIRECTORY_SEPARATOR . $imageName;
        
        $this->image->saveAs($image);

        $this->resizeImage($image);

        return $imageFolderName . '/' . $imageName;
    }

    /**
     * @return string
     */
    public static function getAssetsPath($folder)
    {
        return Yii::getAlias('@frontend') . DIRECTORY_SEPARATOR . 'public_html' . DIRECTORY_SEPARATOR . $folder;
    }

    /**
     * @param $assetPath
     * @return string
     */
    public static function getAssetFolderName($assetPath)
    {
        $folder = substr(md5(time()), mt_rand(0, 30), 2);

        $imagePath = self::getAssetsPath($assetPath) . DIRECTORY_SEPARATOR . $folder;

        if (!file_exists($imagePath)) {
            mkdir($imagePath);
        }

        return $folder;
    }

    public function resizeImage($_image)
    {
        $image = Imagine::getImagine()->open($_image);
        // Изменяем размер аватарки
        if ($this->isResizeImage($image->getSize())) {
            $newSizes = $this->getImageNewSizes($image->getSize());
            $resizedImage = $image->resize(new Box($newSizes['width'], $newSizes['height']));
            $resizedImage->save($_image);
        }
    }

    /**
     * @param $size
     * @return bool
     */
    public function isResizeImage($size)
    {
        return ($size->getWidth() > self::IMAGE_WIDTH) || ($size->getHeight() > self::IMAGE_HEIGHT);
    }

    /**
     * @param $size
     * @return array
     */
    public static function getImageNewSizes($size)
    {
        $w = $size->getWidth();
        $h = $size->getHeight();
        $newDimensions = [];

        if ($w > $h) {
            $newDimensions['width'] = self::IMAGE_WIDTH;
            $newDimensions['height'] = floor( ($h/$w) * self::IMAGE_WIDTH );
        } else {
            $newDimensions['width'] = floor( ($w/$h) * self::IMAGE_HEIGHT );
            $newDimensions['height'] = self::IMAGE_HEIGHT;
        }

        return $newDimensions;
    }

    /**
     * @param $primaryKey
     * @param $subLangFolder
     * @param $lang
     * @return string
     */
    public function subtitleUpload($primaryKey, $subLangFolder, $lang)
    {
        // название картинки: movie_id_xxxxx.extension
        $subName = $primaryKey . '_' . substr(md5(time()), mt_rand(0, 27), 5) . '.' . $lang;

        $subFolderName = Movie::getAssetFolderName($subLangFolder);

        $sub = Movie::getAssetsPath($subLangFolder) . DIRECTORY_SEPARATOR . $subFolderName . DIRECTORY_SEPARATOR . $subName;

        if ($lang == 'en') {
            $this->en_subtitle->saveAs($sub);
        } elseif ($lang == 'ru') {
            $this->ru_subtitle->saveAs($sub);
        }


        return $subFolderName . '/' . $subName;
    }

    /**
     * Возвращает все сериалы
     * @return static[]
     */
    public static function getSerials()
    {
        return Movie::findAll([
            'movie_type' => self::MOVIE_TYPE_SERIAL
        ]);
    }

    /**
     * Удаляет картинку фильма
     */
   public static function deleteImage($image)
   {
       // Получаем полный путь к картинке
       $imagePath = self::getAssetsPath(self::IMAGE_PATH) . DIRECTORY_SEPARATOR . $image;

       // Проверка на существование
       if (file_exists($imagePath)) {
           unlink($imagePath);
       }
   }

    /**
     * Удаляет субтитры фильма
     */
    public static function deleteSubtitle($subFolder, $usbtitle)
    {
        // Получаем полный путь к англ. субтитру
        $enSub = self::getAssetsPath($subFolder) . DIRECTORY_SEPARATOR . $usbtitle;

        if (file_exists($enSub)) {
            unlink($enSub);
        }
    }
}
