<?php
namespace common\models;

/**
 * Class Level
 * @package common\models
 */
class Level
{
    /**
     * Начинающий
     */
    const LEVEL_BEGINNER        = '1';

    /**
     * Средний
     */
    const LEVEL_INTERMEDIATE    = '2';

    /**
     * Профессональный
     */
    const LEVEL_ADVANCED        = '3';

    /**
     *
     */
    const LEVELS = [
        self::LEVEL_BEGINNER => 'Начинающий',
        self::LEVEL_INTERMEDIATE => 'Средний',
        self::LEVEL_ADVANCED => 'Продвинутый'
    ];

    const LEVELS_LINK = [
        self::LEVEL_BEGINNER => 'beginner',
        self::LEVEL_INTERMEDIATE => 'intermediate',
        self::LEVEL_ADVANCED => 'advanced'
    ];


}