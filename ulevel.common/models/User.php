<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;


/**
 * This is the model class for table "user".
 *
 * @property string $user_id
 * @property string $first_name
 * @property string $email
 * @property string $email_status
 * @property string $avatar
 * @property string $about_me
 * @property string $password
 * @property string $auth_key
 * @property string $password_reset_token
 * @property integer $level
 * @property integer $status
 * @property string $created_at
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{

    /**
     * Пользователь удален
     */
    const STATUS_DELETED    = 0;

    /**
     * Активный пользователь
     */
    const STATUS_ACTIVE     = 1;

    /**
     * Email подтвержден
     */
    const EMAIL_CONFIRMED      = 1;

    /**
     * Email не подтвержден
     */
    const EMAIL_NOT_CONFIRMED    = 0;

    /**
     * Ширина аватарки
     */
    const AVATAR_WIDTH  = 150;

    /**
     * Высота аватарки
     */
    const AVATAR_HEIGHT = 150;

    /**
     *
     */
    const EMAIL_TOKEN_SALT = '95@#AfHR59#@';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['first_name', 'password', 'email', 'auth_key'], 'required'
            ],
            [
                ['level', 'status'], 'integer'
            ],
            [
                ['created_at', 'level', 'status'], 'safe'
            ],
            [
                ['first_name', 'avatar', 'password', 'password_reset_token'], 'string', 'max' => 255
            ],
            [
                ['auth_key'], 'string', 'max' => 45
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'first_name' => 'First Name',
            'about_me' => 'О себе',
            'password' => 'Password',
            'auth_key' => 'Auth Key',
            'password_reset_token' => 'Password Reset Token',
            'level' => 'Level',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne([
            'user_id' => $id,
            'status' => self::STATUS_ACTIVE
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
    * Finds user by email
    *
    * @param string $email
    * @return static|null
    */
    public static function findByEmail($email)
    {
        return static::findOne([
            'email' => $email,
            'status' => self::STATUS_ACTIVE
        ]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Удаляет аватарку пользователя
     */
    public function removeAvatar()
    {
        $imagePath = Yii::getAlias('@webroot') . '/images/avatars/' . $this->avatar;

        if (file_exists($imagePath)) {
            unlink($imagePath);
        }

        $this->avatar = null;

        return $this->save(false);
    }

    /**
     * todo: Переделать
     */
    public function getEmailToken()
    {
        return md5($this->user_id . self::EMAIL_TOKEN_SALT . $this->first_name) . $this->user_id;
    }

    /**
     * @param $token
     * @return string
     */
    public static function getUserIdFromToken($token)
    {
        return substr($token, 32);
    }
}
