<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "movie_genre".
 *
 * @property string $movie_id
 * @property string $genre_id
 *
 * @property Genre $genre
 * @property Movie $movie
 */
class MovieGenre extends \yii\db\ActiveRecord
{

    public static $movieGenres = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'movie_genre';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['movie_id', 'genre_id'], 'required'
            ],
            [
                ['movie_id', 'genre_id'], 'integer'
            ]
        ];
    }

    /**
     * @return bool
     */
    public static function _validate()
    {
        if (!isset($_POST['genres']) || !is_array($_POST['genres'])) {
            return false;
        }

        $genres = $_POST['genres'];

        foreach ($genres as $val) {
            if (!array_key_exists($val, Genre::getGenres())) {
                return false;
            }
            self::$movieGenres[] = $val;
        }

        return true;
    }

    /**
     * Сохраняет жанры
     * @param $movieId
     * @param $isNewRecord
     * @return boolean
     */
    public static function _save($movieId, $isNewRecord = true)
    {
        // Если есть данные, то удаляем и заново сохраняем
        if (!$isNewRecord) {
            MovieGenre::deleteAll([
                'movie_id' => $movieId
            ]);
        }

        $genres = [];

        foreach (self::$movieGenres as $genreId) {
            $genres[] = [$movieId, $genreId];
        }

        Yii::$app->db->createCommand()
            ->batchInsert(MovieGenre::tableName(), ['movie_id', 'genre_id'], $genres)
            ->execute();

        return true;
    }

    /**
     * Возвращает жанры фильма
     * @param $movieId
     * @return array
     */
    public static function getMovieGenres($movieId)
    {
        $movieGenres = MovieGenre::findAll([
            'movie_id' => $movieId
        ]);

        $_movieGenres = [];

        foreach ($movieGenres as $movieGenre) {
            $_movieGenres[] = $movieGenre->genre_id;
        }

        return $_movieGenres;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'movie_id' => 'Movie ID',
            'genre_id' => 'Genre ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGenre()
    {
        return $this->hasOne(Genre::className(), ['genre_id' => 'genre_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMovie()
    {
        return $this->hasOne(Movie::className(), ['movie_id' => 'movie_id']);
    }
}
