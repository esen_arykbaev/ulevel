<?php

namespace common\models;

use Yii;
use yii\base\InvalidParamException;
use yii\db\Expression;


/**
 * This is the model class for table "dictionary".
 *
 * @property string $word_id
 * @property string $word_en
 * @property string $word_ru
 *
 * @property UserDictionary[] $userDictionaries
 */
class Dictionary extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dictionary';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['word_en', 'word_ru'], 'required'
            ],
            [
                ['word_en', 'word_ru'], 'string', 'max' => 255
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'word_id' => 'Word ID',
            'word_en' => 'Word En',
            'word_ru' => 'Word Ru',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDictionaries()
    {
        return $this->hasMany(UserDictionary::className(), ['word_id' => 'word_id']);
    }

    /**
     * Returns $num random records
     * @param integer $num
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getRandomWords($num)
    {
        if (!ctype_digit($num) && !is_int($num)) {
            throw new InvalidParamException('Param $num must be numeric');
        }

        return static::find()
                    ->orderBy(new Expression('RAND()'))
                    ->limit($num)
                    ->all();
    }
}
