<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "genre".
 *
 * @property string $genre_id
 * @property string $genre_name
 */
class Genre extends \yii\db\ActiveRecord
{
    /**
     * @var null
     */
    private static $genreBeginner = null;

    /**
     * @var null
     */
    private static $genreIntermediate = null;

    /**
     * @var null
     */
    private static $genreAdvanced = null;

    /**
     * @var null
     */
    private static $genres = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'genre';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['genre_name'], 'required'
            ],
            [
                ['genre_name'], 'string', 'max' => 255
            ],
            [
                ['level'], 'integer'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'genre_id' => 'Genre ID',
            'genre_name' => 'Genre Name',
            'level' => 'Уровень'
        ];
    }

    /**
     * @return array|null
     */
    public static function getGenreBeginner()
    {
        if (self::$genreBeginner !== null) {
            return self::$genreBeginner;
        }

        $genres = Genre::find()
                ->where([
                    'level' => [Level::LEVEL_BEGINNER, null]
                ])
                ->all();

        self::$genreBeginner = ArrayHelper::map($genres, 'genre_id', 'genre_name');

        return self::$genreBeginner;
    }

    /**
     * @return array|null
     */
    public static function getGenreIntermediate()
    {
        if (self::$genreIntermediate !== null) {
            return self::$genreIntermediate;
        }

        $genres = Genre::find()
            ->where([
                'level' => [Level::LEVEL_INTERMEDIATE, null]
            ])
            ->all();

        self::$genreIntermediate = ArrayHelper::map($genres, 'genre_id', 'genre_name');

        return self::$genreIntermediate;
    }

    /**
     * @return array|null
     */
    public static function getGenreAdvanced()
    {
        if (self::$genreAdvanced !== null) {
            return self::$genreAdvanced;
        }

        $genres = Genre::find()
            ->where([
                'level' => [Level::LEVEL_ADVANCED, null]
            ])
            ->all();

        self::$genreAdvanced = ArrayHelper::map($genres, 'genre_id', 'genre_name');

        return self::$genreAdvanced;
    }

    /**
     * @return array|null
     */
    public static function getGenres()
    {
        if (self::$genres !== null) {
            return self::$genres;
        }

        self::$genres = ArrayHelper::map(Genre::find()->all(), 'genre_id', 'genre_name');

        return self::$genres;
    }
}
