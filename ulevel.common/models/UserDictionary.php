<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_dictionary".
 *
 * @property string $user_id
 * @property string $word_id
 * @property string $translate
 *
 * @property Dictionary $word
 * @property User $user
 */
class UserDictionary extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_dictionary';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['user_id', 'word_id', 'translate'], 'required'
            ],
            [
                ['user_id', 'word_id'], 'integer'
            ],
            [
                ['translate'], 'string', 'max' => 255
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'word_id' => 'Word ID',
            'translate' => 'Translate',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWord()
    {
        return $this->hasOne(Dictionary::className(), ['word_id' => 'word_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['user_id' => 'user_id']);
    }

    /**
     * @param $userId
     * @return int|string
     */
    public static function getUserDictionaryCount($userId)
    {
        return UserDictionary::find()
                    ->where([ 'user_id' => $userId ])
                    ->count();
    }
}
