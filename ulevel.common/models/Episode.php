<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "episode".
 *
 * @property string $movie_id
 * @property string $episode_id
 * @property integer $season
 * @property string $name_ru
 * @property string $name_en
 * @property string $description
 * @property string $viewed
 * @property string $release_year
 * @property string $duration
 * @property string $en_subtitle
 * @property string $ru_subtitle
 * @property string $youtube
 * @property string $vimeo
 * @property string $megogo
 * @property string $local_video
 * @property string $created_at
 *
 * @property Movie $movie
 */
class Episode extends \yii\db\ActiveRecord
{

    /**
     *
     */
    const SEASONS = [
        '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5',
        '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'episode';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['movie_id', 'season', 'name_ru', 'name_en', 'description', 'viewed', 'release_year', 'duration'], 'required'
            ],
            [
                ['movie_id', 'season', 'viewed'], 'integer'
            ],
            [
                ['release_year', 'created_at'], 'safe'
            ],
            [
                ['name_ru', 'name_en', 'youtube', 'vimeo', 'megogo', 'local_video'], 'string', 'max' => 255
            ],
            [
                ['description'], 'string', 'max' => 5000
            ],
            [
                ['duration'], 'string', 'max' => 45
            ],
            [
                ['en_subtitle', 'ru_subtitle'], 'file', 'skipOnEmpty' => true, 'checkExtensionByMimeType' => false, 'extensions' => 'srt, sub, sbv'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'movie_id' => 'ID сериала',
            'episode_id' => 'Episode ID',
            'season' => 'Season',
            'name_ru' => 'Name Ru',
            'name_en' => 'Name En',
            'description' => 'Description',
            'viewed' => 'Viewed',
            'release_year' => 'Release Year',
            'duration' => 'Duration',
            'en_subtitle' => 'En Subtitle',
            'ru_subtitle' => 'Ru Subtitle',
            'youtube' => 'Youtube',
            'vimeo' => 'Vimeo',
            'megogo' => 'Megogo',
            'local_video' => 'Local Video',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMovie()
    {
        return $this->hasOne(Movie::className(), ['movie_id' => 'movie_id']);
    }

    /**
     * @param $subLangFolder
     * @param $lang
     * @return string
     */
    public function subtitleUpload($subLangFolder, $lang)
    {
        $subFolderName = Movie::getAssetFolderName($subLangFolder);

        if ($lang == 'en') {
            $sub = Movie::getAssetsPath($subLangFolder) . DIRECTORY_SEPARATOR .
                $subFolderName . DIRECTORY_SEPARATOR . $this->en_subtitle->baseName . '.' . $this->en_subtitle->extension;
            $this->en_subtitle->saveAs($sub);
        } elseif ($lang == 'ru') {
            $sub = Movie::getAssetsPath($subLangFolder) . DIRECTORY_SEPARATOR .
                $subFolderName . DIRECTORY_SEPARATOR . $this->ru_subtitle->baseName . '.' . $this->ru_subtitle->extension;
            $this->ru_subtitle->saveAs($sub);
        }


        return $subFolderName;
    }

    /**
     * @param $subLangFolder
     * @param $subtitle
     */
    public function deleteSubtitle($subLangFolder, $subtitle)
    {
        $subtitle = Movie::getAssetsPath($subLangFolder) . DIRECTORY_SEPARATOR . $subtitle;

        if (file_exists($subtitle)) {
            unlink($subtitle);
        }
    }
}
