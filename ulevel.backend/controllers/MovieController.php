<?php

namespace backend\controllers;

use Yii;
use common\models\Movie;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

use common\models\Genre;
use common\models\MovieGenre;
use frontend\models\MovieSearch;

/**
 * MovieController implements the CRUD actions for Movie model.
 */
class MovieController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                  [
                    'actions' => ['index', 'view', 'create', 'update', 'delete'],
                    'allow' => true,
                    'roles' => ['@'],
                  ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Movie models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Movie::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Movie model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Movie model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Movie([
            'scenario' => 'create'
        ]);

        if ($model->load(Yii::$app->request->post()) && MovieGenre::_validate()) {

            $model->image = UploadedFile::getInstance($model, 'image');
            $model->en_subtitle = UploadedFile::getInstance($model, 'en_subtitle');
            $model->ru_subtitle = UploadedFile::getInstance($model, 'ru_subtitle');

            if ($model->save()) {

                $id = $model->getPrimaryKey();

                $model->image = $model->imageUpload($id);

                if ($model->en_subtitle !== null) {
                    $model->en_subtitle = $model->subtitleUpload($id, Movie::SUB_EN_PATH, 'en');
                }
                if ($model->ru_subtitle !== null) {
                    $model->ru_subtitle = $model->subtitleUpload($id, Movie::SUB_RU_PATH, 'ru');
                }

                // Сохраняем жанры
                MovieGenre::_save($id);

                if ($model->save(false)) {
                    return $this->redirect(['view', 'id' => $model->movie_id]);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Movie model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $model->image = UploadedFile::getInstance($model, 'image');
            $model->en_subtitle = UploadedFile::getInstance($model, 'en_subtitle');
            $model->ru_subtitle = UploadedFile::getInstance($model, 'ru_subtitle');

            if ($model->validate() && MovieGenre::_validate()) {
                $oldModel = $this->findModel($id);

                if ($model->image !== null) {

                    if (!empty($oldModel->image)) {
                        Movie::deleteImage($oldModel->image);
                    }

                    $model->image = $model->imageUpload($model->movie_id);
                } else {
                    $model->image = $oldModel->image;
                }

                if ($model->en_subtitle !== null) {

                    if (!empty($model->en_subtitle)) {
                        Movie::deleteSubtitle(Movie::SUB_EN_PATH, $model->en_subtitle);
                    }

                    $model->en_subtitle = $model->subtitleUpload($id, Movie::SUB_EN_PATH, 'en');
                } else {
                    $model->en_subtitle = $oldModel->en_subtitle;
                }

                if ($model->ru_subtitle !== null) {

                    if (!empty($model->ru_subtitle)) {
                        Movie::deleteSubtitle(Movie::SUB_RU_PATH, $model->ru_subtitle);
                    }

                    $model->ru_subtitle = $model->subtitleUpload($id, Movie::SUB_RU_PATH, 'ru');
                } else {
                    $model->ru_subtitle = $oldModel->ru_subtitle;
                }

                $model->save(false);
                MovieGenre::_save($model->movie_id, false);

                return $this->redirect(['view', 'id' => $model->movie_id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Movie model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($model->movie_type == Movie::MOVIE_TYPE_FILM) {
            
            MovieGenre::deleteAll([
                'movie_id' => $model->movie_id
            ]);

            if (!empty($model->image)) {
                Movie::deleteImage($model->image);
            }

            if (!empty($model->en_subtitle)) {
                Movie::deleteSubtitle(Movie::SUB_EN_PATH, $model->en_subtitle);
            }

            if (!empty($model->ru_subtitle)) {
                Movie::deleteSubtitle(Movie::SUB_RU_PATH, $model->ru_subtitle);
            }

            $model->delete();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Movie model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Movie the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Movie::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
