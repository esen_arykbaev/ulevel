<?php

namespace backend\controllers;

use common\models\Movie;
use Yii;
use yii\filters\AccessControl;
use common\models\Episode;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * EpisodeController implements the CRUD actions for Episode model.
 */
class EpisodeController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Episode models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Episode::find()
                        ->innerJoin(Movie::tableName(), 'movie.movie_id = episode.movie_id')
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Episode model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Episode model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Episode();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $model->en_subtitle = UploadedFile::getInstance($model, 'en_subtitle');
            $model->ru_subtitle = UploadedFile::getInstance($model, 'ru_subtitle');

            if ($model->en_subtitle !== null) {
                $subFolder = $model->subtitleUpload(Movie::SUB_EN_PATH, 'en');
                $model->en_subtitle = $subFolder . '/' . $model->en_subtitle->baseName . '.' . $model->en_subtitle->extension;
            }
            if ($model->ru_subtitle !== null) {
                $subFolder = $model->subtitleUpload(Movie::SUB_RU_PATH, 'ru');
                $model->ru_subtitle = $subFolder . '/' . $model->ru_subtitle->baseName . '.' . $model->ru_subtitle->extension;
            }

            $model->save(false);

            return $this->redirect(['view', 'id' => $model->episode_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Episode model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $model->en_subtitle = UploadedFile::getInstance($model, 'en_subtitle');
            $model->ru_subtitle = UploadedFile::getInstance($model, 'ru_subtitle');

            if ($model->en_subtitle !== null || $model->ru_subtitle !== null) {
                $oldModel = $this->findModel($id);
            }

            // Если есть новые субтитры, удаляем старые
            if ($model->en_subtitle !== null) {

                if (!empty($oldModel->en_subtitle)) {
                    $oldModel->deleteSubtitle(Movie::SUB_EN_PATH, $oldModel->en_subtitle);
                }

                $subFolder = $model->subtitleUpload(Movie::SUB_EN_PATH, 'en');
                $model->en_subtitle = $subFolder . '/' . $model->en_subtitle->baseName . '.' . $model->en_subtitle->extension;
            }
            if ($model->ru_subtitle !== null) {

                if (!empty($oldModel->en_subtitle)) {
                    $oldModel->deleteSubtitle(Movie::SUB_RU_PATH, $oldModel->en_subtitle);
                }

                $subFolder = $model->subtitleUpload(Movie::SUB_RU_PATH, 'ru');
                $model->ru_subtitle = $subFolder . '/' . $model->ru_subtitle->baseName . '.' . $model->ru_subtitle->extension;
            }

            $model->save();

            return $this->redirect(['view', 'id' => $model->episode_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Episode model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Episode model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Episode the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Episode::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
