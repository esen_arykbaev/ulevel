<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Movies';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="movie-index container">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Movie', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'movie_id',
            'name_ru',
            'name_en',
            'viewed',
            'release_year',
            // 'duration',
            // 'level',
            // 'movie_url:url',
            // 'image',
            // 'en_subtitle',
            // 'ru_subtitle',
            // 'youtube',
            // 'vimeo',
            // 'megogo',
            // 'local_video',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
