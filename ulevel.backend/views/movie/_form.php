<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use common\models\Genre;
use common\models\Level;
use common\models\MovieGenre;
use common\models\Movie;

/* @var $this yii\web\View */
/* @var $model common\models\Movie */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs('var genres = '. json_encode(Genre::getGenres()), \yii\web\View::POS_END);

?>

<div class="movie-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'autocomplete' => 'off',
            'enctype' => 'multipart/form-data'
        ]
    ]); ?>

    <?= $form->field($model, 'name_ru')->textInput(['maxlength' => true, 'required' => true]) ?>

    <?= $form->field($model, 'name_en')->textInput(['maxlength' => true, 'required' => true]) ?>

    <?= $form->field($model, 'movie_type')->dropDownList(Movie::MOVIE_TYPES) ?>

    <?= $form->field($model, 'description')->textarea(['maxlength' => true, 'required' => true]) ?>

    <?= $form->field($model, 'release_year')->textInput(['maxlength' => true, 'required' => true]) ?>

    <?= $form->field($model, 'duration')->textInput(['maxlength' => true, 'required' => true]) ?>

    <?= $form->field($model, 'level')->dropDownList(Level::LEVELS) ?>

    <?= $form->field($model, 'image')->fileInput(['accept' => 'image/*']) ?>

    <?= $form->field($model, 'en_subtitle')->fileInput() ?>

    <?= $form->field($model, 'ru_subtitle')->fileInput() ?>

    <?= $form->field($model, 'youtube')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vimeo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'megogo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'local_video')->textInput(['maxlength' => true]) ?>

    <?php if ($model->isNewRecord): ?>
    <div class="form-group">
        <?= Html::dropDownList('genres[]', null, Genre::getGenres(), ['class' => 'form-control']) ?>
    </div>
    <?php else: ?>
        <?php $i = 0; foreach (MovieGenre::getMovieGenres($model->movie_id) as $genreId): ?>
            <div class="form-group">
                <?= Html::dropDownList('genres[]', $genreId, Genre::getGenres(), ['class' => 'form-control form-group']) ?>
                <?php if ($i !== 0): ?>
                <button type="button" class="btn btn-default btn-sm remove-genre">Удалить жанр</button>
                <?php endif; ?>
            </div>
        <?php $i++; endforeach; ?>
    <?php endif; ?>

    <div class="form-group b-add-genre">
        <button type="button" class="btn btn-primary add-genre">Добавить жанр</button>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
