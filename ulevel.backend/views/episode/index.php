<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Episodes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="episode-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Episode', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'movie.name_ru',
            'episode_id',
            'season',
            'name_ru',
            'name_en',
            // 'description',
            // 'viewed',
            // 'release_year',
            // 'duration',
            // 'en_subtitle',
            // 'ru_subtitle',
            // 'youtube',
            // 'vimeo',
            // 'megogo',
            // 'local_video',
            // 'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
