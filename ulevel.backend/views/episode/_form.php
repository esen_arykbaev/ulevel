<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use common\models\Movie;
use common\models\Episode;

/* @var $this yii\web\View */
/* @var $model common\models\Episode */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="episode-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data'
        ]
    ]); ?>

    <?= $form->field($model, 'movie_id')->dropDownList(ArrayHelper::map(Movie::getSerials(), 'movie_id', 'name_ru')) ?>

    <?= $form->field($model, 'season')->dropDownList(Episode::SEASONS) ?>

    <?= $form->field($model, 'name_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'viewed')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'release_year')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'duration')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'en_subtitle')->fileInput() ?>

    <?= $form->field($model, 'ru_subtitle')->fileInput() ?>

    <?= $form->field($model, 'youtube')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vimeo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'megogo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'local_video')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
