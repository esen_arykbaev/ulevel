jQuery(document).ready(function()
{
  $('.add-genre').on('click', function() {
    var t = '<div class="form-group">' +
              '<select name="genres[]" class="form-control form-group">';

    for (var p in genres) {
      t += '<option value="'+ p +'">'+ genres[p] +'</option>';
    }

    t += '</select>' +
        '<button type="button" class="btn btn-default btn-sm remove-genre">Удалить жанр</button>' +
        '</div>';

    $(t).insertBefore($('.b-add-genre'));

  });

  $('.movie-form').on('click', '.remove-genre' ,function() {
    $(this).parent('.form-group').remove();
  });
});